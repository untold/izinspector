# About Izinspector

Use Izinspector to ease the keep inspectors organized and to ease the debugging with some additional editor features.

## Requirements

This version of the package is compatible with the following versions of the Unity Editor:

* 2022.2 and later (recommended)

## Package contents

The following table indicates the folder structure of the package:

|Location|Description|
|---|---|
|`<Editor>`|Editor folder. Contains all the code for the package's editor features.|
|`<Editor Default Resources>`|Editor folder. Contains all the assets used only by the editor.|
|`<Runtime>`|Runtime folder. Contains the code for the package's runtime features|

## Document revision history

|Date|Reason|
|---|---|
|March 6th, 2023|Document edited. Matches package version 2.1.1|
