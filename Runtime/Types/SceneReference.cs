using UnityEngine;

namespace Izinspector.Runtime.Types {
    [System.Serializable]
    public sealed class SceneReference {
        #region Public Variables
#if UNITY_EDITOR
        public UnityEditor.SceneAsset d_sceneAsset;
#endif
        [SerializeField] private string m_scenePath;
        [SerializeField] private string m_sceneName;
        #endregion

        #region Properties
        public string Path => m_scenePath;
        public string Name => m_sceneName;
        #endregion

        #region Public Methods
        public string GetPath() => m_scenePath;
        #endregion

        #region Operators
        public static implicit operator string(SceneReference sceneReference) => sceneReference.m_sceneName;
        #endregion
    }
}