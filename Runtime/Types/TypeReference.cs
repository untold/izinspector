using UnityEngine;

namespace Izinspector.Runtime.Types {
    [System.Serializable]
    public sealed class TypeReference<T> {
        #region Public Variables
        [SerializeField] private string m_assemblyQualifiedName;
#pragma warning disable CS0414 // Field is assigned but its value is never used
        [SerializeField] private string m_typeNameSpace;
        [SerializeField] private string m_typeName;
#pragma warning restore CS0414 // Field is assigned but its value is never used
        #endregion

        #region Operators
        public static implicit operator System.Type(TypeReference<T> typeReference) => System.Type.GetType(typeReference.m_assemblyQualifiedName);
        public static implicit operator TypeReference<T>(System.Type type) => new() {
            m_assemblyQualifiedName = type.AssemblyQualifiedName,
            m_typeNameSpace = type.Namespace,
            m_typeName = type.Name
        };
        #endregion
    }
}