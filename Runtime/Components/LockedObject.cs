﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace Izinspector.Runtime.Components {
    /// <summary>
    /// <b>Editor Only</b>
    /// </summary>
    [ExecuteInEditMode]
    public class LockedObject : MonoBehaviour {
        #region Private Variables
        [HideInInspector] public Vector3 _position;
        [HideInInspector] public Quaternion _rotation;
        [HideInInspector] public Vector3 _scale;
        private static Texture2D _lockIcon;
        #endregion

        #region Properties
        private static Texture2D LockIcon => _lockIcon ??= (Texture2D)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Object Locker/Lock.png");
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            _position = transform.localPosition;
            _rotation = transform.localRotation;
            _scale = transform.localScale;

            EditorGUIUtility.SetIconForObject(gameObject, LockIcon);
        }

        private void Update() {
            transform.localPosition = _position;
            transform.localRotation = _rotation;
            transform.localScale = _scale;

            EditorGUIUtility.SetIconForObject(gameObject, null);
        }
        #endregion
    }
}
#endif