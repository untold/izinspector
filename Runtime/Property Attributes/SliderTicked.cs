using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
    public class SliderTicked : PropertyAttribute {
        #region Public Variables
        #endregion

        #region Private Variables
        private float _min;
        private float _max;
        private float _snapThreshold;
        private float[] _ticks;
        #endregion

        #region Properties
        public float Min => _min;
        public float Max => _max;
        public float SnapThreshold => _snapThreshold;
        public float[] Ticks => _ticks;
        #endregion

        #region Constructors
        public SliderTicked(float min, float max, float snapThreshold, params float[] ticks) {
            _min = min;
            _max = max;
            _snapThreshold = snapThreshold;
            _ticks = ticks;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}