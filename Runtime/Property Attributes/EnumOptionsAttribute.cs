﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class EnumOptionsAttribute : PropertyAttribute {
        #region Properties
        public string DisplayName { get; }
        public string[] Options { get; }
        #endregion

        #region Constructors
        public EnumOptionsAttribute(string falseValue, string trueValue, string displayName = null) {
            Options = new[] { falseValue, trueValue };
            DisplayName = displayName;
        }

        public EnumOptionsAttribute(string[] options, string displayName = null) {
            Options = options;
            DisplayName = displayName;
        }
        #endregion
    }
}