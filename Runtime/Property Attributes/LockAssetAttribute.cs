﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public sealed class LockAssetAttribute : PropertyAttribute {
        #region Properties
        public string[] Paths { get; }
        #endregion

        #region Constructors
        public LockAssetAttribute(params string[] paths) => Paths = paths;
        #endregion
    }
}