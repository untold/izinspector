﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class ReadOnlyAttribute : PropertyAttribute {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        public ReadOnlyAttribute() {}
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}