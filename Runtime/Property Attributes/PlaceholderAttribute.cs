﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class PlaceholderAttribute : PropertyAttribute {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        public string Label { get; }
        #endregion

        #region Constructors
        public PlaceholderAttribute(string label) => Label = label;
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}