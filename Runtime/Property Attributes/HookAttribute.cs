﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class HookAttribute : PropertyAttribute {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        public string MethodName { get; }
        public bool RuntimeOnly { get; }
        #endregion

        #region Constructors
        public HookAttribute(string methodName, bool runtimeOnly = false) {
            MethodName = methodName;
            RuntimeOnly = runtimeOnly;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}