﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class ShowIfAttribute : PropertyAttribute {
        #region Properties
        public string MethodName { get; }
        public bool Invert { get; }
        #endregion

        #region Constructors
        public ShowIfAttribute(string methodName, bool invert = false, int order = -1) {
            MethodName = methodName;
            Invert = invert;
            this.order = order;
        }
        #endregion

    }
}