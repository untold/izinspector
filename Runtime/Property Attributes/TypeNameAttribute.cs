﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class TypeNameAttribute : PropertyAttribute {
        #region Properties
        public Type BaseType { get; }
        public bool UseQualifiedName { get; }
        #endregion

        #region Constructors
        public TypeNameAttribute(Type baseType, bool useQualifiedName = false) {
            BaseType = baseType;
            UseQualifiedName = useQualifiedName;
        }
        #endregion
    }
}