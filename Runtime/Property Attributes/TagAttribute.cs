using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
    public class TagAttribute : PropertyAttribute {
        #region Constructors
        public TagAttribute() {}
        #endregion
    }
}