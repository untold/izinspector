﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class SolidHeaderAttribute : PropertyAttribute {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        public string Label { get; }
        #endregion

        #region Constructors
        public SolidHeaderAttribute(string label) => Label = label;
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}