﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class IntPopupAttribute : PropertyAttribute {
        #region Properties
        public int[] Values { get; }
        #endregion

        #region Constructors
        public IntPopupAttribute(params int[] values) => Values = values;
        #endregion
    }
}