﻿using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
    public class AnimatorFieldAttribute : PropertyAttribute {
        #region Properties
        public AnimatorControllerParameterType? Type { get; }
        #endregion

        #region Constructors
        public AnimatorFieldAttribute() { }

        /// <summary>
        /// Displays the field as a popup populated with an animator's fields.
        /// </summary>
        /// <param name="type">The type of field to filter.</param>
        /// <remarks>There must be an <see cref="Animator"/> component attached to this <see cref="GameObject"/> or any of its parents for this attribute to work.</remarks>
        public AnimatorFieldAttribute(AnimatorControllerParameterType type) => Type = type;
        #endregion
    }
}