﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class EnumFromInstancesAttribute : PropertyAttribute {
        #region Constructors
        public EnumFromInstancesAttribute() {}
        #endregion
    }
}