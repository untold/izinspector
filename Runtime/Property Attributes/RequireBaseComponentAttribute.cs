﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public sealed class RequireBaseComponentAttribute : PropertyAttribute {
        #region Properties
        public Type RequiredType { get; }
        #endregion

        #region Constructors
        public RequireBaseComponentAttribute(Type type) => RequiredType = type;
        #endregion
    }
}