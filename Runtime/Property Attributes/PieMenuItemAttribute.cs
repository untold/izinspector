﻿#if UNITY_2022_1_OR_NEWER
using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    public enum PieContext { General = 0, Create = 1, Utility = 2 }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class PieMenuItemAttribute : PropertyAttribute {
        #region Properties
        public string Path { get; }
        public PieContext Context { get; }
        public string IconPath { get; }
        public string ValidatorName { get; }
        #endregion

        #region Constructors
        public PieMenuItemAttribute(string path, PieContext context = PieContext.General, string iconPath = null, string validatorName = null) {
            Path = path;
            Context = context;
            IconPath = iconPath;
            ValidatorName = validatorName;
        }
        #endregion
    }
}
#endif