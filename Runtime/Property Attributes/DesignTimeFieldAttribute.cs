﻿using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class DesignTimeFieldAttribute : PropertyAttribute {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        public DesignTimeFieldAttribute() {}
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}