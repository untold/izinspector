﻿using System;

namespace Izinspector.Runtime.PropertyAttributes.Filters {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class GroupAttribute : Attribute {
        #region Properties
        public string GroupName { get; }
        #endregion

        #region Constructors
        public GroupAttribute(string groupName) => GroupName = groupName;
        #endregion
    }
}