﻿using System;

namespace Izinspector.Runtime.PropertyAttributes.Filters {
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class InspectableAttribute : Attribute {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        public bool IsReadOnly { get; }
        #endregion

        #region Constructors
        public InspectableAttribute(bool isReadOnly = true) => IsReadOnly = isReadOnly;
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}