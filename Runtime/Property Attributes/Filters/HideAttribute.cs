﻿using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes.Filters {
    public class HideAttribute : PropertyAttribute { }
}