﻿using System;

namespace Izinspector.Runtime.PropertyAttributes.Filters {
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public sealed class ButtonAttribute : Attribute {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        public string Label { get; }
        #endregion

        #region Constructors
        public ButtonAttribute(string label) => Label = label;
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}