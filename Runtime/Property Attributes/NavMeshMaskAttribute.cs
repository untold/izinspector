using System;
using UnityEngine;

namespace Izinspector.Runtime.PropertyAttributes {
    [AttributeUsage(AttributeTargets.Field)]
    public class NavMeshMaskAttribute : PropertyAttribute {
        #region Properties
        public bool Multiple { get; }
        #endregion

        #region Constructors
        public NavMeshMaskAttribute(bool multiple = false) => Multiple = multiple;
        #endregion
    }
}