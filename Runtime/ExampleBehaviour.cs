﻿using System;
using Izinspector.Runtime.PropertyAttributes;
using Izinspector.Runtime.PropertyAttributes.Filters;
using UnityEngine;

namespace Izinspector.Runtime {
    public class ExampleBehaviour : MonoBehaviour {
        #region Constants
        [Inspectable] public const string k_path = "https://example.nowhere";
        #endregion

        #region Public Variables
        [Header("Serialized Fields")]
        [RuntimeField] public bool m_runtimeOnlyBoolProp;
        [DesignTimeField] public bool m_designTimeOnlyBoolProp;
        public bool m_boolProp;
        [SerializeField] private bool m_privateBoolProp;
        [SerializeField, DebugField] protected bool m_protectedBoolProp;
        [SerializeField, Lockable] private string m_lockableStringProp;
        [SerializeField, Placeholder("Placeholder Text")] private string m_stringWithPlaceholderProp;
        [SerializeField, Lockable, Placeholder("Placeholder Text")] private string m_lockableStringWithPlaceholderProp;
        [SerializeField, EnumOptions(new[] { "Option A", "Option B", "Option C" })] private string m_enumStringProp;
        [SerializeField, EnumOptions("False", "True")] private bool m_enumBoolProp;
        [SerializeField, Lockable, EnumOptions("False", "True")] private bool m_lockableEnumBoolProp;
        [SerializeField, Hook(nameof(OnHookedBoolPropChanged))] private bool m_hookedBoolProp;
        [SerializeField, ShowIf(nameof(m_hookedBoolProp))] private bool m_showIfBoolProp;
        [SerializeField, ShowIf(nameof(m_hookedBoolProp)), Lockable, EnumOptions("False", "True")] private bool m_showIfLockableEnumBoolProp;
        [SerializeField, EnumFromInstances] private GameObject m_objectReferenceAsEnum;

        [SerializeField, Group("Group")] private Vector2 m_groupedVector2;
        [SerializeField, Group("Group"), EnumOptions(new[] { "0", "1", "2" }, "Fake Int Popup")] private int m_groupedEnumIntProp;
        [SerializeField, Group("Group"), IntPopup(0, 1, 2)] private int m_groupedEnumTrueIntProp;

        // Non Serialized Fields
        [NonSerialized] public bool m_nonSerBoolProp;
        private bool m_nonSerPrivateBoolProp;
        protected bool m_nonSerProtectedBoolProp;

        [SerializeField, Group("Group")] private int m_groupedInt;
        [SerializeField, Range(0, 10), Group("Group")] private int m_groupedIntSlider;

        [Inspectable(false)] public static bool m_staticBoolField;
        [Inspectable] public static bool m_readonlyStaticBoolField;
        [Inspectable(false)] private static bool m_privateStaticBoolField;
        [Inspectable] private static bool m_privateReadonlyStaticBoolField;

        [DelegateInspector] public Action m_action;
        [DelegateInspector] public Action<bool, Vector2> m_actionWithArguments;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Methods
        [AnimationEvent]
        public void AnimationEventMethod() {}
        [AnimationEvent]
        public void AnimationEventMethodWithArgument(float value) {}
        [AnimationEvent]
        public void AnimationEventMethodWithArgument(AnimationEvent animationEvent) {}
        [Button("Button Method")]
        public void ButtonMethod() => Debug.Log("Button Method Invoked\n");
        #endregion

        #region Private Methods
        private void OnHookedBoolPropChanged() => Debug.Log($"New Value: {m_hookedBoolProp}\n");
        #endregion
    }
}