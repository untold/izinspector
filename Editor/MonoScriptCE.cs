﻿using System.Collections.Generic;
using System.Reflection;
using Izinspector.Editors.UIElements.MonoScriptInspector;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Packages.Izinspector.Editors {
    [CustomEditor(typeof(MonoScript))]
    public class MonoScriptCE : Editor {
        #region Private Variables
        private TypeToolbar _toolbar;
        private ToolbarToggle _publicMembersToggle, _privateMembersToggle;

        private ListView _membersListView;

        private List<MemberInfo> _publicMembers;
        private List<MemberInfo> _privateMembers;
        #endregion

        public override VisualElement CreateInspectorGUI() {
            var root = new VisualElement();

            var script = (MonoScript)target;

            script.ToType(out var type);
            if (type == null) {
                return root;
            }

            _publicMembers = type.GetPublicMembers();
            _privateMembers = type.GetPrivateMembers();

            _toolbar = new TypeToolbar() {
                style = {
                    top = -1f,
                    left = -15f,
                    borderRightWidth = 1f,
                    borderTopRightRadius = 10.5f,
                    borderBottomRightRadius = 10.5f,
                }
            };
            root.Add(_toolbar);
            _toolbar.Set(type.FullName!.Split('.'));

            _membersListView = new ListView(_publicMembers) {
                makeItem = MakeMemberItem,
                bindItem = BindMemberItem,
                fixedItemHeight = 23f,
                style = {
                    minHeight = 64f,
                    paddingTop = 4f,
                    paddingBottom = 4f,
                }
            };
            root.Add(_membersListView);

            return root;
        }

        private VisualElement MakeMemberItem() => new MemberInfoElement();

        private void BindMemberItem(VisualElement element, int index) {
            var mi = _publicMembers[index];
            (element as MemberInfoElement)!.Set(mi);
        }
    }
}