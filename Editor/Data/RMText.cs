﻿using UnityEngine;

namespace Izinspector.Editors.Data {
    internal enum TextStyle { BigHeader, Header, Section, Normal }

    [System.Serializable]
    internal class RMText : RMSection {
        #region Public Variables
        [field: SerializeField, TextArea] internal string Text { get; private set; }
        [field: SerializeField] internal TextStyle Style { get; private set; } = TextStyle.Normal;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}