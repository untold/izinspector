﻿using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Izinspector.Editors.Data {
    [CustomEditor(typeof(ReadMeSO))]
    internal sealed class ReadMeSOCE : Editor {
        #region Public Variables
        #endregion

        #region Private Variables
        private SerializedProperty _sectionsProp;

        private ReorderableList _sectionsListDrawers;

        private bool _editing;
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        private void OnEnable() {
            _sectionsProp = serializedObject.FindProperty("<Sections>k__BackingField");

            _sectionsListDrawers = new ReorderableList(serializedObject, _sectionsProp, true, true, true, true) {
                drawHeaderCallback = DrawListHeader,
                drawElementCallback = DrawListElement,
                onAddDropdownCallback = OnAddDropdownCallback,
                elementHeightCallback = CalcListElementHeight
            };
        }

        protected override void OnHeaderGUI() {
            GUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                GUILayout.FlexibleSpace();

                _editing = GUILayout.Toggle(_editing, Icons.Edit, EditorStyles.toolbarButton);
            }
            GUILayout.EndHorizontal();
        }

        public override void OnInspectorGUI() {
            serializedObject.Update();

            if (_editing)
                _sectionsListDrawers.DoLayoutList();
            else {
                for (int i = 0; i < _sectionsProp.arraySize; i++)
                    DrawSection(_sectionsProp.GetArrayElementAtIndex(i));

                GUILayout.Space(18);
            }

            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private void DrawListHeader(Rect rect) {
            GUI.Label(rect, "Sections", EditorStyles.label);
        }

        private void DrawListElement(Rect rect, int index, bool isActive, bool isFocused) {
            var indentedRect = new Rect(rect) {
                x = rect.x + 12,
                width = rect.width - 12
            };
            var ithProp = _sectionsProp.GetArrayElementAtIndex(index);
            EditorGUI.PropertyField(indentedRect, ithProp, true);
        }

        private void OnAddDropdownCallback(Rect buttonRect, ReorderableList list) {
            var menu = new GenericMenu();

            menu.AddItem(new GUIContent("Text"), false, AddSection<RMText>);
            menu.AddItem(new GUIContent("Bullet List"), false, AddSection<RMBulletList>);
            menu.AddItem(new GUIContent("Table"), false, AddSection<RMTable>);
            menu.AddItem(new GUIContent("Image"), false, AddSection<RMImage>);
            menu.AddItem(new GUIContent("Button"), false, AddSection<RMButton>);
            menu.AddItem(new GUIContent("Divider"), false, AddSection<RMDivider>);

            menu.DropDown(buttonRect);
        }

        private float CalcListElementHeight(int index) {
            var ithProp = _sectionsProp.GetArrayElementAtIndex(index);

            return EditorGUI.GetPropertyHeight(ithProp);
        }

        private void AddSection<T>() where T : RMSection {
            var index = _sectionsProp.arraySize;
            serializedObject.Update();
            _sectionsProp.InsertArrayElementAtIndex(index);

            if (typeof(T) == typeof(RMText))
                _sectionsProp.GetArrayElementAtIndex(index).managedReferenceValue = new RMText();
            else if (typeof(T) == typeof(RMBulletList))
                _sectionsProp.GetArrayElementAtIndex(index).managedReferenceValue = new RMBulletList();
            else if (typeof(T) == typeof(RMTable))
                _sectionsProp.GetArrayElementAtIndex(index).managedReferenceValue = new RMTable();
            else if (typeof(T) == typeof(RMImage))
                _sectionsProp.GetArrayElementAtIndex(index).managedReferenceValue = new RMImage();
            else if (typeof(T) == typeof(RMButton))
                _sectionsProp.GetArrayElementAtIndex(index).managedReferenceValue = new RMButton();
            else if (typeof(T) == typeof(RMDivider))
                _sectionsProp.GetArrayElementAtIndex(index).managedReferenceValue = new RMDivider();

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawSection(SerializedProperty sectionProp) {
            if (sectionProp.type == $"managedReference<{nameof(RMText)}>") {
                var text = sectionProp.FindPropertyRelative("<Text>k__BackingField").stringValue;
                var style = sectionProp.FindPropertyRelative("<Style>k__BackingField").enumValueIndex;

                switch ((TextStyle)style) {
                    case TextStyle.BigHeader:
                        GUILayout.Label(text, Styles.RMBigHeader, GUILayout.Width(EditorGUIUtility.currentViewWidth - 36));
                        break;
                    case TextStyle.Header:
                        GUILayout.Label(text, Styles.RMHeader, GUILayout.Width(EditorGUIUtility.currentViewWidth - 36));
                        break;
                    case TextStyle.Section:
                        GUILayout.Label(text, Styles.RMSection, GUILayout.Width(EditorGUIUtility.currentViewWidth - 36));
                        break;
                    case TextStyle.Normal:
                        GUILayout.Label(text, Styles.RMNormal, GUILayout.Width(EditorGUIUtility.currentViewWidth - 36));
                        break;
                    default:
                        return;
                }
            } else if (sectionProp.type == $"managedReference<{nameof(RMBulletList)}>") {
                var textsProp = sectionProp.FindPropertyRelative("<Texts>k__BackingField");
                var type = sectionProp.FindPropertyRelative("<Type>k__BackingField").enumValueIndex;

                switch ((BulletType)type) {
                    case BulletType.Dot:
                        for (var i = 0; i < textsProp.arraySize; i++) {
                            var text = textsProp.GetArrayElementAtIndex(i).stringValue;
                            GUILayout.Label(new GUIContent(text, Icons.RMBulletPointDot), Styles.RMBulletPoint, GUILayout.Width(EditorGUIUtility.currentViewWidth - 36));
                        }
                        break;
                    case BulletType.Romboid:
                        for (var i = 0; i < textsProp.arraySize; i++) {
                            var text = textsProp.GetArrayElementAtIndex(i).stringValue;
                            GUILayout.Label(new GUIContent(text, Icons.RMBulletPointRomboid), Styles.RMBulletPoint, GUILayout.Width(EditorGUIUtility.currentViewWidth - 36));
                        }
                        break;
                    case BulletType.Arrow:
                        for (var i = 0; i < textsProp.arraySize; i++) {
                            var text = textsProp.GetArrayElementAtIndex(i).stringValue;
                            GUILayout.Label(new GUIContent(text, Icons.RMBulletPointArrow), Styles.RMBulletPoint, GUILayout.Width(EditorGUIUtility.currentViewWidth - 36));
                        }
                        break;
                    default:
                        return;
                }
            } else if (sectionProp.type == $"managedReference<{nameof(RMTable)}>") {
                var columnsProp = sectionProp.FindPropertyRelative("<Columns>k__BackingField");

                var columnWidth = (EditorGUIUtility.currentViewWidth - 36) / columnsProp.arraySize;

                for (var i = 0; i < columnsProp.arraySize; i++) {
                    var ithProp = columnsProp.GetArrayElementAtIndex(i).FindPropertyRelative("<Texts>k__BackingField");

                    GUILayout.BeginHorizontal();
                    for (var j = 0; j < ithProp.arraySize; j++) {
                        var jthProp = ithProp.GetArrayElementAtIndex(j);
                        GUILayout.Label(jthProp.stringValue, Styles.RMTable, GUILayout.Width(columnWidth));
                    }
                    GUILayout.EndHorizontal();
                }
            } else if (sectionProp.type == $"managedReference<{nameof(RMImage)}>") {
                var texture = sectionProp.FindPropertyRelative("<Image>k__BackingField").objectReferenceValue as Texture;
                var type = sectionProp.FindPropertyRelative("<ResizeType>k__BackingField").enumValueIndex;
                var height = sectionProp.FindPropertyRelative("<FixedHeight>k__BackingField").intValue;

                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();

                switch ((ResizeType)type) {
                    case ResizeType.FixedHeight:
                        GUILayout.Label(texture, GUILayout.Height(height));
                        break;
                    case ResizeType.StretchHorizontally:
                        GUILayout.Label(texture);
                        break;
                    default:
                        return;
                }

                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            } else if (sectionProp.type == $"managedReference<{nameof(RMButton)}>") {
                var labelProp = sectionProp.FindPropertyRelative("<Label>k__BackingField");
                var linkProp = sectionProp.FindPropertyRelative("<Link>k__BackingField");

                if (GUILayout.Button(labelProp.stringValue, GUILayout.Width(EditorGUIUtility.currentViewWidth - 36)))
                    EditorUtility.OpenWithDefaultApp(linkProp.stringValue);
            } else if (sectionProp.type == $"managedReference<{nameof(RMDivider)}>") {
                var style = sectionProp.FindPropertyRelative("<Style>k__BackingField").enumValueIndex;

                switch ((DividerStyle)style) {
                    case DividerStyle.Space:
                        GUILayout.Space(Styles.RMHeader.fontSize);
                        break;
                    case DividerStyle.Separator:
                        GUILayout.Space(Styles.RMHeader.fontSize);
                        var lr = GUILayoutUtility.GetLastRect();
                        Handles.color = Color.black * .5f;
                        Handles.DrawLine(new Vector2(0f, lr.yMax), new Vector2(EditorGUIUtility.currentViewWidth, lr.yMax), .5f);
                        Handles.color = Color.white;
                        break;
                    default:
                        return;
                }
            }
        }
        #endregion
    }
}