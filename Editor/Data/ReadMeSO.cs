﻿using System.Collections.Generic;
using UnityEngine;

namespace Izinspector.Editors.Data {
    [CreateAssetMenu(menuName = "Read Me", fileName = "New Read Me", order = 0)]
    internal sealed class ReadMeSO : ScriptableObject {
        #region Public Variables
        [field: SerializeReference] internal List<RMSection> Sections { get; private set; } = new List<RMSection>();
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}