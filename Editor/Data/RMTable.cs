﻿using System.Collections.Generic;
using UnityEngine;

namespace Izinspector.Editors.Data {
    [System.Serializable]
    internal class RMTable : RMSection {
        [System.Serializable]
        internal class Row {
            [field: SerializeField] internal List<string> Texts { get; private set; } = new List<string>();
        }

        #region Public Variables
        [field: SerializeField] internal List<Row> Columns { get; private set; } = new List<Row>();
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}