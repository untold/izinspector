﻿using UnityEngine;

namespace Izinspector.Editors.Data {
    internal enum BulletType { Dot, Romboid, Arrow }

    [System.Serializable]
    internal class RMBulletList : RMSection {
        #region Public Variables
        [field: SerializeField] internal string[] Texts { get; private set; }
        [field: SerializeField] internal BulletType Type { get; private set; } = BulletType.Dot;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}