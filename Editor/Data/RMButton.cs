﻿using UnityEngine;

namespace Izinspector.Editors.Data {
    [System.Serializable]
    internal class RMButton : RMSection {
        #region Public Variables
        [field: SerializeField] internal string Label { get; private set; }
        [field: SerializeField] internal string Link { get; private set; }
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}