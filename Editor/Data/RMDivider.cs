﻿using UnityEngine;

namespace Izinspector.Editors.Data {
    internal enum DividerStyle { Space, Separator }

    [System.Serializable]
    internal class RMDivider : RMSection {
        #region Public Variables
        [field: SerializeField] internal DividerStyle Style { get; private set; } = DividerStyle.Space;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}