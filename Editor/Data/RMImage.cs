﻿using UnityEngine;

namespace Izinspector.Editors.Data {
    internal enum ResizeType { StretchHorizontally, FixedHeight }

    [System.Serializable]
    internal class RMImage : RMSection {
        #region Public Variables
        [field: SerializeField] internal Texture Image { get; private set; }
        [field: SerializeField] internal ResizeType ResizeType { get; private set; } = ResizeType.FixedHeight;
        [field: SerializeField, Min(16)] internal int FixedHeight { get; private set; } = 16;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}