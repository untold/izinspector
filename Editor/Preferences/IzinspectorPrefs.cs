﻿using UnityEditor;

namespace Izinspector.Editors.Preferences {
    internal class IzinspectorPrefs {
        #region Private Variables
        private static IzinspectorPrefs s_Instance;
        #endregion

        #region Constructors
        internal static IzinspectorPrefs Instance {
            get => s_Instance ??= new IzinspectorPrefs();
            set => s_Instance = value;
        }
        #endregion

        #region Properties
        internal static bool InspectMonoBehaviours {
            get => EditorPrefs.GetBool(PrefKeys.k_inspectMonoBehavioursKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_inspectMonoBehavioursKey, value);
        }
        internal static bool InspectScriptableObjects {
            get => EditorPrefs.GetBool(PrefKeys.k_inspectScriptableObjectsKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_inspectScriptableObjectsKey, value);
        }
        internal static bool InspectPingableComponent {
            get => EditorPrefs.GetBool(PrefKeys.k_inspectPingableComponentKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_inspectPingableComponentKey, value);
        }
        internal static bool ShowRequirements {
            get => EditorPrefs.GetBool(PrefKeys.k_showRequirementsKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_showRequirementsKey, value);
        }

        internal static bool ShowObjectStateOverlay {
            get => EditorPrefs.GetBool(PrefKeys.k_showObjectStateOverlayKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_showObjectStateOverlayKey, value);
        }
        internal static bool ShowIconOverlay {
            get => EditorPrefs.GetBool(PrefKeys.k_showIconOverlayKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_showIconOverlayKey, value);
        }
        internal static bool ShowLabelOverlay {
            get => EditorPrefs.GetBool(PrefKeys.k_showLabelOverlayKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_showLabelOverlayKey, value);
        }
        internal static bool ShowHeaderOverlay {
            get => EditorPrefs.GetBool(PrefKeys.k_showHeaderOverlayKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_showHeaderOverlayKey, value);
        }
        internal static bool ShowTreeEntryDepthOverlay {
            get => EditorPrefs.GetBool(PrefKeys.k_showTreeEntryDepthOverlayKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_showTreeEntryDepthOverlayKey, value);
        }

        internal static bool ShowFileSceneBuildOverlay {
            get => EditorPrefs.GetBool(PrefKeys.k_showFileSceneBuildOverlayKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_showFileSceneBuildOverlayKey, value);
        }
        internal static bool ShowFileTextureInfoOverlay {
            get => EditorPrefs.GetBool(PrefKeys.k_showFileTextureInfoOverlayKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_showFileTextureInfoOverlayKey, value);
        }

#if UNITY_2022_1_OR_NEWER
        internal static bool ShowDefaultPieActions {
            get => EditorPrefs.GetBool(PrefKeys.k_showDefaultPieActionsKey, true);
            set => EditorPrefs.SetBool(PrefKeys.k_showDefaultPieActionsKey, value);
        }
#endif
        #endregion
    }
}