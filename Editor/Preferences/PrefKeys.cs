﻿namespace Izinspector.Editors.Preferences {
    internal static class PrefKeys {
        #region Constants
        internal const string k_inspectMonoBehavioursKey = "Izinspector.Inspectors.MonoBehaviours";
        internal const string k_inspectScriptableObjectsKey = "Izinspector.Inspectors.ScriptableObjects";
        internal const string k_inspectPingableComponentKey = "Izinspector.Inspectors.DrawPingableComponent";
        internal const string k_showRequirementsKey = "Izinspector.Inspectors.ShowRequirements";

        internal const string k_showObjectStateOverlayKey = "Izinspector.Overlays.ObjectState";
        internal const string k_showIconOverlayKey = "Izinspector.Overlays.Icon";
        internal const string k_showLabelOverlayKey = "Izinspector.Overlays.Label";
        internal const string k_showHeaderOverlayKey = "Izinspector.Overlays.Header";
        internal const string k_showTreeEntryDepthOverlayKey = "Izinspector.Overlays.TreeEntryDepth";

        internal const string k_showFileSceneBuildOverlayKey = "Izinspector.Overlays.SceneBuild";
        internal const string k_showFileTextureInfoOverlayKey = "Izinspector.Overlays.TextureInfo";

#if UNITY_2022_1_OR_NEWER
        internal const string k_showDefaultPieActionsKey = "Izinspector.PieMenus.DefaultActions";
#endif
        #endregion
    }
}