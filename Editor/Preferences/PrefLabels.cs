﻿using UnityEngine;

namespace Izinspector.Editors.Preferences {
    internal static class PrefLabels {
        #region Properties
        internal static readonly GUIContent Inspector = new("Advanced Inspector");
        internal static readonly GUIContent Hierarchy = new("Hierarchy Overlays");
        internal static readonly GUIContent ProjectWin = new("Files Overlays");
#if UNITY_2022_1_OR_NEWER
        internal static readonly GUIContent PieMenus = new("Pie Menus");
#endif

        internal static readonly GUIContent InspectMonoBehaviours =
            new("MonoBehaviours",
                "Whether to use the custom inspector utility with MonoBehaviours.");
        internal static readonly GUIContent InspectScriptableObjects =
            new("ScriptableObjects",
                "Whether to use the custom inspector utility with ScriptableObjects.");
        internal static readonly GUIContent InspectPingableComponent =
            new("Pingable Component",
                "Whether to display the readonly field for the component to allow pinging it.");
        internal static readonly GUIContent ShowRequirements =
            new("Requirements",
                "Whether to display the warnings when required base components are missing.");

        internal static readonly GUIContent ShowObjectStateOverlay =
            new("Show Object State",
                "Whether to display the enabled/disabled state toggle.");
        internal static readonly GUIContent ShowIconOverlay =
            new("Show Special Icon",
                "Whether to display the icon of particular components before the item's name.");
        internal static readonly GUIContent ShowLabelOverlay =
            new("Show Label Highlight",
                "Whether to display an highlight on objects named with a #.");
        internal static readonly GUIContent ShowHeaderOverlay =
            new("Show Header (Divider)",
                "Whether to display an object named with --- as a header (divider).");
        internal static readonly GUIContent ShowTreeEntryDepthOverlay =
            new("Show Tree Entry Depth",
                "Whether to display lines next to objects to emphasize indentation.");

        internal static readonly GUIContent ShowFileSceneBuildOverlay =
            new("Show Include Scene in Build",
                "Whether to display a toggle on scene assets to include/exclude them from the build.");
        internal static readonly GUIContent ShowFileTextureInfoOverlay =
            new("Show Texture Infos",
                "Whether to display dimensions, size and compression infos on texture files.");

#if UNITY_2022_1_OR_NEWER
        internal static readonly GUIContent ShowPieDefaultActions =
            new("Show Default Pie Actions",
                "Whether to display the default actions for the SceneView Pie menu.");
#endif

        internal static readonly GUIContent Infos = new("These settings are local and will not change for the other developers in your team.");
        #endregion
    }
}