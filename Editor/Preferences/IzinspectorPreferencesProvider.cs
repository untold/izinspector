using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
#if UNITY_2022_1_OR_NEWER
using Izinspector.Editors.Tools.PieMenus.Utility;
#endif

namespace Izinspector.Editors.Preferences {
    internal class IzinspectorPreferencesProvider : SettingsProvider {
        #region Private Variables
        #endregion

        #region Constructors
        private IzinspectorPreferencesProvider() : base("Preferences/ItsTheConnection/Izinspector", SettingsScope.User) {
            keywords = new HashSet<string>(new[] { "Utility", "Tools", "Izinspector", "Inspector" });
        }
        #endregion

        #region Overrides
        public override bool HasSearchInterest(string searchContext) => true;

        public override void OnGUI(string searchContext) {
            GUILayout.Space(10);

            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.Space(10);
                EditorGUILayout.BeginVertical();
                {
                    EditorGUILayout.LabelField(PrefLabels.Inspector, EditorStyles.boldLabel);
                    IzinspectorPrefs.InspectMonoBehaviours = EditorGUILayout.Toggle(PrefLabels.InspectMonoBehaviours, IzinspectorPrefs.InspectMonoBehaviours);
                    IzinspectorPrefs.InspectScriptableObjects = EditorGUILayout.Toggle(PrefLabels.InspectScriptableObjects, IzinspectorPrefs.InspectScriptableObjects);
                    IzinspectorPrefs.InspectPingableComponent = EditorGUILayout.Toggle(PrefLabels.InspectPingableComponent, IzinspectorPrefs.InspectPingableComponent);
                    IzinspectorPrefs.ShowRequirements = EditorGUILayout.Toggle(PrefLabels.ShowRequirements, IzinspectorPrefs.ShowRequirements);

                    EditorGUILayout.Space(10);
                    EditorGUILayout.LabelField(PrefLabels.Hierarchy, EditorStyles.boldLabel);
                    IzinspectorPrefs.ShowObjectStateOverlay = EditorGUILayout.Toggle(PrefLabels.ShowObjectStateOverlay, IzinspectorPrefs.ShowObjectStateOverlay);
                    IzinspectorPrefs.ShowIconOverlay = EditorGUILayout.Toggle(PrefLabels.ShowIconOverlay, IzinspectorPrefs.ShowIconOverlay);
                    IzinspectorPrefs.ShowLabelOverlay = EditorGUILayout.Toggle(PrefLabels.ShowLabelOverlay, IzinspectorPrefs.ShowLabelOverlay);
                    IzinspectorPrefs.ShowHeaderOverlay = EditorGUILayout.Toggle(PrefLabels.ShowHeaderOverlay, IzinspectorPrefs.ShowHeaderOverlay);
                    IzinspectorPrefs.ShowTreeEntryDepthOverlay = EditorGUILayout.Toggle(PrefLabels.ShowTreeEntryDepthOverlay, IzinspectorPrefs.ShowTreeEntryDepthOverlay);

                    EditorGUILayout.Space(10);
                    EditorGUILayout.LabelField(PrefLabels.ProjectWin, EditorStyles.boldLabel);
                    IzinspectorPrefs.ShowFileSceneBuildOverlay = EditorGUILayout.Toggle(PrefLabels.ShowFileSceneBuildOverlay, IzinspectorPrefs.ShowFileSceneBuildOverlay);
                    IzinspectorPrefs.ShowFileTextureInfoOverlay = EditorGUILayout.Toggle(PrefLabels.ShowFileTextureInfoOverlay, IzinspectorPrefs.ShowFileTextureInfoOverlay);

#if UNITY_2022_1_OR_NEWER
                    EditorGUILayout.Space(10);
                    EditorGUILayout.LabelField(PrefLabels.PieMenus, EditorStyles.boldLabel);
                    EditorGUI.BeginChangeCheck();
                    IzinspectorPrefs.ShowDefaultPieActions = EditorGUILayout.Toggle(PrefLabels.ShowPieDefaultActions, IzinspectorPrefs.ShowDefaultPieActions);
                    if (EditorGUI.EndChangeCheck()) {
                        if (IzinspectorPrefs.ShowDefaultPieActions)
                            PieMenusAssembler.InjectDefaultActionsIntoMenus();
                        else
                            PieMenusAssembler.EjectDefaultActionsFromMenus();
                    }
#endif
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
        }

        public override void OnFooterBarGUI() {
            EditorGUILayout.LabelField(PrefLabels.Infos, EditorStyles.centeredGreyMiniLabel);
        }
        #endregion

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider() => new IzinspectorPreferencesProvider();
    }

}