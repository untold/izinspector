﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Izinspector.Editors.Preferences;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEngine;
namespace Izinspector.Editors {
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ScriptableObject), true)]
    public class ScriptableObjectCE : Editor {
        #region Public Variables
        #endregion

        #region Private Variables
        private FieldInfo[] _properties = new FieldInfo[0];
        private Dictionary<string, FieldInfo[]> _groupedProperties = new Dictionary<string, FieldInfo[]>();
        private FieldInfo[] _staticFields = new FieldInfo[0];
        private FieldInfo[] _actionFields = new FieldInfo[0];
        private FieldInfo[] _hiddenFields = new FieldInfo[0];
        private MethodInfo[] _animEventsMethods = new MethodInfo[0];
        private Dictionary<string, MethodInfo> _buttonsMethods = new Dictionary<string, MethodInfo>();

        private static bool _staticsFoldout = true;
        private static bool _actionsFoldout = false;
        private static bool _animEventsFoldout = false;
        private static Dictionary<string, bool> _groupsFoldouts = new Dictionary<string, bool>();

        private string _scriptAssetPath;
        #endregion

        #region Overrides
        private void OnEnable() {
            if (!IzinspectorPrefs.InspectScriptableObjects)
                return;

            try {
                if (!serializedObject.targetObject) {
                    Debug.LogWarning("Check if a script is missing...\n", target);
                    return;
                }
            } catch {
                return;
            }

            _properties = Reflector.CollectProperties(serializedObject);
            _groupedProperties = Reflector.CollectGroupedProperties(serializedObject);
            _staticFields = Reflector.CollectStaticFields(serializedObject);
            _actionFields = Reflector.CollectActionFields(serializedObject);
            _animEventsMethods = Reflector.CollectAnimationEventsMethods(serializedObject);
            _buttonsMethods = Reflector.CollectButtonsMethods(serializedObject);

            _groupsFoldouts = new Dictionary<string, bool>();
            foreach (var group in _groupedProperties)
                _groupsFoldouts.Add(@group.Key, true);

            var typeName = target.GetType().Name;
            var guid = AssetDatabase.FindAssets(typeName).FirstOrDefault();
            _scriptAssetPath = AssetDatabase.GUIDToAssetPath(guid);

            OnAfterEnabled();
        }

        public override void OnInspectorGUI () {
            if (!IzinspectorPrefs.InspectScriptableObjects) {
                base.OnInspectorGUI();
                return;
            }

            serializedObject.Update();

            #region GUI Here
            if (IzinspectorPrefs.InspectPingableComponent) {
                GUILayout.Space(-2);
                GUILayout.BeginHorizontal();
                {
                    GUILayout.FlexibleSpace();
                    if (GUILayout.Button("Show", Styles.CustomAssetLabelLeft))
                        EditorGUIUtility.PingObject(AssetDatabase.LoadMainAssetAtPath(_scriptAssetPath));
                    if (GUILayout.Button("Open", Styles.CustomAssetLabelRight))
                        AssetDatabase.OpenAsset(AssetDatabase.LoadMainAssetAtPath(_scriptAssetPath));
                }
                GUILayout.EndHorizontal();
                GUILayout.Space(2);
            }

            OnPrependInspectorGUI();

            foreach (var prop in _properties)
                EditorGUILayout.PropertyField(serializedObject.FindProperty(prop.Name), true);

            foreach (var group in _groupedProperties) {
                var gFold = _groupsFoldouts[group.Key];
                InspectorDrawer.DrawFoldoutAsComponent(ref gFold, group.Key, () => {
                    foreach (var prop in group.Value)
                        EditorGUILayout.PropertyField(serializedObject.FindProperty(prop.Name), true);
                });
                _groupsFoldouts[group.Key] = gFold;
            }

            if (_staticFields.Any()) {
                EditorGUILayout.Space();
                InspectorDrawer.DrawFoldoutAsList(ref _staticsFoldout, "Statics", () => {
                    foreach (var staticField in _staticFields)
                        InspectorDrawer.DrawStaticField(staticField, serializedObject.targetObject);
                });
            }

            if (_actionFields.Any()) {
                EditorGUILayout.Space();
                InspectorDrawer.DrawFoldoutAsList(ref _actionsFoldout, "Actions", () => {
                    foreach (var actionField in _actionFields)
                        InspectorDrawer.DrawAction(actionField, serializedObject.targetObject);
                });
            }

            if (_animEventsMethods.Any()) {
                EditorGUILayout.Space();
                InspectorDrawer.DrawFoldoutAsList(ref _animEventsFoldout, "Animation Events", () => {
                    foreach (var evMethod in _animEventsMethods)
                        InspectorDrawer.DrawAnimationEvent(evMethod);
                });
            }

            if (_buttonsMethods.Any()) {
                EditorGUILayout.Space();
                foreach (var button in _buttonsMethods) {
                    if (GUILayout.Button(button.Key))
                        button.Value.Invoke(serializedObject.targetObject, null);
                }
            }

            OnAppendInspectorGUI();
            #endregion

            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        protected virtual void OnAfterEnabled() {}

        protected virtual void OnPrependInspectorGUI() {}
        protected virtual void OnAppendInspectorGUI() {}
        #endregion
    }
}