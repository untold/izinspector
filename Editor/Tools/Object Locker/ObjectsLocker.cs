﻿using System.Linq;
using Izinspector.Runtime.Components;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Izinspector.Editors.Tools.ObjectLocker {
    [InitializeOnLoad]
    internal class ObjectsLocker {
        #region Private Variables
        private static Texture2D _lockIcon;
        #endregion

        #region Properties
        private static Texture2D LockIcon => _lockIcon ??= (Texture2D)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Object Locker/Lock.png");
        #endregion

        #region Constructors
        static ObjectsLocker() {
            EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyOverlay;
        }

        ~ObjectsLocker() {
            EditorApplication.hierarchyWindowItemOnGUI -= OnHierarchyOverlay;
        }
        #endregion

        #region Event Methods
        private static void OnHierarchyOverlay(int instanceId, Rect selectionRect) {
            var obj = EditorUtility.InstanceIDToObject(instanceId);

            if (obj is not GameObject go)
                return;

            if (!go.TryGetComponent(out LockedObject lockedObject))
                return;

            var iconRect = new Rect(selectionRect) {
                width = selectionRect.height,
            };
            GUI.DrawTexture(iconRect, LockIcon);
        }
        #endregion

        #region Menu Items
        [MenuItem("GameObject/Lock/On %L")]
        private static void LockObjects() {
            foreach (var gameObject in Selection.gameObjects) {
                if (gameObject.TryGetComponent(out LockedObject _))
                    return;

                gameObject.AddComponent<LockedObject>().hideFlags = HideFlags.HideInInspector;
            }
        }

        [MenuItem("GameObject/Lock/On", true)]
        private static bool LockObjectsValidator() => Selection.gameObjects.Any();

        [MenuItem("GameObject/Lock/Off #%L")]
        private static void UnlockObjects() {
            foreach (var gameObject in Selection.gameObjects) {
                if (!gameObject.TryGetComponent(out LockedObject lockedObject))
                    return;

                Object.DestroyImmediate(lockedObject);
            }
        }

        [MenuItem("GameObject/Lock/Off", true)]
        private static bool UnlockObjectsValidator() => Selection.gameObjects.Any(go => go.TryGetComponent(out LockedObject _));

        [MenuItem("GameObject/Lock/Off (All) &%L")]
        private static void UnlockAll() {
#if UNITY_2023_1_OR_NEWER
            foreach (var lockedObject in Object.FindObjectsByType<LockedObject>(FindObjectsInactive.Include, FindObjectsSortMode.None))
#else
            foreach (var lockedObject in Object.FindObjectsOfType<LockedObject>(true))
#endif
                Object.DestroyImmediate(lockedObject);
        }
        #endregion
    }
}