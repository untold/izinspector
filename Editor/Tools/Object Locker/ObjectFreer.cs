﻿using Izinspector.Runtime.Components;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Izinspector.Editors.Tools.ObjectLocker {
    internal static class ObjectFreer {
        #region Constants
        private const string k_progressWindowTitle = "Cleaning Locked Objects from {0}";
        private const string k_progressWindowDescription = "Removing all components related to the Locked Object tool to avoid build errors";
        #endregion

        #region Overrides
        [PostProcessScene(2)]
        internal static void CleanLockedObjects() {
            if (EditorApplication.isPlayingOrWillChangePlaymode)
                return;

            Debug.Log("Cleaning locked objects...\n");

            for (var i = 0; i < SceneManager.sceneCount; i++)
                RemoveLocksFromScene(SceneManager.GetSceneAt(i));
        }
        #endregion

        #region Private Methods
        private static void RemoveLocksFromScene(Scene scene) {
            EditorUtility.DisplayProgressBar(string.Format(k_progressWindowTitle, scene.name), k_progressWindowDescription, 0f);

            foreach (var go in scene.GetRootGameObjects()) {
                var snaps = go.GetComponentsInChildren<LockedObject>(true);
                var len = snaps.Length;
                Debug.Log($"Snaps found: {len}\n");
                for (var i = len - 1; i >= 0; i--) {
                    EditorUtility.DisplayProgressBar(string.Format(k_progressWindowTitle, scene.name), k_progressWindowDescription, 1f - ((float)i / len));
                    Object.DestroyImmediate(snaps[i]);
                }
            }

            EditorUtility.ClearProgressBar();
        }
        #endregion
    }
}