﻿using System.Linq;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Tools.PieMenus.Utility {
    [InitializeOnLoad]
    public class AssetLockStatusController {
        #region Constructors
        static AssetLockStatusController() {
            AssetsLocker.CollectAllItems();
            EditorApplication.projectWindowItemOnGUI += OnGUI;
        }

        ~AssetLockStatusController() {
            AssetsLocker.Reset();
            EditorApplication.projectWindowItemOnGUI -= OnGUI;
        }
        #endregion

        #region Event Methods
        private static void OnGUI(string guid, Rect selectionRect) {
            var path = AssetDatabase.GUIDToAssetPath(guid);
            if (!AssetsLocker.AllItems.TryGetValue(path, out var info))
                return;

            var r = new Rect(selectionRect) {
                x = selectionRect.x - 15f,
                y = selectionRect.y - 2f,
                width = 20f,
                height = 20f
            };
            GUI.Label(r, new GUIContent(Icons.LockIcon.image, $"This asset's path is used by: \n- {string.Join("\n- ", info.Select(t => t.Name))}"), "label");
        }
        #endregion
    }
}