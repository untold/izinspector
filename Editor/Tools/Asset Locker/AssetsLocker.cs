﻿#if UNITY_2022_1_OR_NEWER
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using Izinspector.Runtime.PropertyAttributes;

namespace Izinspector.Editors.Tools.PieMenus.Utility {
    internal static class AssetsLocker {
        #region Constants
        private static string[] ExcludedLibs { get; } = {
            "Unity.", "UnityEngine", "UnityEditor", "mscorlib", "netstandard", "unityplastic"
        };

        private static string[] ExcludedLibsRoots { get; } = {
            "Bee", "System", "Mono", "Microsoft", "JetBrains"
        };

        private const string k_progressTitle = "Collecting Locked Paths";
        private const string k_progressDescription = "Preparing to search locked assets...";
        private const string k_progressDescriptionFormat = "Searching for locked paths in \'{0}\'";
        #endregion

        #region Properties
        internal static Dictionary<string, List<Type>> AllItems { get; private set; }
        #endregion

        #region Public Methods
        internal static void Reset() => AllItems = null;

        internal static void CollectAllItems() {
            if (AllItems != null)
                return;

            ShowProgressStart();

            InitItems();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(IsValidAssembly).ToArray();
            var assemblyCount = (float)assemblies.Length;
            var i = -1;

            foreach (var assembly in assemblies) {
                var assemblyName = assembly.GetName().Name;

                ShowProgress(assemblyName, ++i / assemblyCount);

                foreach (var type in assembly.GetTypes()) {
                    var attr = type.GetCustomAttribute<LockAssetAttribute>();

                    if (attr == null)
                        continue;

                    foreach (var path in attr.Paths) {
                        if (AllItems!.TryGetValue(path, out var types))
                            types.Add(type);
                        else
                            AllItems.Add(path, new() { type });
                    }
                }
            }

            ShowProgressEnd();
        }
        #endregion

        #region Private Methods
        private static void InitItems() => AllItems = new();

        private static bool IsValidAssembly(Assembly assembly) {
            var name = assembly.GetName().Name;

            if (ExcludedLibs.Any(l => name.Contains(l)))
                return false;

            return !ExcludedLibsRoots.Any(l => name.StartsWith(l));
        }

        private static void ShowProgressStart() => EditorUtility.DisplayProgressBar(k_progressTitle, k_progressDescription, 0f);
        private static void ShowProgress(string assemblyName, float progress) => EditorUtility.DisplayProgressBar(k_progressTitle, string.Format(k_progressDescriptionFormat, assemblyName), progress);
        private static void ShowProgressEnd() => EditorUtility.ClearProgressBar();
        #endregion
    }
}
#endif