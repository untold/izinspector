﻿using System;
using Izinspector.Editors.Tools.TextureCombiner.Data;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Object = UnityEngine.Object;

namespace Izinspector.Editors.Tools.TextureCombiner.Elements {
    internal class TextureInfoElement : VisualElement {
        #region Properties
        public TextureInfo Data { get; set; }
        #endregion

        #region Constructors
        internal TextureInfoElement(ColorChannel channel) {
            style.height = 48f;
            style.minHeight = 48f;
            style.maxHeight = 48f;

            Data = new TextureInfo() {
                Channel = channel
            };

            var texField = new ObjectField("Texture") {
                allowSceneObjects = false,
                objectType = typeof(Texture),
                value = Data.Texture,
                style = {
                    position = Position.Absolute,
                    left = 4f,
                    top = 4f,
                    width = 48f - 8f,
                    bottom = 4f,
                },
            };
            Add(texField);

            var verticalLayout = new VisualElement() {
                style = {
                    position = Position.Absolute,
                    left = 48f,
                    top = 4f,
                    right = 4f,
                    bottom = 4f,
                    alignItems = Align.Stretch,
                    alignContent = Align.Stretch,
                    justifyContent = Justify.FlexEnd,
                }
            };

            var channelField = new EnumField("Source Channel", Data.Channel);
            verticalLayout.Add(channelField);

            var spaceField = new EnumField("Color Space", Data.ColorSpace);
            verticalLayout.Add(spaceField);

            texField.RegisterValueChangedCallback(OnTextureChanged);
            channelField.RegisterValueChangedCallback(OnChannelChanged);
            spaceField.RegisterValueChangedCallback(OnColorSpaceChanged);
        }
        #endregion

        #region Private Methods
        private void OnTextureChanged(ChangeEvent<Object> evt) => Data.Texture = (Texture)evt.newValue;

        private void OnChannelChanged(ChangeEvent<Enum> evt) => Data.Channel = (ColorChannel)evt.newValue;
        private void OnColorSpaceChanged(ChangeEvent<Enum> evt) => Data.ColorSpace = (ColorSpace)evt.newValue;
        #endregion
    }
}