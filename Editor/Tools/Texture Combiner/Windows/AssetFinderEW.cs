﻿using Izinspector.Editors.Tools.TextureCombiner.Data;
using Izinspector.Editors.Tools.TextureCombiner.Elements;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;

namespace Izinspector.Editors.Tools.TextureCombiner.Windows {
    internal class TextureCombinerEW : EditorWindow {
        #region Private Variables
        private TextureInfoElement _redTexture, _greenTexture, _blueTexture, _alphaTexture;
        #endregion

        #region EditorWindow Callbacks
        [MenuItem("Tools/Texture Combiner")]
        internal static void ShowWindow () {
            var win = GetWindow<TextureCombinerEW>("Texture Combiner");
            win.minSize = new Vector2(800, 200);

            win.Show();
        }

        private void CreateGUI() {
            var toolbar = new Toolbar();
            rootVisualElement.Add(toolbar);

            _redTexture = new TextureInfoElement(ColorChannel.R);
            rootVisualElement.Add(_redTexture);
            _greenTexture = new TextureInfoElement(ColorChannel.G);
            rootVisualElement.Add(_greenTexture);
            _blueTexture = new TextureInfoElement(ColorChannel.B);
            rootVisualElement.Add(_blueTexture);
            _alphaTexture = new TextureInfoElement(ColorChannel.A);
            rootVisualElement.Add(_alphaTexture);
        }
        #endregion
    }
}