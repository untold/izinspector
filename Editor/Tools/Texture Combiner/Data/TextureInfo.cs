﻿using UnityEngine;

namespace Izinspector.Editors.Tools.TextureCombiner.Data {
    internal enum ColorChannel { R, G, B, A }

    internal class TextureInfo {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        public Texture Texture { get; set; }
        public ColorSpace ColorSpace { get; set; } = ColorSpace.Linear;
        public ColorChannel Channel { get; set; } = ColorChannel.R;
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}