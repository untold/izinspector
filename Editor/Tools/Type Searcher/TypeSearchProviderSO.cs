using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Izinspector.Editors.Tools.TypeSearcher {
    internal delegate void TypeChosenEventHandler(Type type);

    internal sealed class TypeSearchProviderSO : ScriptableObject, ISearchWindowProvider {
        #region Private Variables
        private readonly List<Type> _types = new();

        private TypeChosenEventHandler _onTypeChosen;
        #endregion

        #region ScriptableObject Callbacks
        // private void OnDestroy() => Debug.Log("Clearing...\n");
        #endregion

        #region ISearchWindowProvider
        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context) {
            var entries = new List<SearchTreeEntry>();

            var root = new SearchTreeGroupEntry(new("All Types"));
            entries.Add(root);

            var scriptIcon = EditorGUIUtility.IconContent("cs Script Icon").image;
            _types.ForEach(t => {
                var label = new GUIContent(t?.Name ?? "None", t != null ? scriptIcon : null);
                entries.Add(new SearchTreeEntry(label) {
                    level = 1,
                    userData = t
                });
            });

            return entries;
        }

        public bool OnSelectEntry(SearchTreeEntry searchTreeEntry, SearchWindowContext context) {
            _onTypeChosen?.Invoke((Type)searchTreeEntry.userData);
            return true;
        }
        #endregion

        #region Public Methods
        internal void InitializeType(Type type) {
            Debug.Log($"Type filter: {type.Name}\n");
            var localProjectPath = Application.dataPath[..^"/Assets".Length];
            var localAssembliesPath = Path.Combine(localProjectPath, "Library/ScriptAssemblies");
            var cSharpAsmPath = Path.Combine(localAssembliesPath, "Assembly-CSharp.dll");

            var cSharpAsm = Assembly.LoadFile(cSharpAsmPath);

            _types.Clear();
            _types.Add(null);
            _types.AddRange(cSharpAsm.GetTypes().Where(t => t.IsSubclassOf(type)));
        }

        internal void LinkRequest(TypeChosenEventHandler callback) => _onTypeChosen = callback;
        #endregion
    }
}