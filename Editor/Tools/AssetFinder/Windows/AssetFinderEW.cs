﻿using System.Collections.Generic;
using Izinspector.Editors.Tools.AssetFinder.Data;
using Izinspector.Editors.Tools.AssetFinder.Elements;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;

namespace Izinspector.Editors.Tools.AssetFinder.Windows {
    internal class AssetFinderEW : EditorWindow {
        #region Private Variables
        private AssetsListView _listView;
        #endregion

        #region EditorWindow Callbacks
        public static void ShowWindow (List<AssetInfo> objects) {
            var win = GetWindow<AssetFinderEW>("Reference Finder");
            win.minSize = new Vector2(721, 100);

            objects.Sort((o1, o2) => string.CompareOrdinal(o1.Path, o2.Path));
            win._listView.Assets = objects;

            win.Show();
        }

        private void CreateGUI() {
            var toolbar = new Toolbar();
            rootVisualElement.Add(toolbar);

            _listView = new AssetsListView();
            rootVisualElement.Add(_listView);
        }
        #endregion
    }
}