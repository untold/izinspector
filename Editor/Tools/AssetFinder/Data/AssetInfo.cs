﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Tools.AssetFinder.Data {
    internal class AssetInfo {
        #region Properties
        public Object Object { get; set; }
        public string Path { get; set; }
        public FileInfo File { get; set; }

        public bool IsScene => Object is SceneAsset;
        #endregion

        #region Public Methods
        public void Select() => Selection.SetActiveObjectWithContext(Object, null);
        #endregion
    }
}