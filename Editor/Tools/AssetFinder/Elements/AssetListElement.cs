﻿using System.IO;
using Izinspector.Editors.Tools.AssetFinder.Data;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.AssetFinder.Elements {
    internal class AssetListElement : VisualElement {
        #region Private Variables
        private readonly Image _typeIconImage;
        private readonly Label _nameLabel;
        private readonly Label _pathLabel;
        private readonly Label _sizeLabel;
        #endregion

        #region Properties
        internal AssetInfo Info { get; private set; }
        #endregion

        #region Constructors
        internal AssetListElement() {
            _typeIconImage = new Image() {
                style = {
                    position = Position.Absolute,
                    left = 4f,
                    top = 2.5f,
                    bottom = 2.5f,
                    width = 16f,
                    height = 16f,
                    marginLeft = 2.5f,
                    marginRight = 2.5f,
                }
            };
            Add(_typeIconImage);

            _nameLabel = new Label() {
                style = {
                    position = Position.Absolute,
                    left = 21f + 4f + 4f,
                    top = 0f,
                    bottom = 0f,
                    width = Length.Percent(25),
                    paddingLeft = 4f,
                    paddingRight = 4f,
                    unityTextAlign = TextAnchor.MiddleLeft,
                    unityOverflowClipBox = OverflowClipBox.ContentBox,
                    textOverflow = TextOverflow.Ellipsis,
                    overflow = Overflow.Hidden,
                    borderLeftWidth = 1f,
                    borderLeftColor = Colors.UnityToolbarLines,
                }
            };
            Add(_nameLabel);

            _pathLabel = new Label() {
                style = {
                    position = Position.Absolute,
                    left = Length.Percent(25),
                    top = 0f,
                    bottom = 0f,
                    right = 80f + 4f + 4f,
                    marginLeft = 29f,
                    paddingLeft = 4f,
                    paddingRight = 4f,
                    unityTextAlign = TextAnchor.MiddleLeft,
                    unityOverflowClipBox = OverflowClipBox.ContentBox,
                    textOverflow = TextOverflow.Ellipsis,
                    overflow = Overflow.Hidden,
                    borderLeftWidth = 1f,
                    borderLeftColor = Colors.UnityToolbarLines,
                }
            };
            Add(_pathLabel);

            _sizeLabel = new Label() {
                style = {
                    position = Position.Absolute,
                    right = 4f,
                    top = 0f,
                    bottom = 0f,
                    width = 80f,
                    paddingLeft = 4f,
                    paddingRight = 4f,
                    unityTextAlign = TextAnchor.MiddleRight,
                    color = Color.gray,
                    borderLeftWidth = 1f,
                    borderLeftColor = Colors.UnityToolbarLines,
                }
            };
            Add(_sizeLabel);

            this.StretchToParentSize();
        }
        #endregion

        #region Public Methods
        internal void SetAsset(AssetInfo info) {
            Info = info;

            _typeIconImage.image = AssetDatabase.GetCachedIcon(Info.Path);
            _nameLabel.text = Path.GetFileNameWithoutExtension(Info.Path);
            _pathLabel.text = Info.Path;
            _sizeLabel.text = Info.File.Length switch {
                < 1_000 => $"{Info.File.Length} kB",
                < 1_000_000 => $"{Info.File.Length / 1_000f:F} MB",
                _ => $"{Info.File.Length / 1_000_000f:F} GB",
            };
        }
        #endregion
    }
}