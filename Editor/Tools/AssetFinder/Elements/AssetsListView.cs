﻿#if UNITY_2022 || UNITY_2023
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Izinspector.Editors.Tools.AssetFinder.Data;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.AssetFinder.Elements {
    internal class AssetsListView : MultiColumnListView {
        #region Properties
        public List<AssetInfo> Assets { set => itemsSource = value; }
        #endregion

        #region Constructors
        internal AssetsListView() {
            this.StretchToParentSize();
            style.top = 21f;

            selectionType = SelectionType.Single;

            columns.Add(new Column() {
                width = 36f,
                minWidth = 36f,
                maxWidth = 36f,
                icon = Background.FromTexture2D((Texture2D)EditorGUIUtility.IconContent("d_FilterByType").image),
                resizable = false,
                sortable = false,
                makeCell = MakeIconCell,
                bindCell = BindIconCell,
            });

            columns.Add(new Column() {
                width = 200f,
                title = "Name",
                minWidth = 100f,
                makeCell = MakeNameCell,
                bindCell = BindNameCell,
            });

            columns.Add(new Column() {
                width = 400f,
                title = "Path",
                minWidth = 100f,
                makeCell = MakePathCell,
                bindCell = BindPathCell,
            });

            columns.Add(new Column() {
                width = 100f,
                title = "Size",
                minWidth = 80f,
                makeCell = MakeSizeCell,
                bindCell = BindSizeCell,
            });

            showAlternatingRowBackgrounds = AlternatingRowBackground.ContentOnly;

            selectionChanged += OnSelectItems;

            fixedItemHeight = 21f;
        }
        #endregion

        #region Private Methods
        private VisualElement MakeIconCell() => new Image() {
            style = {
                position = Position.Absolute,
                left = 10.5f,
                right = 10.5f,
                width = 16f,
                top = 2.5f,
                bottom = 2.5f,
            }
        };

        private void BindIconCell(VisualElement element, int index) {
            var img = (Image)element;
            var item = (AssetInfo)itemsSource[index];

            img.image = AssetDatabase.GetCachedIcon(item.Path);
        }

        private VisualElement MakePathCell() => new Label() {
            style = {
                top = 2.5f,
                bottom = 2.5f,
                unityOverflowClipBox = OverflowClipBox.ContentBox,
                textOverflow = TextOverflow.Ellipsis,
                overflow = Overflow.Hidden,
            }
        };

        private void BindPathCell(VisualElement element, int index) {
            var label = (Label)element;
            var item = (AssetInfo)itemsSource[index];

            label.text = item.Path;
        }

        private VisualElement MakeNameCell() => new Label() {
            style = {
                top = 2.5f,
                bottom = 2.5f,
                unityOverflowClipBox = OverflowClipBox.ContentBox,
                textOverflow = TextOverflow.Ellipsis,
                overflow = Overflow.Hidden,
            }
        };

        private void BindNameCell(VisualElement element, int index) {
            var label = (Label)element;
            var item = (AssetInfo)itemsSource[index];

            label.text = Path.GetFileNameWithoutExtension(item.Path);
        }

        private VisualElement MakeSizeCell() => new Label() {
            style = {
                top = 2.5f,
                bottom = 2.5f,
                color = Color.gray,
                unityTextAlign = TextAnchor.MiddleRight,
            }
        };

        private void BindSizeCell(VisualElement element, int index) {
            var label = (Label)element;
            var item = (AssetInfo)itemsSource[index];

            label.text = item.File.Length switch {
                < 1_000 => $"{item.File.Length} kB",
                < 1_000_000 => $"{item.File.Length / 1_000f:F} MB",
                _ => $"{item.File.Length / 1_000_000f:F} GB",
            };
        }

        private void OnSelectItems(IEnumerable<object> obj) {
            if (obj == null)
                return;

            var list = obj.ToList();
            if (list.Count <= 0)
                return;

            (list[0] as AssetInfo)?.Select();
        }
        #endregion
    }
}
#else
using System.Collections.Generic;
using System.Linq;
using Izinspector.Editors.Tools.AssetFinder.Data;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.AssetFinder.Elements {
    internal class AssetsListView : ListView {
        #region Properties
        public List<AssetInfo> Assets { set => itemsSource = value; }
        #endregion

        #region Constructors
        internal AssetsListView() {
            this.StretchToParentSize();
            style.top = 21f;

            selectionType = SelectionType.Single;

            makeItem = MakeItem;
            bindItem = BindItem;

            showAlternatingRowBackgrounds = AlternatingRowBackground.ContentOnly;

#if UNITY_2023_1_OR_NEWER
            selectionChanged += OnSelectItems;
#else
            onSelectionChange += OnSelectItems;
#endif

            fixedItemHeight = 21f;
        }
        #endregion

        #region Private Methods
        private VisualElement MakeItem() => new AssetListElement();

        private void BindItem(VisualElement element, int index) {
            var assetElement = (AssetListElement)element;
            var info = (AssetInfo)itemsSource[index];

            assetElement.SetAsset(info);
        }

        private void OnSelectItems(IEnumerable<object> obj) {
            if (obj == null)
                return;

            var list = obj.ToList();
            if (list.Count <= 0)
                return;

            (list[0] as AssetInfo)?.Select();
        }
        #endregion
    }
}
#endif