﻿using System;
using Izinspector.Editors.Tools.AssetFinder.Windows;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Tools.AssetFinder.Utility {
    internal static class MenuItemFunctions {
        #region Public Methods
        // SCRIPT USAGE FINDER -------------------------------------------------------------------

        [MenuItem("Assets/Find/Script Usages")]
        private static void FindUsagesInProject() {
            var monoScript = (Selection.activeObject as MonoScript)!;
            try {
                var type = monoScript.GetClass();
                if (!type.IsSubclassOf(typeof(MonoBehaviour))) {
                    Debug.LogWarning("The script does not inherit from MonoBehaviour\n", monoScript);
                    return;
                }

                var objects = AssetsFinder.GetPrefabsWithComponent(type);
                if (objects is not { Count: > 0 }) {
                    EditorUtility.DisplayDialog("Script Usages Finder", $"No usages of the type \"{type.Name}\" were found", "Ok");
                    return;
                }

                AssetFinderEW.ShowWindow(objects);
            } catch {
                Debug.LogWarning("The script doesn't contain a valid type\n", monoScript);
            }
        }

        [MenuItem("Assets/Find/Script Usages", true)]
        private static bool FindUsagesInProjectValidator() => Selection.activeObject is MonoScript;

        // REFERENCE FINDER ----------------------------------------------------------------------

        [MenuItem("Assets/Find/References")]
        private static void FindReferencesInProject() {
            var obj = Selection.activeObject;
            try {
                var objects = AssetsFinder.GetPrefabsDependant(obj);
                if (objects is not { Count: > 0 }) {
                    EditorUtility.DisplayDialog("References Finder", $"No prefab referencing \"{obj.name}\" was found", "Ok");
                    return;
                }

                AssetFinderEW.ShowWindow(objects);
            } catch {
                Debug.LogWarning("Something went wrong while searching for references\n", obj);
            }
        }

        [MenuItem("Assets/Find/References", true)]
        private static bool FindReferencesInProjectValidator() => Selection.activeObject;

        // REFERENCE FINDER ----------------------------------------------------------------------

        [MenuItem("Assets/Find/All References")]
        private static void FindAllReferencesInProject() {
            var obj = Selection.activeObject;
            try {
                var objects = AssetsFinder.GetAllReferences(obj);
                if (objects is not { Count: > 0 }) {
                    EditorUtility.DisplayDialog("References Finder", $"No assets referencing \"{obj.name}\" was found", "Ok");
                    return;
                }

                AssetFinderEW.ShowWindow(objects);
            } catch {
                Debug.LogWarning("Something went wrong while searching for references\n", obj);
            }
        }

        [MenuItem("Assets/Find/All References", true)]
        private static bool FindAllReferencesInProjectValidator() => Selection.activeObject;

        [MenuItem("Assets/Find/All References (Iterative)")]
        private static void FindAllReferencesInProjectIterative() {
            var obj = Selection.activeObject;
            try {
                var objects = AssetsFinder.GetAllReferencesIterative(obj);
                if (objects is not { Count: > 0 }) {
                    EditorUtility.DisplayDialog("References Finder", $"No assets referencing \"{obj.name}\" was found", "Ok");
                    return;
                }

                AssetFinderEW.ShowWindow(objects);
            } catch {
                Debug.LogWarning("Something went wrong while searching for references\n", obj);
            }
        }

        [MenuItem("Assets/Find/All References (Iterative)", true)]
        private static bool FindAllReferencesInProjectIterativeValidator() => Selection.activeObject;
        #endregion
    }
}