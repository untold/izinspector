﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Izinspector.Editors.Tools.AssetFinder.Data;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Izinspector.Editors.Tools.AssetFinder.Utility {
    internal static class AssetsFinder {
        #region Constants
        private const string k_scriptsUsagesFinder = "Script Usages Finder";
        private const string k_referenceFinder = "References Finder";

        private const string k_collectingPrefabs = "Collecting all prefabs...";
        private const string k_noPrefabsFound = "No prefab was found inside the Assets";

        private const string k_ok = "Ok";
        #endregion

        #region Public Methods
        internal static List<AssetInfo> GetPrefabsWithComponent(Type type) {
            EditorUtility.DisplayProgressBar(k_scriptsUsagesFinder, k_collectingPrefabs, 0f);

            var prefabsGuids = AssetDatabase.FindAssets("t:prefab");
            if (prefabsGuids.Length <= 0) {
                EditorUtility.ClearProgressBar();
                EditorUtility.DisplayDialog(k_scriptsUsagesFinder, k_noPrefabsFound, k_ok);
                return null;
            }

            var total = (float)prefabsGuids.Length;
            var i = 0;

            var paths = prefabsGuids.Select(guid => {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                EditorUtility.DisplayProgressBar("Script Usages Finder", $"Loading: \"{path}\"", ++i / total);
                return path;
            });

            var objects = new List<AssetInfo>();

            i = 0;
            foreach (var path in paths) {
                EditorUtility.DisplayProgressBar("Script Usages Finder", $"Checking: \"{Path.GetFileNameWithoutExtension(path)}\"", ++i / total);
                var asset = AssetDatabase.LoadMainAssetAtPath(path);
                if (asset is not GameObject go)
                    continue;

                if (!go.GetComponentInChildren(type))
                    continue;

                objects.Add(new AssetInfo {
                    Path = path,
                    Object = go,
                    File = new FileInfo(path)
                });
            }

            EditorUtility.ClearProgressBar();

            return objects;
        }

        internal static List<AssetInfo> GetPrefabsDependant(Object dependency) {
            if (!AssetDatabase.TryGetGUIDAndLocalFileIdentifier(dependency, out var objGuid, out long objId))
                return null;

            EditorUtility.DisplayProgressBar(k_referenceFinder, k_collectingPrefabs, 0f);

            var prefabsGuids = AssetDatabase.FindAssets("t:prefab");
            if (prefabsGuids.Length <= 0) {
                EditorUtility.ClearProgressBar();
                EditorUtility.DisplayDialog(k_referenceFinder, k_noPrefabsFound, k_ok);
                return null;
            }

            var total = (float)prefabsGuids.Length;
            var i = 0;

            var paths = prefabsGuids.Select(guid => {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                EditorUtility.DisplayProgressBar(k_referenceFinder, $"Loading: \"{path}\"", ++i / total);
                return path;
            });

            var objects = new List<AssetInfo>();

            i = 0;
            foreach (var path in paths) {
                EditorUtility.DisplayProgressBar(k_referenceFinder, $"Checking: \"{Path.GetFileNameWithoutExtension(path)}\"", ++i / total);

                var fileContents = File.ReadAllText(path);
                if (!fileContents.Contains(objGuid))
                    continue;

                objects.Add(new AssetInfo {
                    Path = path,
                    Object = AssetDatabase.LoadMainAssetAtPath(path),
                    File = new FileInfo(path)
                });
            }

            EditorUtility.ClearProgressBar();

            return objects;
        }

        internal static List<AssetInfo> GetAllReferences(Object dependency, in HashSet<string> alreadySearched = null) {
            if (!AssetDatabase.TryGetGUIDAndLocalFileIdentifier(dependency, out var objGuid, out long objId))
                return null;

            EditorUtility.DisplayProgressBar(k_referenceFinder, k_collectingPrefabs, 0f);

            var prefabsGuids = AssetDatabase.FindAssets("t:prefab t:ScriptableObject t:scene");
            if (alreadySearched != null && prefabsGuids.Length > 0)
                prefabsGuids = prefabsGuids.Except(alreadySearched).ToArray();

            if (prefabsGuids.Length <= 0) {
                EditorUtility.ClearProgressBar();
                EditorUtility.DisplayDialog(k_referenceFinder, k_noPrefabsFound, k_ok);
                return null;
            }

            var total = (float)prefabsGuids.Length;
            var i = 0;

            var paths = prefabsGuids.Select(guid => {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                EditorUtility.DisplayProgressBar(k_referenceFinder, $"Loading: \"{path}\"", ++i / total);
                return path;
            });

            var objects = new List<AssetInfo>();

            i = 0;
            foreach (var path in paths) {
                EditorUtility.DisplayProgressBar(k_referenceFinder, $"Checking: \"{Path.GetFileNameWithoutExtension(path)}\"", ++i / total);

                var fileContents = File.ReadAllText(path);
                if (!fileContents.Contains(objGuid))
                    continue;

                objects.Add(new AssetInfo {
                    Path = path,
                    Object = AssetDatabase.LoadMainAssetAtPath(path),
                    File = new FileInfo(path)
                });
            }

            EditorUtility.ClearProgressBar();

            return objects;
        }

        internal static List<AssetInfo> GetAllReferencesIterative(Object dependency) {
            if (!AssetDatabase.TryGetGUIDAndLocalFileIdentifier(dependency, out var objGuid, out long objId))
                return null;

            var objects = new List<AssetInfo>();
            var guids = new HashSet<string>();

            var newRefs = GetAllReferences(dependency, in guids);
            while (newRefs is { Count: > 0 }) {
                objects.AddRange(newRefs);

                var tempRefs = new List<AssetInfo>(newRefs);
                newRefs.Clear();
                foreach (var assetInfo in tempRefs) {
                    if (assetInfo.IsScene)
                        continue;

                    newRefs = GetAllReferences(assetInfo.Object, in guids);
                }
            }

            return objects;
        }
        #endregion
    }
}