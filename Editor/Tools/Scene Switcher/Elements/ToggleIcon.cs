﻿using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.SceneSwitcher.Elements {
    internal class ToggleIcon : VisualElement {
        #region Public Variables
        internal event System.Action<bool> OnValueChangedEvent; 
        #endregion

        #region Private Variables
        private readonly SerializedProperty _property;

        private readonly Image _icon;
        private readonly Texture _onIcon;
        private readonly Texture _offIcon;
        #endregion

        #region Constructors
        internal ToggleIcon(SerializedProperty property, Texture onIcon, Texture offIcon) {
            _property = property;
            _onIcon = onIcon;
            _offIcon = offIcon;

            style.width = 21f;

            _icon = new Image() {
                image = property.boolValue ? onIcon : offIcon,
                style = {
                    position = Position.Absolute,
                    left = 2.5f,
                    top = 2.5f,
                    right = 2.5f,
                    bottom = 2.5f,
                }
            };
            Add(_icon);

            this.Bind(property.serializedObject);
            this.TrackPropertyValue(property, OnValueChanged);

            RegisterCallback<ClickEvent>(OnClick);
        }
        #endregion

        #region Event Methods
        private void OnClick(ClickEvent evt) {
            evt.StopImmediatePropagation();

            _property.serializedObject.Update();
            _property.boolValue = !_property.boolValue;
            _property.serializedObject.ApplyModifiedPropertiesWithoutUndo();
        }

        private void OnValueChanged(SerializedProperty prop) {
            _icon.image = prop.boolValue ? _onIcon : _offIcon;
            OnValueChangedEvent?.Invoke(prop.boolValue);
        }
        #endregion
    }
}