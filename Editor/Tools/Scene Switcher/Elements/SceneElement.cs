﻿using System;
using System.Text.RegularExpressions;
using Izinspector.Editors.Tools.SceneSwitcher.Data;
using Izinspector.Editors.Tools.SceneSwitcher.Utility;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using static UnityEngine.UIElements.DropdownMenuAction;

namespace Izinspector.Editors.Tools.SceneSwitcher.Elements {
    internal class SceneElement : VisualElement, ISelectable {
        #region Public Variables
        #endregion

        #region Private Variables
        private ISelection _parentSelection;
        private bool _isOdd;
        private bool _isSelected;
        private DateTime _clickTime;

        private SerializedProperty _sceneInfoProperty;
        private FilterOuts _filters;
        private string _filterText;

        private readonly VisualElement _openIndicator;
        private readonly VisualElement _leftBackground;
        private readonly Label _label;
        private readonly Image _buildToggle;
        #endregion

        #region Properties
        public FilterOuts Filters {
            get => _filters;
            set {
                _filters = value;
                OnFiltersChanged();
            }
        }

        internal string FilterText {
            get => _filterText;
            set {
                _filterText = value;
                OnFilterTextChanged();
            }
        }
        #endregion

        #region Constructors
        internal SceneElement(SerializedProperty sceneInfoProperty, ISelection parentSelection, bool isOdd) {
            _parentSelection = parentSelection;
            _isOdd = isOdd;

            _sceneInfoProperty = sceneInfoProperty;
            _filters = (FilterOuts)_sceneInfoProperty.serializedObject.FindProperty("<Filters>k__BackingField").enumValueFlag;

            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorSceneManager.sceneClosed += OnSceneClosed;

            style.flexDirection = FlexDirection.Row;
            style.alignItems = Align.FlexStart;
            style.alignContent = Align.FlexStart;
            style.flexGrow = 1f;
            style.height = 21f;
            style.backgroundColor = _isOdd ? Colors.UnityLightBackground : Color.clear;

            _leftBackground = new VisualElement() {
                style = {
                    position = Position.Absolute,
                    top = 0f,
                    bottom = 0f,
                    width = 42f + 6f,
                    backgroundColor = Colors.UnityDarkBackground,
                }
            };
            Add(_leftBackground);

            var sceneNameProp = _sceneInfoProperty.FindPropertyRelative("<SceneName>k__BackingField");
            var isSceneOpened = SceneUtils.IsSceneOpened(sceneNameProp.stringValue);
            _openIndicator = new VisualElement() {
                style = {
                    position = Position.Absolute,
                    top = 0f,
                    bottom = 0f,
                    width = 2f,
                    backgroundColor = Colors.UnityGreen,
                },
                visible = isSceneOpened,
            };
            Add(_openIndicator);

            var faveProp = _sceneInfoProperty.FindPropertyRelative("<IsFavorite>k__BackingField");
            var toggleFave = new ToggleIcon(faveProp, Icons.StarOn, Icons.StarOff) {
                style = {
                    position = Position.Absolute,
                    left = 2f,
                    height = 21f,
                    width = 21f,
                }
            };
            toggleFave.OnValueChangedEvent += TrackFaveValueChanged;
            Add(toggleFave);

            var hideProp = _sceneInfoProperty.FindPropertyRelative("<IsHidden>k__BackingField");
            var toggleHide = new ToggleIcon(hideProp, Icons.Hide, Icons.Show) {
                style = {
                    position = Position.Absolute,
                    left = 2 + 21f,
                    height = 21f,
                    width = 21f,
                }
            };
            toggleHide.OnValueChangedEvent += TrackHiddenValueChanged;
            Add(toggleHide);

            _label = new Label(sceneNameProp.stringValue) {
                style = {
                    unityTextAlign = TextAnchor.MiddleLeft,
                    alignContent = Align.Center,
                    alignItems = Align.Center,
                    alignSelf = Align.Center,
                    left = 42f + 6f + 4f,
                }
            };
            Add(_label);

            var playButton = new Button(PlayFromScene) {
                iconImage = (Texture2D)EditorGUIUtility.IconContent("Animation.Play").image,
                style = {
                    position = Position.Absolute,
                    right = 48f + 41f + 21f + 3f,
                    top = 1f,
                    bottom = 1f,
                    width = 19f,
                    borderTopLeftRadius = 4f,
                    borderTopRightRadius = 4f,
                    borderBottomRightRadius = 4f,
                    borderBottomLeftRadius = 4f,
                    paddingLeft = 1f,
                    paddingTop = 1f,
                    paddingRight = 1f,
                    paddingBottom = 1f,
                },
                enabledSelf = !EditorApplication.isPlayingOrWillChangePlaymode
            };
            Add(playButton);

            var openButton = new Button(() => OpenScene(OpenSceneMode.Single)) {
                text = "Open",
                style = {
                    position = Position.Absolute,
                    right = 41f + 21f + 3f,
                    top = 1f,
                    bottom = 1f,
                    width = StyleKeyword.Auto,
                    borderTopLeftRadius = 8f,
                    borderTopRightRadius = 0f,
                    borderBottomRightRadius = 0f,
                    borderBottomLeftRadius = 8f,
                },
                enabledSelf = !EditorApplication.isPlayingOrWillChangePlaymode
            };
            Add(openButton);
            var openAddButton = new Button(() => OpenScene(OpenSceneMode.Additive)) {
                text = "Add",
                style = {
                    position = Position.Absolute,
                    right = 21f + 3f,
                    top = 1f,
                    bottom = 1f,
                    width = 42f,
                    borderTopLeftRadius = 0f,
                    borderTopRightRadius = 8f,
                    borderBottomRightRadius = 8f,
                    borderBottomLeftRadius = 0f,
                },
                enabledSelf = !EditorApplication.isPlayingOrWillChangePlaymode
            };
            Add(openAddButton);

            var scenePathProp = _sceneInfoProperty.FindPropertyRelative("<AssetPath>k__BackingField");
            var isInBuild = SceneUtils.IsInBuildSettings(scenePathProp.stringValue);
            _buildToggle = new Image() {
                image = isInBuild ? Icons.InBuild : Icons.NotInBuild,
                style = {
                    position = Position.Absolute,
                    right = 0f,
                    height = 21f,
                    width = 21f,
                    paddingLeft = 4f,
                    paddingTop = 4f,
                    paddingRight = 4f,
                    paddingBottom = 4f,
                },
                enabledSelf = !EditorApplication.isPlayingOrWillChangePlaymode
            };
            _buildToggle.RegisterCallback<MouseDownEvent>(ToggleBuildSettings);
            Add(_buildToggle);

            RegisterCallback<MouseEnterEvent>(OnMouseEnter);
            RegisterCallback<MouseLeaveEvent>(OnMouseLeave);
            RegisterCallback<MouseDownEvent>(OnMouseClick);

            this.AddManipulator(new ContextualMenuManipulator(CreateContextMenu));
        }
        #endregion

        #region Public Methods
        internal void Refresh(FilterOuts filtersPropEnumValueFlag) {
            visible = (filtersPropEnumValueFlag & _filters) switch {
                FilterOuts.Favorites => IsFavorite(),
                FilterOuts.Hidden => !IsHidden(),
                FilterOuts.Favorites | FilterOuts.Hidden => IsFavorite() && !IsHidden(),
                _ => true
            };
            style.display = visible ? DisplayStyle.Flex : DisplayStyle.None;
            MarkDirtyRepaint();
        }
        #endregion

        #region Private Methods
        private void SelectSceneAsset() {
            var scenePath = _sceneInfoProperty.FindPropertyRelative("<AssetPath>k__BackingField").stringValue;
            var asset = AssetDatabase.LoadMainAssetAtPath(scenePath);
            Selection.SetActiveObjectWithContext(asset, null);
        }

        private void PlayFromScene() {
            var sceneCount = SceneManager.sceneCount;
            for (var i = 0; i < sceneCount; i++)
                PlayModeUtils.AddSceneToReOpen(SceneManager.GetSceneAt(i).path);

            var scenePath = _sceneInfoProperty.FindPropertyRelative("<AssetPath>k__BackingField").stringValue;
            if (!SceneUtils.SaveAndOpenScene(scenePath, OpenSceneMode.Single))
                return;

            EditorApplication.EnterPlaymode();
        }

        private void OpenScene(OpenSceneMode mode) {
            var scenePath = _sceneInfoProperty.FindPropertyRelative("<AssetPath>k__BackingField").stringValue;
            SceneUtils.SaveAndOpenScene(scenePath, mode);
        }

        private void CloseScene() {
            var sceneName = _sceneInfoProperty.FindPropertyRelative("<SceneName>k__BackingField").stringValue;
            SceneUtils.SaveAndCloseScene(sceneName);
        }

        private bool IsFavorite() {
            var isFaveProp = _sceneInfoProperty.FindPropertyRelative("<IsFavorite>k__BackingField");
            return isFaveProp.boolValue;
        }

        private bool IsHidden() {
            var isHiddenProp = _sceneInfoProperty.FindPropertyRelative("<IsHidden>k__BackingField");
            return isHiddenProp.boolValue;
        }
        #endregion

        #region Event Methods
        private void OnMouseEnter(MouseEnterEvent evt) {
            style.backgroundColor = _isSelected ? Colors.UnitySelectionHighlight : Colors.UnityDarkBackground;
        }

        private void OnMouseLeave(MouseLeaveEvent evt) {
            style.backgroundColor = _isSelected ? Colors.UnitySelection : (_isOdd ? Colors.UnityLightBackground : Color.clear);
        }

        private void OnMouseClick(MouseDownEvent mouseDownEvent) {
            if (mouseDownEvent.button != 0)
                return;

            if (!HitTest(mouseDownEvent.localMousePosition))
                return;

            if ((DateTime.Now - _clickTime).TotalMilliseconds < 250)
                SelectSceneAsset();
            else
                _clickTime = DateTime.Now;

            _parentSelection.AddToSelection(this);
        }

        private void ToggleBuildSettings(MouseDownEvent evt) {
            evt.StopImmediatePropagation();
            var scenePathProp = _sceneInfoProperty.FindPropertyRelative("<AssetPath>k__BackingField");
            var isInBuild = SceneUtils.ToggleBuildSettingsPresence(scenePathProp.stringValue);
            _buildToggle.image = isInBuild ? Icons.InBuild : Icons.NotInBuild;
        }

        private void CreateContextMenu(ContextualMenuPopulateEvent evt) {
            var sceneName = _sceneInfoProperty.FindPropertyRelative("<SceneName>k__BackingField").stringValue;

            var isSceneOpened = SceneUtils.IsSceneOpened(sceneName);
            var isOnlyOpenedScene = isSceneOpened && SceneManager.sceneCount <= 1;

            var openedSceneAdditiveStatus = isSceneOpened ? Status.Hidden : Status.Normal;
            var openedSceneStatus = isOnlyOpenedScene ? Status.Hidden : Status.Normal;
            var closableSceneStatus = isSceneOpened ? (isOnlyOpenedScene ? Status.Disabled : Status.Normal) : Status.Hidden;

            evt.menu.AppendAction("Select", _ => SelectSceneAsset());
            evt.menu.AppendSeparator();
            evt.menu.AppendAction("Play From Here", _ => PlayFromScene(), openedSceneStatus);
            evt.menu.AppendSeparator();
            evt.menu.AppendAction("Open", _ => OpenScene(OpenSceneMode.Single), openedSceneStatus);
            evt.menu.AppendAction("Open (Additive)", _ => OpenScene(OpenSceneMode.Additive), openedSceneAdditiveStatus);
            evt.menu.AppendAction("Close", _ => CloseScene(), closableSceneStatus);

            evt.menu.PrepareForDisplay(evt);
        }

        private void RefreshStyle() {
            style.backgroundColor = _isSelected ? Colors.UnitySelection : Color.clear;
        }

        private void OnFiltersChanged() {
            visible = _filters switch {
                FilterOuts.Favorites => IsFavorite(),
                FilterOuts.Hidden => !IsHidden(),
                FilterOuts.Favorites | FilterOuts.Hidden => IsFavorite() && !IsHidden(),
                _ => true
            };
            style.display = visible ? DisplayStyle.Flex : DisplayStyle.None;
            MarkDirtyRepaint();
        }

        private void OnFilterTextChanged() {
            var sceneName = _sceneInfoProperty.FindPropertyRelative("<SceneName>k__BackingField").stringValue;
            var ignoreCase = !_sceneInfoProperty.serializedObject.FindProperty("<IsSearchCaseSensitive>k__BackingField").boolValue;
            var comparison = ignoreCase ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture;
            visible = sceneName.Contains(_filterText, comparison);
            style.display = visible ? DisplayStyle.Flex : DisplayStyle.None;
            _label.text = !string.IsNullOrWhiteSpace(_filterText) ? Regex.Replace(sceneName, _filterText, "<b><color=#FA6>$0</color></b>", RegexOptions.IgnoreCase) : sceneName;
            MarkDirtyRepaint();
        }

        private void TrackFaveValueChanged(bool isFavorite) {
            if (isFavorite)
                _filters |= FilterOuts.Favorites;
            else
                _filters ^= FilterOuts.Favorites;
            (parent.parent as SceneGroupHeader)!.Refresh();
        }

        private void TrackHiddenValueChanged(bool isHidden) {
            if (isHidden)
                _filters |= FilterOuts.Hidden;
            else
                _filters ^= FilterOuts.Hidden;
            (parent.parent as SceneGroupHeader)!.Refresh();
        }

        private void OnSceneOpened(Scene scene, OpenSceneMode openSceneMode) {
            var assetPath = _sceneInfoProperty.FindPropertyRelative("<AssetPath>k__BackingField").stringValue;
            // Debug.Log($"{scene.path}\n{assetPath}");
            if (!_openIndicator.visible && scene.path == assetPath)
                _openIndicator.visible = true;
        }

        private void OnSceneClosed(Scene scene) {
            var assetPath = _sceneInfoProperty.FindPropertyRelative("<AssetPath>k__BackingField").stringValue;
            // Debug.Log($"{scene.path}\n{assetPath}");
            if (_openIndicator.visible && scene.path == assetPath)
                _openIndicator.visible = false;
        }
        #endregion

        #region Interface (ISelectable)
        public bool IsSelectable() => true;

        public bool HitTest(Vector2 localPoint) => localPoint.x > 42f + 6f && ContainsPoint(localPoint);

        public void Select(VisualElement selectionContainer, bool additive) {
            _isSelected = true;
            _leftBackground.visible = false;
            RefreshStyle();
        }

        public void Unselect(VisualElement selectionContainer) {
            _isSelected = false;
            _leftBackground.visible = true;
            RefreshStyle();
        }

        public bool IsSelected(VisualElement selectionContainer) => _isSelected;
        #endregion
    }
}