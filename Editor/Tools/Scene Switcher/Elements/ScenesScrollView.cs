﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.SceneSwitcher.Elements {
    internal class ScenesScrollView : ScrollView, ISelection {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        internal ScenesScrollView() {
            this.StretchToParentSize();
            style.top = 21f;
        }
        #endregion

        #region Public Methods
        internal void SetGroups(SerializedObject settings) {
            var groupsData = settings.FindProperty("<GroupDataDict>k__BackingField");
            var dictPairsProp = groupsData.FindPropertyRelative("<Pairs>k__BackingField");

            for (var i = 0; i < dictPairsProp.arraySize; i++) {
                var ithGroupProp = dictPairsProp.GetArrayElementAtIndex(i);
                var groupNameProp = ithGroupProp.FindPropertyRelative("<Key>k__BackingField");
                var groupDataProp = ithGroupProp.FindPropertyRelative("<Value>k__BackingField");

                var groupElement = new SceneGroupHeader(groupNameProp.stringValue, groupDataProp, this);
                Add(groupElement);
            }
        }

        internal void SetFilterFavorites(bool doFilter) {
            var groups = Children().OfType<SceneGroupHeader>();

            foreach (var group in groups)
                group.SetFilterFavorites(doFilter);

            MarkDirtyRepaint();
        }

        internal void SetFilterHidden(bool doFilter) {
            var groups = Children().OfType<SceneGroupHeader>();

            foreach (var group in groups)
                group.SetFilterHidden(doFilter);

            MarkDirtyRepaint();
        }

        internal void SetFilterText(string filter) {
            var groups = Children().OfType<SceneGroupHeader>();

            foreach (var group in groups)
                group.SetFilterText(filter);

            MarkDirtyRepaint();
        }

        internal async void Refresh() {
            await Task.Yield();

            var groups = Children().OfType<SceneGroupHeader>();
            foreach (var group in groups)
                group.Refresh();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Interface (ISelection)
        public void AddToSelection(ISelectable selectable) {
            if (selection.Contains(selectable))
                return;

            ClearSelection();
            selection.Add(selectable);
            selectable.Select(this, false);
        }

        public void RemoveFromSelection(ISelectable selectable) {
            if (!selection.Contains(selectable))
                return;

            selection.Remove(selectable);
            selectable.Unselect(this);
        }

        public void ClearSelection() {
            foreach (var selectable in selection)
                selectable.Unselect(this);
            selection.Clear();
        }

        public List<ISelectable> selection { get; } = new();
        #endregion
    }
}