﻿using Izinspector.Editors.Tools.SceneSwitcher.Data;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.SceneSwitcher.Elements {
    internal class ScenesToolbar : Toolbar {
        #region Public Variables
        public event System.Action<string> OnSearchFilterChangedEvent; 
        public event System.Action<bool> OnCaseSensitiveSearchFilterChangedEvent; 
        public event System.Action<bool> OnFavoritesFilterChangedEvent; 
        public event System.Action<bool> OnHiddenFilterChangedEvent; 
        #endregion

        #region Private Variables
        private readonly SerializedObject _serializedObject;
        private readonly SerializedProperty _caseSensProp;
        private readonly SerializedProperty _filtersProp;

        private readonly Image _faveIcon;
        private readonly Image _visibleIcon;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        internal ScenesToolbar(SerializedObject serializedObject) {
            _serializedObject = serializedObject;

            _caseSensProp = serializedObject.FindProperty("<IsSearchCaseSensitive>k__BackingField");
            var caseSensitiveToggle = new ToolbarToggle() {
                text = "Aa",
                value = _caseSensProp.boolValue,
            };
            caseSensitiveToggle.Bind(_serializedObject);
            caseSensitiveToggle.RegisterValueChangedCallback(OnCaseSensitiveSearchChanged);
            Add(caseSensitiveToggle);

            var searchField = new ToolbarSearchField();
            searchField.RegisterValueChangedCallback(OnSearchFilterChanged);
            Add(searchField);

            var spacer = new ToolbarSpacer() {
                flex = true
            };
            Add(spacer);

            _filtersProp = serializedObject.FindProperty("<Filters>k__BackingField");
            var filters = (FilterOuts)_filtersProp.enumValueFlag;

            var favoritesFilterToggle = new ToolbarToggle() {
                value = filters.HasFlag(FilterOuts.Favorites),
                style = {
                    width = 26f,
                }
            };
            _faveIcon = new Image() {
                image = filters.HasFlag(FilterOuts.Favorites) ? Icons.StarOn : Icons.StarOnDisabled,
                style = {
                    position = Position.Absolute,
                    left = 2f,
                    top = 2f,
                    right = 2f,
                    bottom = 2f,
                }
            };
            favoritesFilterToggle.Add(_faveIcon);
            favoritesFilterToggle.RegisterValueChangedCallback(OnFavoritesFilterChanged);
            Add(favoritesFilterToggle);

            var visibleFilterToggle = new ToolbarToggle() {
                value = filters.HasFlag(FilterOuts.Hidden),
                style = {
                    width = 26f,
                }
            };
            _visibleIcon = new Image() {
                image = filters.HasFlag(FilterOuts.Hidden) ? Icons.Hide : Icons.ShowDisabled,
                style = {
                    position = Position.Absolute,
                    left = 2f,
                    top = 2f,
                    right = 2f,
                    bottom = 2f,
                }
            };
            visibleFilterToggle.Add(_visibleIcon);
            visibleFilterToggle.RegisterValueChangedCallback(OnVisibleFilterChanged);
            Add(visibleFilterToggle);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Methods
        private void OnSearchFilterChanged(ChangeEvent<string> evt) => OnSearchFilterChangedEvent?.Invoke(evt.newValue);

        private void OnCaseSensitiveSearchChanged(ChangeEvent<bool> evt) {
            _caseSensProp.serializedObject.Update();
            _caseSensProp.boolValue = evt.newValue;
            _caseSensProp.serializedObject.ApplyModifiedPropertiesWithoutUndo();
            OnCaseSensitiveSearchFilterChangedEvent?.Invoke(evt.newValue);
        }

        private void OnFavoritesFilterChanged(ChangeEvent<bool> evt) {
            _faveIcon.image = evt.newValue ? Icons.StarOn : Icons.StarOnDisabled;

            _filtersProp.serializedObject.Update();
            if (evt.newValue)
                _filtersProp.enumValueFlag = (int)((FilterOuts)_filtersProp.enumValueFlag | FilterOuts.Favorites);
            else
                _filtersProp.enumValueFlag = (int)((FilterOuts)_filtersProp.enumValueFlag ^ FilterOuts.Favorites);
            _filtersProp.serializedObject.ApplyModifiedPropertiesWithoutUndo();

            OnFavoritesFilterChangedEvent?.Invoke(evt.newValue);
        }

        private void OnVisibleFilterChanged(ChangeEvent<bool> evt) {
            _visibleIcon.image = evt.newValue ? Icons.Hide : Icons.ShowDisabled;

            _filtersProp.serializedObject.Update();
            if (evt.newValue)
                _filtersProp.enumValueFlag = (int)((FilterOuts)_filtersProp.enumValueFlag | FilterOuts.Hidden);
            else
                _filtersProp.enumValueFlag = (int)((FilterOuts)_filtersProp.enumValueFlag ^ FilterOuts.Hidden);
            _filtersProp.serializedObject.ApplyModifiedPropertiesWithoutUndo();

            OnHiddenFilterChangedEvent?.Invoke(evt.newValue);
        }
        #endregion
    }
}