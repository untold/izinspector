﻿using System.Linq;
using Izinspector.Editors.Tools.SceneSwitcher.Data;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.SceneSwitcher.Elements {
    internal class SceneGroupHeader : VisualElement {
        #region Private Variables
        private readonly SerializedProperty _groupInfoProperty;

        private string _labelContents;

        private readonly VisualElement _contentsContainer;
        private readonly Label _label;
        private readonly Image _dropdownIcon;
        #endregion

        #region Properties
        public bool IsExpanded { get; private set; }
        #endregion

        #region Constructors
        public SceneGroupHeader(string label, SerializedProperty groupInfoProperty, ISelection parentSelection) {
            _groupInfoProperty = groupInfoProperty;
            _labelContents = label;

            style.flexWrap = Wrap.NoWrap;
            style.flexDirection = FlexDirection.Column;
            style.flexGrow = 1f;
            style.minHeight = 21f;

            var toolbar = new Toolbar() {
                style = {
                    position = Position.Absolute,
                    left = 0f,
                    top = 0f,
                    right = 0f,
                    flexDirection = FlexDirection.Column,
                    flexGrow = 1f,
                    height = 21f,
                    minHeight = 21f,
                }
            };
            Add(toolbar);

            var isExpandedProp = _groupInfoProperty.FindPropertyRelative("<IsExpanded>k__BackingField");
            IsExpanded = isExpandedProp.boolValue;

            _dropdownIcon = new Image() {
                image = IsExpanded ? Icons.DropdownOn : Icons.DropdownOff,
                style = {
                    position = Position.Absolute,
                    left = 2f,
                    top = 2f,
                    bottom = 2f,
                    width = 18f,
                }
            };
            toolbar.Add(_dropdownIcon);

            _label = new Label() {
                text = _labelContents,
                style = {
                    position = Position.Absolute,
                    left = 21f,
                    top = 0f,
                    bottom = 0f,
                    flexGrow = 1f,
                    unityTextAlign = TextAnchor.MiddleLeft,
                }
            };
            toolbar.Add(_label);

            _contentsContainer = new VisualElement() {
                style = {
                    position = Position.Absolute,
                    left = 0f,
                    top = 21f,
                    right = 0f,
                    flexWrap = Wrap.Wrap,
                    flexDirection = FlexDirection.Column,
                    flexGrow = 1f,
                    height = StyleKeyword.Auto,
                    display = IsExpanded ? DisplayStyle.Flex : DisplayStyle.None,
                },
            };
            Add(_contentsContainer);

            var scenesProp = _groupInfoProperty.FindPropertyRelative("<Scenes>k__BackingField");
            for (var i = 0; i < scenesProp.arraySize; i++) {
                var ithSceneProp = scenesProp.GetArrayElementAtIndex(i);
                var sceneElement = new SceneElement(ithSceneProp, parentSelection, i % 2 == 0);
                _contentsContainer.Add(sceneElement);
            }

            OnToggleExpand();

            RegisterCallback<MouseDownEvent>(OnMouseDown);
        }

        internal bool SetFilterFavorites(bool doFilter) {
            var sceneElements = _contentsContainer.Children().OfType<SceneElement>();

            var activeChildren = _contentsContainer.childCount;
            foreach (var sceneElement in sceneElements) {
                if (doFilter)
                    sceneElement.Filters |= FilterOuts.Favorites;
                else
                    sceneElement.Filters ^= FilterOuts.Favorites;
                if (!sceneElement.visible)
                    --activeChildren;
            }

            style.height = IsExpanded ? _contentsContainer.Children().OfType<SceneElement>().Count(c => c.visible) * 21f + 21f : 21f;
            style.display = activeChildren > 0 ? DisplayStyle.Flex : DisplayStyle.None;
          
            var filtersProp = _groupInfoProperty.serializedObject.FindProperty("<Filters>k__BackingField");
            if (filtersProp.enumValueFlag != 0)
                _label.text = _labelContents + $" ({activeChildren}/{_contentsContainer.childCount})";
            else
                _label.text = _labelContents;

            return activeChildren > 0;
        }

        internal bool SetFilterHidden(bool doFilter) {
            var sceneElements = _contentsContainer.Children().OfType<SceneElement>();

            var activeChildren = _contentsContainer.childCount;
            foreach (var sceneElement in sceneElements) {
                if (doFilter)
                    sceneElement.Filters |= FilterOuts.Hidden;
                else
                    sceneElement.Filters ^= FilterOuts.Hidden;
                if (!sceneElement.visible)
                    --activeChildren;
            }

            style.height = IsExpanded ? _contentsContainer.Children().OfType<SceneElement>().Count(c => c.visible) * 21f + 21f : 21f;
            style.display = activeChildren > 0 ? DisplayStyle.Flex : DisplayStyle.None;

            var filtersProp = _groupInfoProperty.serializedObject.FindProperty("<Filters>k__BackingField");
            if (filtersProp.enumValueFlag != 0)
                _label.text = _labelContents + $" ({activeChildren}/{_contentsContainer.childCount})";
            else
                _label.text = _labelContents;

            return activeChildren > 0;
        }

        internal bool SetFilterText(string filter) {
            var sceneElements = _contentsContainer.Children().OfType<SceneElement>();

            var activeChildren = _contentsContainer.childCount;
            foreach (var sceneElement in sceneElements) {
                sceneElement.FilterText = filter;
                if (!sceneElement.visible)
                    --activeChildren;
            }

            style.height = IsExpanded ? _contentsContainer.Children().OfType<SceneElement>().Count(c => c.visible) * 21f + 21f : 21f;
            style.display = activeChildren > 0 ? DisplayStyle.Flex : DisplayStyle.None;

            return activeChildren > 0;
        }

        internal void Refresh() {
            var sceneElements = _contentsContainer.Children().OfType<SceneElement>();

            var filtersProp = _groupInfoProperty.serializedObject.FindProperty("<Filters>k__BackingField");

            var activeChildren = _contentsContainer.childCount;
            foreach (var sceneElement in sceneElements) {
                sceneElement.Refresh((FilterOuts)filtersProp.enumValueFlag);
                if (!sceneElement.visible)
                    --activeChildren;
            }

            style.display = activeChildren > 0 ? DisplayStyle.Flex : DisplayStyle.None;
            style.height = IsExpanded ? _contentsContainer.Children().OfType<SceneElement>().Count(c => c.visible) * 21f + 21f : 21f;

            if (filtersProp.enumValueFlag != 0)
                _label.text = _labelContents + $" ({activeChildren}/{_contentsContainer.childCount})";
            else
                _label.text = _labelContents;

            MarkDirtyRepaint();
            parent.MarkDirtyRepaint();
        }
        #endregion

        #region Event Methods
        private void OnMouseDown(MouseDownEvent evt) {
            if (evt.localMousePosition.y > 21f)
                return;

            evt.StopImmediatePropagation();
            IsExpanded = !IsExpanded;

            _groupInfoProperty.serializedObject.Update();
            _groupInfoProperty.FindPropertyRelative("<IsExpanded>k__BackingField").boolValue = IsExpanded;
            _groupInfoProperty.serializedObject.ApplyModifiedPropertiesWithoutUndo();

            OnToggleExpand();
        }

        private void OnToggleExpand() {
            _contentsContainer.style.display = IsExpanded ? DisplayStyle.Flex : DisplayStyle.None;
            style.height = IsExpanded ? _contentsContainer.Children().OfType<SceneElement>().Count(c => c.visible) * 21f + 21f : 21f;
            _dropdownIcon.image = IsExpanded ? Icons.DropdownOn : Icons.DropdownOff;
            MarkDirtyRepaint();
        }
        #endregion
    }
}