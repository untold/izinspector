using System.IO;
using System.Linq;
using Izinspector.Editors.Tools.SceneSwitcher.Utility;
using Izinspector.Editors.Utility;
using UnityEngine;

namespace Izinspector.Editors.Tools.SceneSwitcher.Data {
    [System.Flags]
    public enum FilterOuts { None = 0, Favorites = 1, Hidden = 2, All = ~0 }

    internal class SceneSwitcherSettings : ScriptableObject {
        #region Internal Classes
        #endregion

        #region Public Variables
        [field: SerializeField] private SerializableDictionary<SceneGroupInfo> GroupDataDict { get; set; } = new();
        [field: SerializeField] internal FilterOuts Filters { get; set; }
        [field: SerializeField] internal bool IsSearchCaseSensitive { get; set; }
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Functions
        internal static SceneSwitcherSettings CreateTemporary() {
#if UNITY_2023_1_OR_NEWER
            var instance = FindFirstObjectByType<SceneSwitcherSettings>(FindObjectsInactive.Include);
#else
            var instance = FindObjectOfType<SceneSwitcherSettings>(true);
#endif
            if (instance != null)
                return instance;

            instance = CreateInstance<SceneSwitcherSettings>();
            instance.hideFlags = HideFlags.HideAndDontSave;
            return instance;
        }

        internal void SaveToFile() {
            var json = JsonUtility.ToJson(this, true);
            var path = Application.dataPath[..^"Assets/".Length] + "/UserSettings/SceneSwitcherSettings.asset";

            File.WriteAllText(path, json);
        }

        internal void LoadFromFile() {
            var path = Application.dataPath[..^"Assets/".Length] + "/UserSettings/SceneSwitcherSettings.asset";

            if (!File.Exists(path)) {
                Refresh();
                SaveToFile();
                return;
            }

            var json = File.ReadAllText(path);
            JsonUtility.FromJsonOverwrite(json, this);

            CheckAllScenes();
        }
        #endregion

        #region Private Methods
        private void Refresh() {
            SceneUtils.GetAllScenesPaths(out var scenesPaths);
            SceneUtils.CreateGroupsFromPaths(scenesPaths, out var groups);

            foreach (var (groupName, groupData) in groups)
                GroupDataDict[groupName] = groupData;
        }

        private void CheckAllScenes() {
            SceneUtils.GetAllScenesPaths(out var scenesPaths);
            SceneUtils.CreateGroupsFromPaths(scenesPaths, out var groups);

            GroupDataDict.ForEach(sgi => sgi.Scenes.RemoveAll(si => !si.IsReal()));

            var existingScenes = GroupDataDict.Values.SelectMany(gi => gi.Scenes);
            var realScenes = groups.Values.SelectMany(gi => gi.Scenes);

            var except = realScenes.Except(existingScenes).ToList();

            if (!except.Any())
                return;

            foreach (var sceneInfo in except) {
                var splits = sceneInfo.AssetPath.Split('/');
                var groupName = splits[^2];
                if (GroupDataDict.TryGetValue(groupName, out var group))
                    GroupDataDict[groupName].TryAddScene(sceneInfo);
                else
                    GroupDataDict.AddDefinition(groupName, new SceneGroupInfo(sceneInfo.AssetPath) { Scenes = { sceneInfo } });
            }
        }
        #endregion
    }
}