﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Tools.SceneSwitcher.Data {
    [System.Serializable]
    internal class SceneGroupInfo {
        [field: SerializeField] internal string Path { get; private set; }
        [field: SerializeField] internal List<SceneInfo> Scenes { get; private set; } = new();

        [field: SerializeField] internal string Name { get; private set; }
        [field: SerializeField] internal bool IsExpanded { get; set; }

        public SceneGroupInfo(string groupPath) {
            Path = groupPath.Split('/').ToArray()[^2];
            Name = Path.Split('/').Last();
            AddScene(AssetDatabase.AssetPathToGUID(groupPath), groupPath);
        }

        public void AddScene(string assetGuid, string assetPath) => AddScene(new SceneInfo(assetGuid, assetPath));
        public void AddScene(SceneInfo sceneInfo) => Scenes.Add(sceneInfo);

        public bool TryAddScene(SceneInfo sceneInfo) {
            if (Scenes.Exists(s => s.AssetPath == sceneInfo.AssetPath))
                return false;

            Scenes.Add(sceneInfo);
            return true;
        }
    }
}