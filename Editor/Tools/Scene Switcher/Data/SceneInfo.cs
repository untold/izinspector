﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Izinspector.Editors.Tools.SceneSwitcher.Data {
    [System.Serializable]
    internal class SceneInfo {
        [field: SerializeField] public string AssetGuid { get; private set; }
        [field: SerializeField] public string AssetPath { get; private set; }
        [field: SerializeField] public string AssetName { get; private set; }
        [field: SerializeField] public string SceneName { get; private set; }
        [field: SerializeField] public bool IsFavorite { get; private set; }
        [field: SerializeField] public bool IsHidden { get; private set; }

        public bool IsInBuild { get; private set; }

        public SceneInfo(string assetGuid, string assetPath) {
            AssetGuid = assetGuid;
            AssetPath = assetPath;

            AssetName = AssetPath.Split('/').Last();
            SceneName = AssetName.Split('.').First();

            var buildIndex = SceneUtility.GetBuildIndexByScenePath(assetPath);
            IsInBuild = buildIndex >= 0;
        }

        public void SetBuildStatus(bool isBuild) {
            if(isBuild) {
                EditorBuildSettings.scenes = EditorBuildSettings.scenes.Append(new EditorBuildSettingsScene(AssetPath, true)).ToArray();
                IsInBuild = true;
                return;
            }

            EditorBuildSettings.scenes = EditorBuildSettings.scenes.TakeWhile(s => s.path != AssetPath).ToArray();
            IsInBuild = false;
        }

        public bool IsReal() => AssetDatabase.AssetPathExists(SceneName);

        public static implicit operator string(SceneInfo sceneInfo) => sceneInfo.SceneName;
    }
}