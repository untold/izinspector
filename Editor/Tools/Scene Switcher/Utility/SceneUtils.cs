﻿using System.Collections.Generic;
using System.Linq;
using Izinspector.Editors.Tools.SceneSwitcher.Data;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace Izinspector.Editors.Tools.SceneSwitcher.Utility {
    internal static class SceneUtils {
        #region Public Methods
        internal static bool SaveAndOpenScene(string sceneToOpenPath, OpenSceneMode openSceneMode) {
            if (openSceneMode == OpenSceneMode.Single) {
                var wantsToSwitchScenes = EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();

                if(!wantsToSwitchScenes)
                    return false;
            }

            EditorSceneManager.OpenScene(sceneToOpenPath, openSceneMode);
            return true;
        }

        public static void SaveAndCloseScene(string sceneName) {
            var scene = SceneManager.GetSceneByName(sceneName);

            if (scene.isDirty) {
                var wantsToSwitchScenes = EditorSceneManager.SaveModifiedScenesIfUserWantsTo(new[] { scene });

                if(!wantsToSwitchScenes)
                    return;
            }

            EditorSceneManager.CloseScene(scene, true);
        }

        internal static bool IsSceneOpened(string sceneName) {
            var scene = SceneManager.GetSceneByName(sceneName);
            return scene.IsValid();
        }

        internal static bool IsInBuildSettings(string scenePath) => EditorBuildSettings.scenes.Any(s => s.path.Equals(scenePath));

        internal static bool ToggleBuildSettingsPresence(string scenePath) {
            var sceneIsInBuild = EditorBuildSettings.scenes.Any(s => s.path.Equals(scenePath));

            if (!sceneIsInBuild)
                EditorBuildSettings.scenes = EditorBuildSettings.scenes.Append(new EditorBuildSettingsScene(scenePath, true)).ToArray();
            else
                EditorBuildSettings.scenes = EditorBuildSettings.scenes.Where(s => !s.path.Equals(scenePath)).ToArray();

            return !sceneIsInBuild;
        }

        internal static void GetAllScenesPaths(out List<string> scenesPaths) {
            var sceneAssetsGUIDS = AssetDatabase.FindAssets("t:Scene");
            scenesPaths = sceneAssetsGUIDS.Select(AssetDatabase.GUIDToAssetPath).ToList();
        }

        internal static void CreateGroupsFromPaths(List<string> scenesPaths, out Dictionary<string, SceneGroupInfo> groups) {
            groups = new Dictionary<string, SceneGroupInfo>();

            foreach (var path in scenesPaths) {
                var splits = path.Split('/');
                var groupName = splits[^2];
                var sceneName = splits[^1];

                if (!groups.ContainsKey(groupName)) {
                    var groupData = new SceneGroupInfo(path);
                    groups.Add(groupName, groupData);
                    continue;
                }

                groups[groupName].AddScene(AssetDatabase.AssetPathToGUID(path), path);
            }
        }
        #endregion
    }
}