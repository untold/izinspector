using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Izinspector.Editors.Tools.SceneSwitcher.Utility {
    [InitializeOnLoad]
    internal sealed class PlayModeUtils {
        #region Constants
        private const string k_prefKey_scenesToReOpen = "Izinspector.SceneSwitcher.ScenesToReOpen";
        private const char k_separatorChar = '|';
        #endregion

        #region Constructors
        static PlayModeUtils() {
            Debug.Log("Initializing PlayModeUtils...\n");
            EditorApplication.playModeStateChanged += OnPlayModeStateChange;
        }

        ~PlayModeUtils() {
            EditorApplication.playModeStateChanged -= OnPlayModeStateChange;
            Debug.Log("...finalizing PlayModeUtils\n");
        }
        #endregion

        #region Public Methods
        public static void AddSceneToReOpen(string path) {
            var prefValue = EditorPrefs.GetString(k_prefKey_scenesToReOpen, null);

            var list = string.IsNullOrWhiteSpace(prefValue) ? new() : prefValue.Split(k_separatorChar).ToList();
            list.Add(path);

            EditorPrefs.SetString(k_prefKey_scenesToReOpen, string.Join(k_separatorChar, list));
        }
        #endregion

        #region Private Methods
        private static void ReOpenScenes() {
            var prefValue = EditorPrefs.GetString(k_prefKey_scenesToReOpen, null);

            if (string.IsNullOrWhiteSpace(prefValue))
                return;

            var list = prefValue.Split(k_separatorChar).ToList();

            for (var i = 0; i < list.Count; i++)
                EditorSceneManager.OpenScene(list[i], i == 0 ? OpenSceneMode.Single : OpenSceneMode.Additive);

            EditorPrefs.DeleteKey(k_prefKey_scenesToReOpen);
        }
        #endregion

        #region Event Methods
        private static void OnPlayModeStateChange(PlayModeStateChange state) {
            if (state != PlayModeStateChange.EnteredEditMode)
                return;

            ReOpenScenes();
        }
        #endregion
    }
}