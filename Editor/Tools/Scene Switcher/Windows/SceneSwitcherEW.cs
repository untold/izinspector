using Izinspector.Editors.Tools.SceneSwitcher.Data;
using Izinspector.Editors.Tools.SceneSwitcher.Elements;
using UnityEngine;
using UnityEditor;

namespace Izinspector.Editors.Tools.SceneSwitcher.Windows {
    internal class SceneSwitcherEW : EditorWindow {
        #region Constants
        #endregion

        #region Public Variables
        #endregion

        #region Private Variables
        private ScenesScrollView _scrollView;

        private string _lastFilterText;
        #endregion

        #region Properties
        internal static SceneSwitcherSettings Settings { get; private set; }
        #endregion

        #region EditorWindow Callbacks
        [MenuItem("Window/Scene Switcher %TAB")]
        public static void ShowWindow () {
            var win = GetWindow<SceneSwitcherEW>("Scene Switcher");
            win.titleContent.image = EditorGUIUtility.IconContent("P4_Updating").image;
            win.minSize = new Vector2(600, 400);
            win.Show();
        }

        private void OnEnable() {
            Settings ??= SceneSwitcherSettings.CreateTemporary();
            Settings.LoadFromFile();
        }

        private void CreateGUI() {
            var obj = new SerializedObject(Settings);

            var toolbar = new ScenesToolbar(obj);
            rootVisualElement.Add(toolbar);
            toolbar.OnFavoritesFilterChangedEvent += FilterFaves;
            toolbar.OnSearchFilterChangedEvent += FilterSearch;
            toolbar.OnCaseSensitiveSearchFilterChangedEvent += FilterCaseSensitive;
            toolbar.OnHiddenFilterChangedEvent += FilterHidden;

            _scrollView = new ScenesScrollView();
            rootVisualElement.Add(_scrollView);

            _scrollView.SetGroups(obj);
            _scrollView.Refresh();
        }

        private void OnDisable() {
            Settings?.SaveToFile();
        }
        #endregion

        #region Public Functions
        #endregion

        #region Event Methods
        private void FilterFaves(bool doFilter) => _scrollView.SetFilterFavorites(doFilter);

        private void FilterSearch(string filter) {
            _scrollView.SetFilterText(filter);
            _lastFilterText = filter;
        }

        private void FilterCaseSensitive(bool useCase) => _scrollView.SetFilterText(_lastFilterText);

        private void FilterHidden(bool doFilter) {
            _scrollView.SetFilterHidden(doFilter);
        }
        #endregion
    }
}