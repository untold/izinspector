﻿using System.IO;
using UnityEditor;
using Directory = UnityEngine.Windows.Directory;

namespace Izinspector.Editors.Tools.FastFolders {
    internal static class FastFolderCreator {
        #region Private Methods
        [MenuItem("Assets/Create/Folders/3D Model", false, 20)]
        private static void CreateModelFolders() {
            var activePath = AssetDatabase.GetAssetPath(Selection.activeObject);
            var folderPath = ProjectWindowUtil.IsFolder(Selection.activeInstanceID) ? activePath : ProjectWindowUtil.GetContainingFolder(activePath);

            Directory.CreateDirectory(Path.Combine(folderPath, "Model"));
            Directory.CreateDirectory(Path.Combine(folderPath, "Textures"));
            Directory.CreateDirectory(Path.Combine(folderPath, "Materials"));

            AssetDatabase.Refresh();
        }
        #endregion
    }
}