﻿using Izinspector.Editors.Utility;
using UnityEditor.Experimental.GraphView;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace Izinspector.Editors.Tools {
    internal class ResourcePort : Port {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        internal ResourcePort(ResourceNode parentNode, Direction portDirection) : base(Orientation.Horizontal, portDirection, Capacity.Multi, typeof(string)) {
            switch (parentNode.ResourceType) {
                case ResourceType.Scene:
                    portType = typeof(Scene);
                    elementTypeColor = Colors.UnityRed;
                    portColor = elementTypeColor;
                    break;
                case ResourceType.Instance:
                    portType = portDirection == Direction.Input ? typeof(Scene) : typeof(Object);
                    elementTypeColor = Colors.UnityBlue;
                    portColor = elementTypeColor;
                    break;
                case ResourceType.Asset:
                    portType = typeof(Object);
                    elementTypeColor = parentNode.ExternalSource ? Colors.UnityYellow : Colors.UnityGreen;
                    portColor = elementTypeColor;
                    break;
                default:
                    break;
            }

            portName = null;
            name = $"{(direction == Direction.Input ? "IN" : "OUT")}{parentNode.UId}";
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}