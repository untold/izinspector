﻿using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools {
    internal class ToolbarIconButton : ToolbarButton {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        internal ToolbarIconButton(System.Action clickEvent, Texture icon) : base(clickEvent) {
            style.flexDirection = FlexDirection.Row;
            style.alignItems = Align.Center;
            style.justifyContent = Justify.Center;
            style.paddingLeft = 4f;

            var image = new Image() {
                image = icon,
                style = {
                    paddingTop = -1f
                }
            };
            Add(image);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}