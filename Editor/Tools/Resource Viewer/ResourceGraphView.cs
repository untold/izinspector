﻿using System;
using System.Collections.Generic;
using System.Linq;
using Izinspector.Editors.Utility;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools {
    internal class ResourceGraphView : GraphView {
        #region Public Variables
        #endregion

        #region Private Variables
        private ResourceNode _rootNode;

        private readonly Dictionary<string, ResourceNode> _nodes = new();
        #endregion

        #region Properties
        #endregion

        #region Constructors
        internal ResourceGraphView() {
            style.backgroundColor = Colors.UnityWindowBorder;
            style.flexGrow = 1f;

            this.AddManipulator(new ContentZoomer());
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new SelectionDropper());

            RegisterCallback<ClickEvent>(OnClick);
        }
        #endregion

        public override EventPropagation DeleteSelection() => EventPropagation.Stop;

        #region Public Methods
        internal ResourceNode AddRootNode(Scene scene) {
            _rootNode = new ResourceNode(scene);

            AddElement(_rootNode);
            return _rootNode;
        }

        internal ResourceNode AddBranchNode(GameObject target) {
            var node = new ResourceNode(target);
            node.SetParent(_rootNode);
            AddElement(node);
            return node;
        }

        internal ResourceNode AddLeafNode(string targetPath) {
            var node = new ResourceNode(targetPath);
            if (_nodes.ContainsKey(node.UId))
            {
                node = _nodes[node.UId];
                return node;
            }

            AddElement(node);
            _nodes.Add(node.UId, node);
            return node;
        }

        internal void RemoveNode(ResourceNode node) {
            foreach (var n in nodes.OfType<ResourceNode>())
                n.ChildNodes.Remove(node.UId);
            RemoveElement(node);
        }

        internal void Refresh() {
            var bot = 0f;
            var lef = 0f;
            _rootNode.RefreshPosition(ref bot, ref lef);
        }

        internal void Purge()
        {
            DeleteElements(graphElements);
            _nodes.Clear();
            GC.Collect();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Event Methods
        private void OnClick(ClickEvent evt) {
            var el = panel.Pick(evt.position);

            if (el == this)
                ClearSelection();
        }
        #endregion
    }
}