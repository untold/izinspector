﻿using System.Linq;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Izinspector.Editors.Tools {
    internal class ResourceToolbar : Toolbar {
        #region Public Variables
        #endregion

        #region Private Variables
        private readonly ResourceGraphView _graphView;
        private readonly ToolbarToggle _includePackagesToggle;
        private readonly ToolbarMenu _searchTypeDropdown;

        private System.Type _type = typeof(Texture2D);
        #endregion

        #region Properties
        #endregion

        #region Constructors
        internal ResourceToolbar(ResourceGraphView graph) {
            _graphView = graph;

            var refreshButton = new ToolbarIconButton(() => Refresh(_type), Icons.Refresh.image);

            _includePackagesToggle = new ToolbarToggle() { text = "Include Packages" };

            _searchTypeDropdown = new ToolbarMenu() {
                text = "Texture2D",
            };
            PopulateDropdown(in _searchTypeDropdown);

            Add(refreshButton);
            Add(new ToolbarSpacer() { style = { flexGrow = 1f } });
            Add(_includePackagesToggle);
            Add(_searchTypeDropdown);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private async void Refresh(System.Type type) {
            _searchTypeDropdown.text = type.Name;
            _graphView.Purge();

            var scene = SceneManager.GetActiveScene();
            var roots = scene.GetRootGameObjects();

            _graphView.AddRootNode(scene);

            foreach (var obj in roots) {
                var deps = EditorUtility.CollectDependencies(new Object[] { obj }).Where(o => o.GetType() == type).ToList();

                if (!deps.Any())
                    continue;

                var node = _graphView.AddBranchNode(obj);

                foreach (var dep in deps) {
                    var assetPath = AssetDatabase.GetAssetPath(dep);

                    if (assetPath.StartsWith("Resources"))
                        continue;

                    if (assetPath.Contains("Editor/")
                        || assetPath.Contains("Editor Default Resources/")
                        || assetPath.Contains("EditorDefaultResources/"))
                        continue;

                    if (!_includePackagesToggle.value && assetPath.StartsWith("Packages"))
                        continue;

                    if (assetPath.StartsWith("Library"))
                        continue;

                    var leaf = _graphView.AddLeafNode(assetPath);
                    leaf?.SetParent(node);
                }

                if (!node.ChildNodes.Any())
                    _graphView.RemoveNode(node);
            }

            await System.Threading.Tasks.Task.Delay(20);
            _graphView.Refresh();
        }

        private void PopulateDropdown(in ToolbarMenu dropdown) {
            dropdown.menu.AppendAction("Texture", _ => Refresh(_type = typeof(Texture2D)));
            dropdown.menu.AppendAction("3D Model", _ => Refresh(_type = typeof(Mesh)));
            dropdown.menu.AppendAction("Materials", _ => Refresh(_type = typeof(Material)));
            dropdown.menu.AppendAction("Data", _ => Refresh(_type = typeof(ScriptableObject)));
            dropdown.menu.AppendAction("Components", _ => Refresh(_type = typeof(MonoScript)));
        }
        #endregion
    }
}