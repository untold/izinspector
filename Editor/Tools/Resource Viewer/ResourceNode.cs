﻿using System.Collections.Generic;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools {
    internal enum ResourceType { Scene, Instance, Asset }

    internal class ResourceNode : Node {
        #region Public Variables
        #endregion

        #region Private Variables
        private readonly SerializedObject _serializedObject;

        private ResourcePort _inputPort, _outPutPort;

        #endregion

        #region Properties
        internal string UId { get; }
        internal ResourceType ResourceType { get; }
        internal bool ExternalSource { get; }

        internal Dictionary<string, ResourceNode> ChildNodes { get; private set; } = new();
        #endregion

        #region Constructors
        // ROOT NODE
        internal ResourceNode(Scene scene) {
            var asset = AssetDatabase.LoadMainAssetAtPath(scene.path);
            _serializedObject = new SerializedObject(asset);
            UId = asset.GetInstanceID().ToString();

            title = scene.name;
            tooltip = scene.path;
            SetCommon();
            ResourceType = SetType(ResourceType.Scene);
            SetRoundBorders();

            _outPutPort = new ResourcePort(this, Direction.Output);
            titleContainer.Add(_outPutPort);
        }

        // BRIDGE NODE
        internal ResourceNode(GameObject target) {
            _serializedObject = new SerializedObject(target);
            UId = target.GetInstanceID().ToString();

            title = target.name;
            tooltip = target.scene.path + " & " + target.name;
            SetCommon();
            ResourceType = SetType(ResourceType.Instance);
            SetRoundBorders();

            _inputPort = new ResourcePort(this, Direction.Input);
            _outPutPort = new ResourcePort(this, Direction.Output);
            titleContainer.Insert(0, _inputPort);
            titleContainer.Add(_outPutPort);
        }

        // LEAF NODE
        internal ResourceNode(string targetPath) {
            var asset = AssetDatabase.LoadMainAssetAtPath(targetPath);
            _serializedObject = new SerializedObject(asset);
            ExternalSource = targetPath.StartsWith("Packages");
            UId = asset.GetInstanceID().ToString();

            title = asset.name;
            tooltip = targetPath;
            SetCommon();
            ResourceType = SetType(ResourceType.Asset);
            SetRoundBorders();

            _inputPort = new ResourcePort(this, Direction.Input);
            titleContainer.Insert(0, _inputPort);
            titleContainer.Add(new ToolbarSpacer() { style = { width = 12f } });
        }
        #endregion

        #region Overrides
        public override void OnSelected() {
            if (_serializedObject != null)
                Selection.SetActiveObjectWithContext(_serializedObject.targetObject, null);
        }
        #endregion

        #region Public Methods
        internal void SetParent(ResourceNode parentNode) {
            if (_inputPort == null || parentNode._outPutPort == null)
                return;

            if (parentNode.ChildNodes.ContainsKey(UId))
                return;

            parentNode.ChildNodes.Add(UId, this);

            var edge = _inputPort.ConnectTo<ResourceEdge>(parentNode._outPutPort);
            Add(edge);
        }

        internal void RefreshPosition(ref float bottom, ref float left, ResourceNode parentNode = null) {
            style.top = bottom;
            style.left = left;

            if (parentNode != null) {
                if (ChildNodes.Count <= 0)
                    bottom += style.height.value.value - 5f;
            }

            left += layout.width + 25f;
            foreach (var childNode in ChildNodes)
                childNode.Value.RefreshPosition(ref bottom, ref left, this);
            left -= layout.width + 25f;
        }
        #endregion

        #region Private Methods
        private void SetCommon() {
            this.Q("collapse-button").RemoveFromHierarchy();
            expanded = false;
            inputContainer.visible = false;
            style.height = 32f;
        }

        private ResourceType SetType(ResourceType type) {
            var color = type switch {
                ResourceType.Scene => Colors.UnityRed,
                ResourceType.Instance => Colors.UnityBlue,
                ResourceType.Asset when ExternalSource => Colors.UnityYellow,
                ResourceType.Asset => Colors.UnityGreen,
                _ => Color.clear
            };

            var border = this.Q("selection-border");
            border.style.borderLeftColor = color;
            border.style.borderTopColor = color;
            border.style.borderRightColor = color;
            border.style.borderBottomColor = color;
            return type;
        }

        private void SetRoundBorders() {
            const float k_radius = /*13f*/0f;

            style.borderTopLeftRadius = k_radius;
            style.borderTopRightRadius = k_radius;
            style.borderBottomRightRadius = k_radius;
            style.borderBottomLeftRadius = k_radius;

            titleContainer.style.borderTopLeftRadius = k_radius;
            titleContainer.style.borderTopRightRadius = k_radius;
            titleContainer.style.borderBottomRightRadius = k_radius;
            titleContainer.style.borderBottomLeftRadius = k_radius;

            mainContainer.style.borderTopLeftRadius = k_radius;
            mainContainer.style.borderTopRightRadius = k_radius;
            mainContainer.style.borderBottomRightRadius = k_radius;
            mainContainer.style.borderBottomLeftRadius = k_radius;

            var border = this.Q("selection-border");
            border.style.borderTopLeftRadius = k_radius + 2f;
            border.style.borderTopRightRadius = k_radius + 2f;
            border.style.borderBottomRightRadius = k_radius + 2f;
            border.style.borderBottomLeftRadius = k_radius + 2f;
        }
        #endregion
    }
}