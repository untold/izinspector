﻿using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Tools {
    internal class ResourceLinkViewerEW : EditorWindow {
        #region Private Variables
        private ResourceToolbar _toolbar;
        private ResourceGraphView _graphView;
        #endregion

        #region Constructors
        [MenuItem("Window/Resources LinkView")]
        private static void ShowWindow() {
            var window = GetWindow<ResourceLinkViewerEW>();
            window.titleContent = new GUIContent("Resources");
            window.Show();
        }
        #endregion

        #region EditorWindow Callbacks
        private void CreateGUI() {
            _graphView = new ResourceGraphView();
            _toolbar = new ResourceToolbar(_graphView);

            rootVisualElement.Add(_toolbar);
            rootVisualElement.Add(_graphView);
        }
        #endregion
    }
}