﻿using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Tools.ModelsBrowser.Data {
    internal class Asset {
        #region Properties
        public Object Target { get; }
        public string Path { get; }
        #endregion

        #region Constructors
        public Asset(Object asset) {
            Target = asset;
            Path = AssetDatabase.GetAssetPath(asset);
        }
        #endregion
    }
}