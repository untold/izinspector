﻿using System.Collections.Generic;
using System.Linq;
using Izinspector.Editors.Tools.ModelsBrowser.Data;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Tools.ModelsBrowser.Utility {
    internal static class AssetsFinder {
        #region Constants
        private const string k_progressTitle = "Collecting Assets";
        private const string k_progressDescription = "Looking for all available assets";
        private const string k_progressDescriptionCollecting = "Collecting asset {0}";
        private const string k_progressDescriptionData = "Creating view data";
        #endregion

        #region Public Methods
        internal static List<Asset> GetModelsPrefabs() {
            EditorUtility.DisplayProgressBar(k_progressTitle, k_progressDescription, 0f);

            var prefabsGuids = AssetDatabase.FindAssets("t:prefab");

            float total = prefabsGuids.Length;

            if (total <= 0) {
                EditorUtility.ClearProgressBar();
                return new();
            }

            var assetsPaths = prefabsGuids.Select(AssetDatabase.GUIDToAssetPath);

            var i = -1;
            // var assets = assetsPaths.Select(AssetDatabase.LoadMainAssetAtPath);
            var assets = new List<Object>();
            foreach (var path in assetsPaths) {
                EditorUtility.DisplayProgressBar(k_progressTitle, string.Format(k_progressDescriptionCollecting, path), ++i / total);
                assets.Add(AssetDatabase.LoadMainAssetAtPath(path));
            }

            EditorUtility.DisplayProgressBar(k_progressTitle, k_progressDescriptionData, 1f);
            var modelAssets = assets.Where(a => a is GameObject go && go.GetComponentInChildren<MeshRenderer>() != null);

            EditorUtility.ClearProgressBar();
            return modelAssets.Select(m => new Asset(m)).ToList();
        }
        #endregion
    }
}