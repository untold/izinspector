﻿using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.ModelsBrowser.Elements {
    internal class ToolbarTabButton : VisualElement {
        #region Public Variables
        internal event EventCallback<int, bool> OnValueChanged; 
        #endregion

        #region Private Variables
        private readonly Label _label;

        private bool _isOn = false;
        #endregion

        #region Properties
        internal bool IsOn {
            get => _isOn;
            set {
                if (value == _isOn)
                    return;

                _isOn = value;
                OnValueChanged?.Invoke(TabIndex, _isOn);
                RefreshStyle();
            }
        }
        internal int TabIndex { get; private set; }
        #endregion

        #region Constructors
        public ToolbarTabButton(string label, int tabIndex, System.Action<int, bool> onSelect) {
            style.flexWrap = Wrap.Wrap;
            style.justifyContent = Justify.SpaceAround;
            // style.unityOverflowClipBox = OverflowClipBox.ContentBox;
            // style.textOverflow = TextOverflow.Clip;
            style.overflow = Overflow.Hidden;

            style.height = style.minHeight = style.maxHeight = 21f;

            style.paddingLeft = 18f;
            style.paddingRight = 18f;

            style.backgroundColor = Colors.UnityToolbar;

            style.borderBottomWidth = 1f;
            style.borderBottomColor = Colors.UnityToolbarLines;

            _label = new Label(label) {
                style = {
                    unityTextAlign = TextAnchor.MiddleCenter,
                    color = Colors.UnityBrightWhite,
                }
            };

            TabIndex = tabIndex;
            OnValueChanged = onSelect.Invoke;

            Add(_label);
            RegisterCallback<MouseEnterEvent>(OnMouseEnter);
            RegisterCallback<MouseLeaveEvent>(OnMouseLeave);
            RegisterCallback<ClickEvent>(OnClicked);
        }
        #endregion

        #region Public Methods
        internal void SetIsOnWithoutNotify(bool isOn) {
            _isOn = isOn;
            RefreshStyle();
        }
        #endregion

        #region Private Methods
        private void RefreshStyle() {
            style.borderBottomColor = IsOn ? Colors.UnityBlue : Colors.UnityToolbarLines;
            MarkDirtyRepaint();
        }
        #endregion

        #region Event Methods
        private void OnMouseEnter(MouseEnterEvent evt) {
            style.backgroundColor = Colors.UnityToolbarHighlight;
            _label.style.color = Color.white;
        }

        private void OnMouseLeave(MouseLeaveEvent evt) {
            style.backgroundColor = Colors.UnityToolbar;
            _label.style.color = Colors.UnityBrightWhite;
        }

        private void OnClicked(ClickEvent evt) {
            IsOn = !IsOn;
            RefreshStyle();
        }
        #endregion
    }
}