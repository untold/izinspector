﻿using System.Collections.Generic;
using Izinspector.Editors.Tools.ModelsBrowser.Data;
using Izinspector.Editors.Utility;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.ModelsBrowser.Elements {
    internal enum ViewStyle { Grid, List }

    internal class AssetsView : ScrollView {
        #region Public Variables
        #endregion

        #region Private Variables
        private ViewStyle _viewStyle = ViewStyle.Grid;

        private readonly List<AssetElementBase> _elements = new();
        #endregion

        #region Properties
        #endregion

        #region Constructors
        public AssetsView() {
            style.flexGrow = 1f;
            style.backgroundColor = Colors.UnityWindowHeaderBackground;

            style.flexWrap = Wrap.Wrap;
            style.alignContent = Align.Center;
            style.justifyContent = Justify.FlexEnd;

            verticalScrollerVisibility = ScrollerVisibility.AlwaysVisible;
            horizontalScrollerVisibility = ScrollerVisibility.Hidden;

            RegisterCallback<GeometryChangedEvent>(OnResize);
        }
        #endregion

        #region Public Methods
        internal void SetViewStyle(ViewStyle viewStyle) {
            _viewStyle = viewStyle;
            for (var i = 0; i < _elements.Count; i++) {
                _elements[i] = _viewStyle switch {
                    ViewStyle.Grid => new AssetGridElement(_elements[i]),
                    _ => new AssetListElement(_elements[i])
                };
            }
            Clear();
            _elements.ForEach(Add);

            OnResize(null);
        }

        internal void SetAssets(List<Asset> prefabs) {
            foreach (var prefab in prefabs) {
                AssetElementBase assetElement = _viewStyle switch {
                    ViewStyle.Grid => new AssetGridElement(prefab),
                    _ => new AssetListElement(prefab)
                };
                _elements.Add(assetElement);
                Add(assetElement);
            }

            OnResize(null);
        }
        #endregion

        #region Private Methods
        private void DoGridLayout() {
            if (_elements.Count <= 0)
                return;

            var elMinSiz = _elements[0].style.minWidth.value.value;
            var siz = new Vector2(layout.width - verticalScroller.resolvedStyle.width, layout.height);

            var columns = Mathf.FloorToInt(siz.x / elMinSiz);
            var rows = Mathf.CeilToInt(_elements.Count / (float)columns);

            var evenSize = siz.x / columns;

            for (var y = 0; y < rows; y++) {
                for (var x = 0; x < columns; x++) {
                    var index = y * columns + x;

                    if (_elements.Count <= index)
                        break;

                    var element = _elements[index];
                    element.style.left = x * evenSize;
                    element.style.top = y * evenSize;
                    element.MarkDirtyRepaint();
                }
            }

            _elements.ForEach(e => {
                e.style.width = evenSize;
                e.style.height = evenSize;
            });

            MarkDirtyRepaint();
        }

        private void DoListLayout() {
            if (_elements.Count <= 0)
                return;

            const float elMinSiz = 82f; // Its the size of the grid element when minimum window size is reached

            for (var i = 0; i < _elements.Count; i++) {
                var element = _elements[i];
                element.style.left = 0f;
                element.style.top = i * elMinSiz;
                element.style.width = layout.width - verticalScroller.resolvedStyle.width;
                element.style.height = elMinSiz;
                element.MarkDirtyRepaint();
            }

            MarkDirtyRepaint();
        }
        #endregion

        #region Event Methods
        private void OnResize(GeometryChangedEvent evt) {
            switch (_viewStyle) {
                case ViewStyle.Grid:
                    DoGridLayout();
                    break;
                case ViewStyle.List:
                    DoListLayout();
                    break;
                default:
                    return;
            }
        }
        #endregion
    }
}