﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.ModelsBrowser.Elements {
    internal class AssetsToolbar : Toolbar {
        #region Public Variables
        internal event EventCallback<int> OnActiveTabChanged; 
        internal event EventCallback<ViewStyle> OnViewStyleChanged; 
        #endregion

        #region Private Variables
        private readonly List<ToolbarTabButton> _tabs = new();
        private readonly ToolbarMenu _searchTypeDropdown;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        public AssetsToolbar(params string[] tabsLabels) {
            for (var i = 0; i < tabsLabels.Length; i++) {
                var tab = new ToolbarTabButton(tabsLabels[i], i, OnTabSelected);
                _tabs.Add(tab);
                Add(tab);
            }

            Add(new ToolbarSpacer() { style = { flexGrow = 1f } });

            _searchTypeDropdown = new ToolbarMenu() {
                text = ViewStyle.Grid.ToString(),
                style = {
                    width = 60f
                }
            };
            PopulateDropdown(in _searchTypeDropdown);

            Add(_searchTypeDropdown);

            _tabs[0].IsOn = true;
        }

        #endregion

        #region Private Methods
        private void PopulateDropdown(in ToolbarMenu dropdown) {
            foreach (ViewStyle viewStyle in System.Enum.GetValues(typeof(ViewStyle)))
                dropdown.menu.AppendAction(viewStyle.ToString(), _ => OnViewStyleSelected(viewStyle));
        }
        #endregion

        #region Event Methods
        private void OnTabSelected(int index, bool value) {
            if (!value) {
                if (_tabs.Any(t => t.IsOn))
                    return;

                _tabs[index].SetIsOnWithoutNotify(true);
                return;
            }

            for (var i = 0; i < _tabs.Count; i++) {
                if (i == index)
                    continue;

                if (!_tabs[i].IsOn)
                    continue;

                _tabs[i].SetIsOnWithoutNotify(false);
            }

            OnActiveTabChanged?.Invoke(index);
        }

        private void OnViewStyleSelected(ViewStyle viewStyle) {
            _searchTypeDropdown.text = viewStyle.ToString();
            OnViewStyleChanged?.Invoke(viewStyle);
        }
        #endregion
    }
}