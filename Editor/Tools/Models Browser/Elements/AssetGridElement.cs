﻿using Izinspector.Editors.Tools.ModelsBrowser.Data;
using Izinspector.Editors.Utility;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.ModelsBrowser.Elements {
    internal class AssetGridElement : AssetElementBase {
        #region Private Variables
        private Label _nameLabel;
        private VisualElement _border;
        #endregion

        #region Constructors
        public AssetGridElement(Asset asset) : base(asset) {
            Populate();
        }

        public AssetGridElement(AssetElementBase source) : base(source) {
            Populate(source._previewImage.image);
        }
        #endregion

        #region Overrides
        protected override void AssignPreview(Texture2D preview) => _previewImage.image = preview;

        protected override void OnMouseEnter(MouseEnterEvent evt) {
            _border.style.borderLeftColor = Colors.UnityBlue;
            _border.style.borderTopColor = Colors.UnityBlue;
            _border.style.borderRightColor = Colors.UnityBlue;
            _border.style.borderBottomColor = Colors.UnityBlue;

            _previewImage.style.marginLeft = 0f;
            _previewImage.style.marginTop = 0f;
            _previewImage.style.marginRight = 0f;
            _previewImage.style.marginBottom = 0f;

            _nameLabel.style.opacity = 1f;
        }

        protected override void OnMouseLeave(MouseLeaveEvent evt) {
            _border.style.borderLeftColor = Color.clear;
            _border.style.borderTopColor = Color.clear;
            _border.style.borderRightColor = Color.clear;
            _border.style.borderBottomColor = Color.clear;

            _border.style.borderLeftWidth = 1f;
            _border.style.borderTopWidth = 1f;
            _border.style.borderRightWidth = 1f;
            _border.style.borderBottomWidth = 1f;

            _previewImage.style.marginLeft = 3f;
            _previewImage.style.marginTop = 3f;
            _previewImage.style.marginRight = 3f;
            _previewImage.style.marginBottom = 3f;

            _nameLabel.style.opacity = 0f;
        }

        protected override void OnMouseDown(MouseDownEvent evt) {
            _border.style.borderLeftWidth = 2f;
            _border.style.borderTopWidth = 2f;
            _border.style.borderRightWidth = 2f;
            _border.style.borderBottomWidth = 2f;
        }

        protected override void OnMouseUp(MouseUpEvent evt) {
            _border.style.borderLeftWidth = 1f;
            _border.style.borderTopWidth = 1f;
            _border.style.borderRightWidth = 1f;
            _border.style.borderBottomWidth = 1f;
        }
        #endregion

        #region Private Methods
        private void Populate(Texture preview = null) {
            style.position = Position.Absolute;

            style.unityOverflowClipBox = OverflowClipBox.PaddingBox;
            style.overflow = Overflow.Hidden;

            style.width = 100f;
            style.height = 100f;

            style.minWidth = 80f;
            style.minHeight = 80f;

            style.maxWidth = 200f;
            style.maxHeight = 200f;

            _previewImage = new Image {
                image = preview ? preview : Icons.Loading,
                style = {
                    overflow = Overflow.Hidden,
                    flexGrow = 1f,
                    borderTopLeftRadius = 2f,
                    borderTopRightRadius = 2f,
                    borderBottomRightRadius = 2f,
                    borderBottomLeftRadius = 2f,
                    marginLeft = 3f,
                    marginTop = 3f,
                    marginRight = 3f,
                    marginBottom = 3f,
                    borderLeftWidth = 1f,
                    borderTopWidth = 1f,
                    borderRightWidth = 1f,
                    borderBottomWidth = 1f,
                    backgroundColor = Colors.UnityWindowHeaderBackground,
                    borderLeftColor = Colors.UnityWindowBorder,
                    borderTopColor = Colors.UnityWindowBorder,
                    borderRightColor = Colors.UnityWindowBorder,
                    borderBottomColor = Colors.UnityWindowBorder,
                }
            };

            _nameLabel = new Label() {
                text = Asset.Target.name,
                style = {
                    position = Position.Absolute,
                    overflow = Overflow.Hidden,
                    height = StyleKeyword.Auto,
                    left = 0f,
                    right = 0f,
                    bottom = 0f,
                    marginLeft = 1f,
                    marginTop = 1f,
                    marginRight = 1f,
                    marginBottom = 1f,
                    paddingLeft = 4f,
                    paddingTop = 4f,
                    paddingRight = 4f,
                    paddingBottom = 4f,
                    backgroundColor = Colors.UnityDarkBackground.Alpha(.65f),
                    unityTextAlign = TextAnchor.LowerCenter,
                    whiteSpace = WhiteSpace.Normal,
                    opacity = 0f,
                }
            };

            _border = new() {
                style = {
                    position = Position.Absolute,
                    left = 0f,
                    top = 0f,
                    right = 0f,
                    bottom = 0f,
                    borderLeftWidth = 1f,
                    borderTopWidth = 1f,
                    borderRightWidth = 1f,
                    borderBottomWidth = 1f,
                    borderLeftColor = Color.clear,
                    borderTopColor = Color.clear,
                    borderRightColor = Color.clear,
                    borderBottomColor = Color.clear,
                }
            };

            Add(_previewImage);
            Add(_nameLabel);
            Add(_border);

            if (!preview)
                LoadPreviewAsync();
        }
        #endregion
    }
}