﻿using System.Threading.Tasks;
using Izinspector.Editors.Tools.ModelsBrowser.Data;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.ModelsBrowser.Elements {
    internal abstract class AssetElementBase : VisualElement {
        #region Private Variables
        protected internal Image _previewImage;

        private bool _pressing;
        #endregion

        #region Properties
        internal Asset Asset { get; private set; }
        #endregion

        #region Constructors
        internal AssetElementBase(Asset asset) {
            Asset = asset;

            RegisterCallback<MouseEnterEvent>(MouseEnter);
            RegisterCallback<MouseLeaveEvent>(MouseLeave);
            RegisterCallback<MouseDownEvent>(MouseDown);
            RegisterCallback<MouseUpEvent>(MouseUp);
            RegisterCallback<MouseMoveEvent>(MouseMove);
            RegisterCallback<ClickEvent>(Click);
        }

        internal AssetElementBase(AssetElementBase source) {
            Asset = source.Asset;

            RegisterCallback<MouseEnterEvent>(MouseEnter);
            RegisterCallback<MouseLeaveEvent>(MouseLeave);
            RegisterCallback<MouseDownEvent>(MouseDown);
            RegisterCallback<MouseUpEvent>(MouseUp);
            RegisterCallback<MouseMoveEvent>(MouseMove);
            RegisterCallback<ClickEvent>(Click);
        }
        #endregion

        #region Private Methods
        protected async void LoadPreviewAsync() {
            while (AssetPreview.IsLoadingAssetPreviews())
                await Task.Yield();
            AssignPreview(AssetPreview.GetAssetPreview(Asset.Target));
        }
        #endregion

        #region Event Methods
        private void MouseEnter(MouseEnterEvent evt) => OnMouseEnter(evt);

        private void MouseLeave(MouseLeaveEvent evt) {
            OnMouseLeave(evt);
            _pressing = false;
        }

        private void MouseDown(MouseDownEvent evt) {
            _pressing = true;
            OnMouseDown(evt);
        }

        private void MouseUp(MouseUpEvent evt) {
            _pressing = false;
            OnMouseUp(evt);
        }

        private void MouseMove(MouseMoveEvent evt) {
            if (!_pressing)
                return;

            DragAndDrop.PrepareStartDrag();
            DragAndDrop.StartDrag($"Create \"{Asset.Target.name}\"");
            DragAndDrop.objectReferences = new[] { Asset.Target };
        }

        private void Click(ClickEvent evt) => Selection.SetActiveObjectWithContext(Asset.Target, null);
        #endregion

        #region Virtual Methods
        protected abstract void AssignPreview(Texture2D preview);

        protected virtual void OnMouseEnter(MouseEnterEvent evt) { }
        protected virtual void OnMouseLeave(MouseLeaveEvent evt) { }
        protected virtual void OnMouseDown(MouseDownEvent evt) { }
        protected virtual void OnMouseUp(MouseUpEvent evt) { }
        #endregion
    }
}