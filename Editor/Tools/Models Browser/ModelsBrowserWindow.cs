﻿using Izinspector.Editors.Tools.ModelsBrowser.Elements;
using Izinspector.Editors.Tools.ModelsBrowser.Utility;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Tools.ModelsBrowser {
    public class ModelsBrowserWindow : EditorWindow {
        #region Private Variables
        private AssetsToolbar _toolbar;
        private AssetsView _assetsView;
        #endregion

        #region Constructors
#if UNITY_2022_1_OR_NEWER
        [PieMenuItem("Models Browser")]
#endif
        [MenuItem("Window/Models Browser")]
        private static void ShowWindow() {
            var window = GetWindow<ModelsBrowserWindow>();
            window.titleContent = new GUIContent("Models Browser");
            window.Show();
        }
        #endregion

        #region EditorWindow Callbacks
        private void CreateGUI() {
            _toolbar = new AssetsToolbar("All", "Props", "Building");
            _assetsView = new AssetsView();

            _toolbar.OnActiveTabChanged += OnTabChanged;
            _toolbar.OnViewStyleChanged += _assetsView.SetViewStyle;

            rootVisualElement.Add(_toolbar);
            rootVisualElement.Add(_assetsView);

            var prefabs = AssetsFinder.GetModelsPrefabs();
            _assetsView.SetAssets(prefabs);
        }
        #endregion

        #region Event Methods
        private void OnTabChanged(int index) {
            switch (index) {
                case 0: // All
                    break;
                case 1: // Props
                    break;
                case 2: // Buildings
                    break;
                default:
                    return;
            }
        }
        #endregion
    }
}