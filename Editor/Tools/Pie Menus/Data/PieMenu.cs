﻿#if UNITY_2022_1_OR_NEWER
using System;
using System.Collections.Generic;

namespace Izinspector.Editors.Tools.PieMenus.Data {
    internal class PieMenu {
        #region Properties
        internal string Name { get; }
        internal List<PieItem> Items { get; } = new();
        internal List<PieMenu> SubMenus { get; } = new();
        internal PieMenu ParentMenu { get; private set; }
        internal bool CanDisplay => Validator != null && Validator.Invoke();

        private Func<bool> Validator { get; set; }
        #endregion

        #region Constructors
        internal PieMenu(string name, Func<bool> validator = null) {
            Name = name;
            Validator = validator ?? (() => true);
        }
        #endregion

        #region Public Methods
        internal void AddItem(PieItem item) => Items.Add(item);
        internal void AddItem(string itemName, Action callback, Func<bool> validate = null, string icon = null) => AddItem(new PieItem(itemName, callback, validate, icon));

        internal void AddMenu(in PieMenu menu) {
            SubMenus.Add(menu);
            menu.AddParentMenu(this);
        }

        internal void RemoveMenu(in PieMenu menu) {
            menu.RemoveParentMenu();
            SubMenus.Remove(menu);
        }

        internal bool TryAddMenu(in PieMenu menu) {
            var newName = menu.Name;
            if (SubMenus.Exists(sm => sm.Name == newName))
                return false;

            SubMenus.Add(menu);
            return true;
        }

        internal bool TryRemoveMenu(string menuName) {
            var index = SubMenus.FindIndex(m => m.Name == menuName);
            if (index < 0)
                return false;

            SubMenus.RemoveAt(index);
            return true;
        }

        internal void AddParentMenu(in PieMenu parentMenu) => ParentMenu = parentMenu;
        internal void RemoveParentMenu() => ParentMenu = null;

        internal void SetValidator(Func<bool> validator) => Validator = validator;
        #endregion

        public override string ToString() => $"{Name} ({SubMenus.Count})";
    }
}
#endif