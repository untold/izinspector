﻿#if UNITY_2022_1_OR_NEWER
using System;
using Izinspector.Runtime.PropertyAttributes;

namespace Izinspector.Editors.Tools.PieMenus.Data {
    internal class PieItem {
        #region Properties
        internal string Path { get; }
        internal string MenuName { get; private set; }
        internal string IconPath { get; }
        internal PieContext Context { get; }
        internal bool CanDisplay => Validator != null && Validator.Invoke();

        private Action Callback { get; }
        private Func<bool> Validator { get; }
        #endregion

        #region Constructors
        internal PieItem(string menuName, Action callback, Func<bool> validator = null, string iconPath = null, PieContext context = PieContext.General) {
            MenuName = Path = menuName;
            Callback = callback;
            Validator = validator ?? (() => true);
            IconPath = iconPath;
            Context = context;
        }
        #endregion

        #region Public Methods
        internal void Execute() => Callback?.Invoke();

        internal void StepInto() {
            try {
                MenuName = MenuName.Substring(MenuName.IndexOf('/') + 1);
            } catch {}
        }

        internal void Restore() => MenuName = Path;
        #endregion

        public override string ToString() => $"{MenuName}";
    }
}
#endif