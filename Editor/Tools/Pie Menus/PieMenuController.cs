#if UNITY_2022_1_OR_NEWER
using System;
using Izinspector.Editors.Tools.Elements;
using Izinspector.Editors.Tools.PieMenus.Utility;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEditor.ShortcutManagement;
using UnityEngine;

namespace Izinspector.Editors.Tools.PieMenus {
    [InitializeOnLoad]
    public static class PieMenuController {
        #region Private Variables
        private static SceneView _targetSceneView;
        #endregion

        #region Properties
        internal static Vector3 PivotPoint { get; private set; }
        internal static Vector3 HitPoint { get; private set; }

        private static PieView PieView { get; }
        private static bool IsVisible { get; set; }
        #endregion

        #region Constructors
        static PieMenuController()
        {
            PieView = new PieView();

            PieItemsFinder.CollectAllItems();
            PieMenusAssembler.AssembleFromDictionary(PieItemsFinder.AllItems);
        }
        #endregion

        #region Public Methods
        internal static void Off() {
            IsVisible = false;
            PieView?.PopOut();
            _targetSceneView.rootVisualElement.Remove(PieView);
        }
        #endregion

        #region Private Methods
        [MenuItem("Tools/Refresh Pie Menu")]
        private static void Refresh() {
            PieItemsFinder.Reset();
            PieMenusAssembler.Reset();
            GC.Collect();

            PieItemsFinder.CollectAllItems();
            PieMenusAssembler.AssembleFromDictionary(PieItemsFinder.AllItems);
        }

        [Shortcut("Toggle General Pie Menu", KeyCode.Q, ShortcutModifiers.Shift)]
        private static void ToggleOverlayGeneral() => TogglePieMenuWithContext(PieContext.General);

        [Shortcut("Toggle Add Pie Menu", KeyCode.A, ShortcutModifiers.Shift)]
        private static void ToggleOverlayAdd() => TogglePieMenuWithContext(PieContext.Create);

        [Shortcut("Toggle Utility Pie Menu", KeyCode.W, ShortcutModifiers.Shift)]
        private static void ToggleOverlayUtility() => TogglePieMenuWithContext(PieContext.Utility);
        #endregion

        #region Private Methods
        private static void TogglePieMenuWithContext(PieContext context) {
            IsVisible = !IsVisible;

            _targetSceneView = SceneView.lastActiveSceneView;
            if (IsVisible) {
                _targetSceneView.rootVisualElement.Add(PieView);
                var mousePosition = GetMousePosition();
                PieView.PopIn(mousePosition, PieMenusAssembler.Menus[context]);
                CollectSpatialInfo(mousePosition);
            } else {
                PieView.PopOut();
                _targetSceneView.rootVisualElement.Remove(PieView);
            }
        }

        private static Vector2 GetMousePosition() {
            if (_targetSceneView != EditorWindow.mouseOverWindow)
                return _targetSceneView.rootVisualElement.layout.size * .5f;

            if (EditorWindow.focusedWindow == _targetSceneView)
                return Event.current.mousePosition - Vector2.up * 44f;

            _targetSceneView.Focus();
            var pos = GUIUtility.GUIToScreenPoint(Event.current.mousePosition);
            pos -= _targetSceneView.position.position + Vector2.up * 44f;

            return pos;
        }

        private static void CollectSpatialInfo(Vector2 mousePosition) {
            PivotPoint = _targetSceneView.pivot;
            var pos = mousePosition / _targetSceneView.rootVisualElement.layout.size;
            pos.y = 1 - pos.y;
            var ray = _targetSceneView.camera.ViewportPointToRay(pos);
            HitPoint = Physics.Raycast(ray, out var hit) ? hit.point : PivotPoint;
        }
        #endregion
    }
}
#endif