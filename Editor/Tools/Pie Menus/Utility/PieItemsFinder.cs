﻿#if UNITY_2022_1_OR_NEWER
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Izinspector.Editors.Tools.PieMenus.Data;
using Izinspector.Runtime.PropertyAttributes;
using ItemsNestedDict = System.Collections.Generic.Dictionary<Izinspector.Runtime.PropertyAttributes.PieContext, System.Collections.Generic.Dictionary<string, Izinspector.Editors.Tools.PieMenus.Data.PieItem>>;

namespace Izinspector.Editors.Tools.PieMenus.Utility {
    internal static class PieItemsFinder {
        #region Constants
        private const BindingFlags k_methodsFlags = BindingFlags.Default | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;

        private static string[] ExcludedLibs { get; } = {
            "Unity.", "UnityEngine", "UnityEditor", "mscorlib", "netstandard", "unityplastic"
        };

        private static string[] ExcludedLibsRoots { get; } = {
            "Bee", "System", "Mono", "Microsoft", "JetBrains"
        };

        private const string k_warnItemsAlreadyInitialized = "Pie Items were already initialized\n";
        private const string k_warnDuplicateItemPath = "Pie Item path was defined multiple times\nPath: {0}";
        private const string k_progressTitle = "Collecting Pie Menus";
        private const string k_progressDescription = "Gathering all the available menus ...";
        private const string k_progressDescriptionFormat = "Gathering all the available static menus from \'{0}\'";
        #endregion

        #region Properties
        internal static ItemsNestedDict AllItems { get; private set; }
        #endregion

        #region Public Methods
        internal static void Reset() => AllItems = null;

        internal static void CollectAllItems() {
            if (AllItems != null) {
                Debug.LogWarning(k_warnItemsAlreadyInitialized);
                return;
            }

            ShowProgressStart();

            InitItems();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(IsValidAssembly).ToArray();
            var assemblyCount = (float)assemblies.Length;
            var i = -1;

            foreach (var assembly in assemblies) {
                var assemblyName = assembly.GetName().Name;

                ShowProgress(assemblyName, ++i / assemblyCount);

                foreach (var type in assembly.GetTypes()) {
                    var methods = type
                        .GetMethods(k_methodsFlags)
                        .Where(m => m.GetCustomAttribute<PieMenuItemAttribute>() != null && m.GetParameters().Length <= 0)
                        .ToList();

                    foreach (var mi in methods) {
                        var pieAttr = mi.GetCustomAttribute<PieMenuItemAttribute>();

                        if (AllItems![pieAttr.Context].ContainsKey(pieAttr.Path)) {
                            Debug.LogWarningFormat(k_warnDuplicateItemPath, pieAttr.Path);
                            continue;
                        }

                        Func<bool> validator = null;
                        if (!string.IsNullOrWhiteSpace(pieAttr.ValidatorName)) {
                            var validatorMethodInfo = methods.FirstOrDefault(m => m.Name == pieAttr.ValidatorName);
                            if (validatorMethodInfo != null)
                                validator = () => (bool)validatorMethodInfo.Invoke(null, null);
                        }

                        var pieItem = new PieItem(pieAttr.Path, () => mi.Invoke(null, null), validator, pieAttr.IconPath);
                        AllItems[pieAttr.Context].Add(pieAttr.Path, pieItem);
                    }
                }
            }

            ShowProgressEnd();
        }
        #endregion

        #region Private Methods
        private static void InitItems() {
            AllItems = new();

            foreach (PieContext context in Enum.GetValues(typeof(PieContext)))
                AllItems.Add(context, new Dictionary<string, PieItem>());
        }

        private static bool IsValidAssembly(Assembly assembly) {
            var name = assembly.GetName().Name;

            if (ExcludedLibs.Any(l => name.Contains(l)))
                return false;

            return !ExcludedLibsRoots.Any(l => name.StartsWith(l));
        }

        private static void ShowProgressStart() => EditorUtility.DisplayProgressBar(k_progressTitle, k_progressDescription, 0f);
        private static void ShowProgress(string assemblyName, float progress) => EditorUtility.DisplayProgressBar(k_progressTitle, string.Format(k_progressDescriptionFormat, assemblyName), progress);
        private static void ShowProgressEnd() => EditorUtility.ClearProgressBar();
        #endregion
    }
}
#endif