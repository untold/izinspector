﻿#if UNITY_2022_1_OR_NEWER
using System.Collections.Generic;
using System.Linq;
using Izinspector.Editors.Preferences;
using Izinspector.Editors.Tools.PieMenus.Data;
using Izinspector.Runtime.PropertyAttributes;
using UnityEngine;
using ItemsNestedDict = System.Collections.Generic.Dictionary<Izinspector.Runtime.PropertyAttributes.PieContext, System.Collections.Generic.Dictionary<string, Izinspector.Editors.Tools.PieMenus.Data.PieItem>>;

namespace Izinspector.Editors.Tools.PieMenus.Utility {
    internal static class PieMenusAssembler {
        #region Constants
        private const string k_warnMenusAlreadyAssembled = "Pie Menus have already been assembled\n";
        #endregion

        #region Private Variables
        internal static Dictionary<PieContext, PieMenu> Menus { get; private set; }
        #endregion

        #region Public Methods
        internal static void Reset() => Menus = null;

        internal static void AssembleFromDictionary(ItemsNestedDict allItems) {
            if (Menus != null) {
                Debug.LogWarning(k_warnMenusAlreadyAssembled);
                return;
            }

            Menus = new();

            foreach (var (ctx, dict) in allItems) {
                var groupedItems = dict.Values.GroupBy(i => {
                    i.Restore();
                    return i.Path.Split('/')[0];
                });

                var cachedMenu = new PieMenu(ctx.ToString().ToUpper());
                GetNestedMenus(in cachedMenu, groupedItems);

                if (IzinspectorPrefs.ShowDefaultPieActions)
                    PieDefaultActions.ContextualInjection(in cachedMenu, ctx);

                Menus.Add(ctx, cachedMenu);
            }
        }

        internal static void InjectDefaultActionsIntoMenus() {
            foreach (var (ctx, menu) in Menus)
                PieDefaultActions.ContextualInjection(in menu, ctx);
        }

        internal static void EjectDefaultActionsFromMenus() {
            foreach (var (ctx, menu) in Menus)
                PieDefaultActions.ContextualEjection(in menu, ctx);
        }
        #endregion

        #region Private Methods
        private static void GetNestedMenus(in PieMenu parentMenu, IEnumerable<IGrouping<string, PieItem>> groupedItems) {
            foreach (var group in groupedItems) {
                if (group.Count() == 1 && !group.First().MenuName.Contains('/')) {
                    parentMenu.AddItem(group.First());
                    continue;
                }

                var m = new PieMenu(group.Key.Split('/')[0]);
                foreach (var pieItem in group) {
                    pieItem.StepInto();
                    if (!pieItem.MenuName.Contains('/'))
                        m.AddItem(pieItem);
                }

                var itemsGroups = group.Where(i => i.MenuName.Contains('/')).GroupBy(i => i.MenuName.Split('/')[0]);
                GetNestedMenus(in m, itemsGroups);

                parentMenu.AddMenu(m);
                m.AddParentMenu(parentMenu);
            }
        }
        #endregion
    }
}
#endif