﻿#if UNITY_2022_1_OR_NEWER
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Izinspector.Editors.Tools.PieMenus.Utility {
    internal static partial class PieDefaultActions {
        #region Private Methods
        private static void CreatePrimitive(PrimitiveType? type = null) {
            Undo.IncrementCurrentGroup();

            var obj = type.HasValue ? GameObject.CreatePrimitive(type.Value) : new GameObject("Empty");
            Undo.RegisterCreatedObjectUndo(obj, $"Create {(type.HasValue ? "Primitive" : "Empty")}");

            Undo.RegisterFullObjectHierarchyUndo(obj, "Set Object Position");
            obj.transform.position = PieMenuController.HitPoint;

            Undo.SetCurrentGroupName($"Create {(type.HasValue ? "Primitive" : "Empty")} at Point");

            Selection.SetActiveObjectWithContext(obj, null);
        }

        private static void CreateParent(Vector3? point) {
            Undo.IncrementCurrentGroup();

            var obj = new GameObject("Parent") { transform = { position = point ?? Selection.activeTransform.position } };
            Undo.RegisterCreatedObjectUndo(obj, $"Create Empty");

            foreach (var t in Selection.transforms)
                Undo.SetTransformParent(t.transform, obj.transform, true, "Modify parent");

            Undo.SetCurrentGroupName($"Create Parent at Point");

            Selection.SetActiveObjectWithContext(obj, null);
        }

        private static void AlignView(bool viewToSelection) {
            var cameraTransform = SceneView.lastActiveSceneView.camera.transform;
            var activeTransform = Selection.activeTransform;

            if (viewToSelection) {
                SceneView.lastActiveSceneView.rotation = activeTransform.rotation;
                SceneView.lastActiveSceneView.pivot = activeTransform.position + cameraTransform.forward * SceneView.lastActiveSceneView.cameraDistance;
            } else {
                Undo.RegisterFullObjectHierarchyUndo(activeTransform, "Align Transform To View");
                activeTransform.position = cameraTransform.position;
                activeTransform.rotation = cameraTransform.rotation;
            }
        }

        private static void AlignCam(bool viewToCamera) {
#if UNITY_2023_1_OR_NEWER
            var cams = Object.FindObjectsByType<Camera>(FindObjectsSortMode.None).ToList();
#else
            var cams = Object.FindObjectsOfType<Camera>().ToList();
#endif
            if (cams.Count <= 0) {
                Debug.LogWarning("No cameras found\n");
                return;
            }

            var cameraTransform = (cams.FirstOrDefault(c => c.CompareTag("MainCamera")) ?? cams[0]).transform;

            if (viewToCamera) {
                SceneView.lastActiveSceneView.rotation = cameraTransform.rotation;
                SceneView.lastActiveSceneView.pivot = cameraTransform.position + cameraTransform.forward * SceneView.lastActiveSceneView.cameraDistance;
            } else {
                Undo.RegisterFullObjectHierarchyUndo(cameraTransform, "Align Camera To View");
                cameraTransform.position = SceneView.lastActiveSceneView.pivot + SceneView.lastActiveSceneView.rotation * Vector3.back * SceneView.lastActiveSceneView.cameraDistance;
                cameraTransform.rotation = SceneView.lastActiveSceneView.rotation;
            }
        }

        [Flags] internal enum TransformType { Position = 1, Rotation = 2, Scale = 4, All = ~0 }
        private static void ResetTransform(TransformType type) {
            var activeTransform = Selection.activeTransform;

            Undo.RegisterFullObjectHierarchyUndo(activeTransform, "Reset Selected Transform");
            if (type.HasFlag(TransformType.Position))
                activeTransform.localPosition = Vector3.zero;
            if (type.HasFlag(TransformType.Rotation))
                activeTransform.localRotation = Quaternion.identity;
            if (type.HasFlag(TransformType.Position))
                activeTransform.localScale = Vector3.one;
        }
        #endregion
    }
}
#endif