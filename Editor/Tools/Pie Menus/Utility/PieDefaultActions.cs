﻿#if UNITY_2022_1_OR_NEWER
using System;
using System.Linq;
using Izinspector.Editors.Tools.PieMenus.Data;
using Izinspector.Editors.Utility;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Tools.PieMenus.Utility {
    [InitializeOnLoad]
    internal static partial class PieDefaultActions {
        #region Properties
        private static PieMenu Reset { get; }

        private static PieMenu ObjectCreation { get; }

        private static PieMenu View { get; }
        private static PieMenu Camera { get; }
        #endregion

        #region Constructors
        static PieDefaultActions() {
            // --- GENERAL -----------------------------------------------------------------

            Reset = new PieMenu("Reset", () => Selection.transforms.Any());
            Reset.AddItem("Position", () => ResetTransform(TransformType.Position), icon: Strings.PieResetPosition);
            Reset.AddItem("Rotation", () => ResetTransform(TransformType.Rotation), icon: Strings.PieResetRotation);
            Reset.AddItem("Scale", () => ResetTransform(TransformType.Scale), icon: Strings.PieResetScale);
            Reset.AddItem("All", () => ResetTransform(TransformType.All), icon: Strings.PieResetAll);
         
            // --- CREATE -----------------------------------------------------------------

            ObjectCreation = new PieMenu("Create");

            var objects3D = new PieMenu("3D Objects");
            foreach (PrimitiveType type in Enum.GetValues(typeof(PrimitiveType)))
                objects3D.AddItem(type.ToString(), () => CreatePrimitive(type), icon: Strings.GetPieCreatePath(type));

            var emptyParent = new PieMenu("Empty Parent", () => Selection.transforms.Any());
            emptyParent.AddItem("At Pie Center", () => CreateParent(PieMenuController.HitPoint));
            emptyParent.AddItem("At Screen Center", () => CreateParent(PieMenuController.PivotPoint));
            emptyParent.AddItem("At World Origin", () => CreateParent(Vector3.zero));
            emptyParent.AddItem("At Active", () => CreateParent(null));

            ObjectCreation.AddItem("Empty", () => CreatePrimitive(), icon: Strings.PieCreateEmpty);
            ObjectCreation.AddMenu(objects3D);
            ObjectCreation.AddMenu(emptyParent);

            // --- UTILITY -----------------------------------------------------------------

            View = new PieMenu("Align", () => Selection.transforms.Any());
            View.AddItem("View To Selection", () => AlignView(true), icon: Strings.PieAlignViewToSelection);
            View.AddItem("Selection To View", () => AlignView(false), icon: Strings.PieAlignSelectionToView);
            Camera = new PieMenu("Camera");
            Camera.AddItem("View To Camera", () => AlignCam(true), icon: Strings.PieAlignViewToCamera);
            Camera.AddItem("Camera To View", () => AlignCam(false), icon: Strings.PieAlignCameraToView);
        }
        #endregion

        #region Public Methods
        internal static void ContextualInjection(in PieMenu cachedMenu, PieContext context) {
            switch (context) {
                case PieContext.General:
                    cachedMenu.AddMenu(Reset);
                    break;
                case PieContext.Create:
                    cachedMenu.AddMenu(ObjectCreation);
                    break;
                case PieContext.Utility:
                    cachedMenu.AddMenu(Camera);
                    cachedMenu.AddMenu(View);
                    break;
                default:
                    return;
            }
        }

        internal static void ContextualEjection(in PieMenu cachedMenu, PieContext context) {
            switch (context) {
                case PieContext.General:
                    cachedMenu.RemoveMenu(Reset);
                    break;
                case PieContext.Create:
                    cachedMenu.RemoveMenu(ObjectCreation);
                    break;
                case PieContext.Utility:
                    cachedMenu.RemoveMenu(Camera);
                    cachedMenu.RemoveMenu(View);
                    break;
                default:
                    return;
            }
        }
        #endregion
    }
}
#endif