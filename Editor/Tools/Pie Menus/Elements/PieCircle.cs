﻿#if UNITY_2022_1_OR_NEWER
using Izinspector.Editors.Utility;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.PieMenus.Elements {
    internal class PieCircle : VisualElement {
        #region Constants
        private const float k_thickness = 6f;
        #endregion

        #region Private Variables
        private readonly VisualElement _innerIndicator;
        private readonly float _radius;
        #endregion

        #region Constructors
        internal PieCircle(Vector2 position, float radius, Color color) {
            _radius = radius;

            style.position = Position.Absolute;
            style.left = position.x - radius;
            style.top = position.y - radius;
            style.width = radius * 2f;
            style.height = radius * 2f;
            style.borderTopLeftRadius = radius;
            style.borderTopRightRadius = radius;
            style.borderBottomRightRadius = radius;
            style.borderBottomLeftRadius = radius;
            style.backgroundColor = Color.clear;

            if (color.a <= 0.001f)
                return;

            style.borderLeftColor = color;
            style.borderTopColor = color;
            style.borderRightColor = color;
            style.borderBottomColor = color;

            style.borderLeftWidth = k_thickness;
            style.borderTopWidth = k_thickness;
            style.borderRightWidth = k_thickness;
            style.borderBottomWidth = k_thickness;

            _innerIndicator = new VisualElement() {
                style = {
                    position = Position.Relative,
                    flexGrow = 1f,
                    borderTopLeftRadius = radius,
                    borderTopRightRadius = radius,
                    borderBottomRightRadius = radius,
                    borderBottomLeftRadius = radius,
                    marginLeft = 1f - k_thickness,
                    marginTop = 1f - k_thickness,
                    marginRight = 1f - k_thickness,
                    marginBottom = 1f - k_thickness,
                    borderLeftColor = Colors.UnityWhite,
                    borderTopColor = Color.clear,
                    borderRightColor = Color.clear,
                    borderBottomColor = Color.clear,
                    borderLeftWidth = k_thickness - 2f,
                    borderTopWidth = k_thickness - 2f,
                    borderRightWidth = k_thickness - 2f,
                    borderBottomWidth = k_thickness - 2f,
                }
            };
            Add(_innerIndicator);
        }
        #endregion

        #region Public Methods
        internal void PointAt(Vector2 point) {
            if (_innerIndicator == null)
                return;

            var size = new Vector2(style.width.value.value, style.height.value.value);
            var dir = point - (/*layout.position + */size * .5f);
            dir.Normalize();

            var angle = Vector2.SignedAngle(Vector2.left, dir);
            _innerIndicator.transform.rotation = Quaternion.Euler(0, 0, angle);
            MarkDirtyRepaint();
        }

        internal void MoveAt(Vector2 position) {
            style.left = position.x - _radius;
            style.top = position.y - _radius;
            MarkDirtyRepaint();
        }
        #endregion
    }
}
#endif