#if UNITY_2022_1_OR_NEWER
using System;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.PieMenus.Elements {
    internal class PieButton : Button {
        #region Constants
        private const float k_iconSize = 16f;
        private const float k_arrowSize = 16f;
        private const float k_radius = 12f;
        private const float k_paddingShort = 4f;
        private const float k_paddingLong = 8f;
        #endregion

        #region Constructors
        internal PieButton(string label, string iconPath, Action onClick, int direction = 0) : base(onClick) {
            style.position = Position.Absolute;

            style.height = k_radius * 2f;
            style.minWidth = k_radius * 2f;
            style.borderTopLeftRadius = k_radius;
            style.borderTopRightRadius = k_radius;
            style.borderBottomRightRadius = k_radius;
            style.borderBottomLeftRadius = k_radius;

            style.borderLeftWidth = 0f;
            style.borderTopWidth = 0f;
            style.borderRightWidth = 0f;
            style.borderBottomWidth = 0f;

            style.fontSize = k_radius;

            text = label;

            var textWidth = EditorStyles.label.CalcSize(new GUIContent(label)).x;
            var totalWidth = textWidth;

            var hasIcon = !string.IsNullOrWhiteSpace(iconPath);
            if (hasIcon) {
                style.unityTextAlign = TextAnchor.MiddleRight;
                style.paddingRight = k_paddingLong;
                AddIconFromPath(iconPath);
                totalWidth += k_iconSize + k_paddingShort * 2f;
            }

            if (direction == 0) {
                style.width = totalWidth + k_paddingLong * 2;
                return;
            }

            // --- SubMenus and ParentMenu -----------

            if (direction == 1) {
                style.unityTextAlign = TextAnchor.MiddleLeft;
                style.paddingLeft = k_paddingLong;
                style.backgroundColor = Colors.UnityLightBackground;
            } else {
                style.unityTextAlign = TextAnchor.MiddleRight;
                style.paddingRight = k_paddingLong;
                style.backgroundColor = Colors.UnityLightBackground;
                style.opacity = .5f;
            }

            var img = new Image() {
                image = direction == 1 ? Icons.Next.image : Icons.Prev.image,
                style = {
                    position = Position.Absolute,
                    left = direction == 1 ? StyleKeyword.Auto : k_paddingShort,
                    top = k_paddingShort,
                    right = direction == 1 ? k_paddingShort : StyleKeyword.Auto,
                    bottom = k_paddingShort,
                    width = k_arrowSize,
                    height = k_arrowSize
                }
            };
            Add(img);
            totalWidth += k_arrowSize;

            style.width = totalWidth + k_paddingLong * 2;

            RegisterCallback<MouseEnterEvent>(OnMouseEnter);
            RegisterCallback<MouseLeaveEvent>(OnMouseLeave);
            RegisterCallback<MouseDownEvent>(OnMouseDown);
            RegisterCallback<MouseUpEvent>(OnMouseUp);
        }
        #endregion

        #region Private Methods
        private void AddIconFromPath(string iconPath) {
            var iconTexture = (Texture2D)EditorGUIUtility.Load(iconPath);
            var icon = new Image() {
                image = iconTexture,
                style = {
                    position = Position.Absolute,
                    left =k_paddingLong,
                    top = k_paddingShort,
                    right = StyleKeyword.Auto,
                    bottom = k_paddingShort,
                    width = k_iconSize,
                    height = k_iconSize
                }
            };
            Add(icon);
        }
        #endregion

        #region Event Methods
        private void OnMouseEnter(MouseEnterEvent evt) => style.backgroundColor = Colors.UnityLighterBackground;
        private void OnMouseLeave(MouseLeaveEvent evt) => style.backgroundColor = Colors.UnityLightBackground;
        private void OnMouseDown(MouseDownEvent evt) => style.backgroundColor = Colors.UnityBackground;
        private void OnMouseUp(MouseUpEvent evt) => style.backgroundColor = Colors.UnityLighterBackground;
        #endregion
    }
}
#endif