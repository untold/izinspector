#if UNITY_2022_1_OR_NEWER
using System.Collections.Generic;
using System.Linq;
using Izinspector.Editors.Tools.PieMenus;
using Izinspector.Editors.Tools.PieMenus.Data;
using Izinspector.Editors.Tools.PieMenus.Elements;
using Izinspector.Editors.Utility;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.Tools.Elements {
    internal class PieView : VisualElement {
        #region Constants
        private const float k_visualRadius = 15f;
        private const float k_radius = 75f;
        private const float k_outset = 150f;
        #endregion

        #region Private Variables
        private Vector2 _origin;
        private PieCircle _circleOuter;
        private PieCircle _circleInner;

        private readonly List<PieButton> _buttons = new();
        #endregion

        #region Constructors
        internal PieView() {
            style.flexGrow = 1f;
        }
        #endregion

        #region Public Methods
        internal void PopIn(Vector2 mousePos, in PieMenu pie) {
            var mPos = this.ChangeCoordinatesTo(this, mousePos);
            _origin = mPos;

            _circleInner = new PieCircle(_origin, k_visualRadius, Colors.UnityLighterBackground);

            _circleOuter = new PieCircle(_origin, k_outset, Color.clear);
            _circleOuter.RegisterCallback<MouseLeaveEvent>(OnMouseLeave);
            _circleOuter.RegisterCallback<MouseDownEvent>(OnMouseDown);

            Add(_circleInner);
            Add(_circleOuter);

            CreateButtonsForPie(pie);

            RegisterCallback<MouseMoveEvent>(OnMouseMove);
        }

        internal void PopOut() {
            Clear();
            _buttons.Clear();
        }
        #endregion

        #region Private Methods
        private void CreateButtonsForPie(PieMenu pie) {
            var i = -1;
            var totalCount = pie.Items.Count(item => item.CanDisplay) + pie.SubMenus.Count(item => item.CanDisplay) + (pie.ParentMenu != null ? 1 : 0);

            if (totalCount <= 0) {
                var emptyLabel = new Label("No Pie Items Set") {
                    style = {
                        unityTextAlign = TextAnchor.MiddleCenter,
                        width = 140f,
                        left = _origin.x - 70f,
                        top = _origin.y - 36f,
                        paddingLeft = 0f,
                        paddingRight = 0f,
                    }
                };
                Add(emptyLabel);
                return;
            }

            if (pie.ParentMenu != null) {
                var btn = new PieButton("Back", null, null, -1);
                btn.RegisterCallback<MouseLeaveEvent>(OnMouseLeave);
                btn.clicked += () => OpenMenu(pie.ParentMenu);
                PositionButton(btn, ++i / (float)totalCount, _origin);
                Add(btn);
                _buttons.Add(btn);
            }

            foreach (var item in pie.Items) {
                if (!item.CanDisplay)
                    continue;

                var btn = new PieButton(item.MenuName, item.IconPath, item.Execute);
                btn.RegisterCallback<MouseLeaveEvent>(OnMouseLeave);
                btn.clicked += PieMenuController.Off;
                PositionButton(btn, ++i / (float)totalCount, _origin);
                Add(btn);
                _buttons.Add(btn);
            }

            foreach (var menu in pie.SubMenus) {
                if (!menu.CanDisplay)
                    continue;

                var btn = new PieButton(menu.Name, null, null, 1);
                btn.RegisterCallback<MouseLeaveEvent>(OnMouseLeave);
                btn.clicked += () => OpenMenu(menu);
                PositionButton(btn, ++i / (float)totalCount, _origin);
                Add(btn);
                _buttons.Add(btn);
            }
        }

        private void PositionButton(Button button, float positionPercent, Vector2 origin) {
            var angle = Mathf.Lerp(0, 360f, positionPercent);
            var angleRad = Mathf.Deg2Rad * (angle - 180f);
            var height = button.style.height.value.value;
            var width = button.style.width.value.value;

            var pos = new Vector2(Mathf.Cos(angleRad), Mathf.Sin(angleRad)) * k_radius;
            pos.y -= height * .5f;
            if (Mathf.Abs(pos.x) > .01f)
                pos.x -= pos.x < 0 ? width - 12f : 12f;
            else
                pos.x -= width * .5f;
            pos += origin;

            var fromPos = origin - new Vector2(width, height) * .5f;
            button.experimental.animation.Start(fromPos.x, pos.x, 50, (e, v) => e.style.left = v);
            button.experimental.animation.Start(fromPos.y, pos.y, 50, (e, v) => e.style.top = v);
        }
        #endregion

        #region Event Methods
        private void OnMouseLeave(MouseLeaveEvent evt) {
            var mPos = (evt.target as VisualElement).ChangeCoordinatesTo(_circleOuter, evt.localMousePosition);
            if (!_circleOuter.ContainsPoint(mPos))
                PieMenuController.Off();
        }

        private static void OnMouseDown(MouseDownEvent mouseDownEvent) => PieMenuController.Off();

        private void OnMouseMove(MouseMoveEvent evt) => _circleInner.PointAt(this.ChangeCoordinatesTo(_circleInner, evt.localMousePosition));

        private void OpenMenu(PieMenu menu) {
            foreach (var pieButton in _buttons)
                try { Remove(pieButton); } catch {
                    // ignored
                }

            _buttons.Clear();

            CreateButtonsForPie(menu);
        }
        #endregion
    }
}
#endif