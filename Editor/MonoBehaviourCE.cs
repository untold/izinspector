using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Izinspector.Editors.Preferences;
using Izinspector.Editors.Utility;
using UnityEngine;
using UnityEditor;

namespace Izinspector.Editors {
    [CanEditMultipleObjects]
    [CustomEditor(typeof(MonoBehaviour), true)]
    public class MonoBehaviourCE : Editor {
        #region Public Variables
        #endregion

        #region Private Variables
        private FieldInfo[] _properties = Array.Empty<FieldInfo>();
        private Dictionary<string, FieldInfo[]> _groupedProperties = new();
        private FieldInfo[] _staticFields = Array.Empty<FieldInfo>();
        private FieldInfo[] _actionFields = Array.Empty<FieldInfo>();
        private MethodInfo[] _animEventsMethods = Array.Empty<MethodInfo>();
        private Dictionary<string, MethodInfo> _buttonsMethods = new();
        private Type[] _requirements = Type.EmptyTypes;

        private bool _staticsFoldout = true;
        private bool _actionsFoldout;
        private bool _animEventsFoldout;
        private Dictionary<string, bool> _groupsFoldouts = new();

        private string _scriptAssetPath;
        #endregion

        #region Overrides
        private void OnEnable() {
            if (!IzinspectorPrefs.InspectMonoBehaviours)
                return;

            if (target == null)
                return;

            if (!serializedObject.targetObject) {
                Debug.LogWarning("Check if a script is missing...\n");
                return;
            }

            _properties = Reflector.CollectProperties(serializedObject);
            _groupedProperties = Reflector.CollectGroupedProperties(serializedObject);
            _staticFields = Reflector.CollectStaticFields(serializedObject);
            _actionFields = Reflector.CollectActionFields(serializedObject);
            _animEventsMethods = Reflector.CollectAnimationEventsMethods(serializedObject);
            _buttonsMethods = Reflector.CollectButtonsMethods(serializedObject);
            _requirements = Reflector.CollectRequirements(serializedObject);

            _groupsFoldouts = new Dictionary<string, bool>();
            foreach (var group in _groupedProperties)
                _groupsFoldouts.Add(group.Key, EditorPrefs.GetBool(group.Key, false));

            var typeName = target.GetType().Name;
            var guid = AssetDatabase.FindAssets(typeName).FirstOrDefault();
            _scriptAssetPath = AssetDatabase.GUIDToAssetPath(guid);

            OnAfterEnabled();
        }

        public override void OnInspectorGUI () {
            if (!IzinspectorPrefs.InspectMonoBehaviours) {
                base.OnInspectorGUI();
                return;
            }

            serializedObject.Update();

            if (IzinspectorPrefs.ShowRequirements) {
                var components = new List<Component>();
                (serializedObject.targetObject as MonoBehaviour)!.GetComponents(components);
                foreach (var requiredType in _requirements) {
                    if (components.Exists(c => requiredType.IsInstanceOfType(c)))
                        continue;

                    EditorGUILayout.HelpBox($"A component of type \"{requiredType.BaseType!.Name}\" is required", MessageType.Warning);
                }
            }

            #region GUI Here
            if (IzinspectorPrefs.InspectPingableComponent) {
                GUILayout.Space(-2);
                GUILayout.BeginHorizontal();
                {
                    GUILayout.FlexibleSpace();
                    if (GUILayout.Button("Show", Styles.CustomAssetLabelLeft))
                        EditorGUIUtility.PingObject(AssetDatabase.LoadMainAssetAtPath(_scriptAssetPath));
                    if (GUILayout.Button("Open", Styles.CustomAssetLabelRight))
                        AssetDatabase.OpenAsset(AssetDatabase.LoadMainAssetAtPath(_scriptAssetPath));
                }
                GUILayout.EndHorizontal();
                GUILayout.Space(2);
            }

            OnPrependInspectorGUI();

            foreach (var prop in _properties)
                EditorGUILayout.PropertyField(serializedObject.FindProperty(prop.Name), true);

            foreach (var group in _groupedProperties) {
                if (!_groupsFoldouts.TryGetValue(group.Key, out var gFold))
                    continue;

                // EditorGUILayout.Space();
                InspectorDrawer.DrawFoldoutAsComponent(ref gFold, group.Key, () => {
                    foreach (var prop in group.Value)
                        EditorGUILayout.PropertyField(serializedObject.FindProperty(prop.Name), true);
                });
                _groupsFoldouts[group.Key] = gFold;
                EditorPrefs.SetBool(group.Key, gFold);
            }

            if (_staticFields.Any()) {
                EditorGUILayout.Space();
                InspectorDrawer.DrawFoldoutAsList(ref _staticsFoldout, "Statics", () => {
                    foreach (var staticField in _staticFields)
                        InspectorDrawer.DrawStaticField(staticField, serializedObject.targetObject);
                });
            }

            if (_actionFields.Any()) {
                EditorGUILayout.Space();
                InspectorDrawer.DrawFoldoutAsList(ref _actionsFoldout, "Actions", () => {
                    foreach (var actionField in _actionFields)
                        InspectorDrawer.DrawAction(actionField, serializedObject.targetObject);
                });
            }

            if (_animEventsMethods.Any()) {
                EditorGUILayout.Space();
                InspectorDrawer.DrawFoldoutAsList(ref _animEventsFoldout, "Animation Events", () => {
                    foreach (var evMethod in _animEventsMethods)
                        InspectorDrawer.DrawAnimationEvent(evMethod);
                });
            }

            if (_buttonsMethods.Any()) {
                EditorGUILayout.Space();
                foreach (var button in _buttonsMethods) {
                    if (GUILayout.Button(button.Key))
                        button.Value.Invoke(serializedObject.targetObject, null);
                }
            }

            OnAppendInspectorGUI();
            #endregion

            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        protected virtual void OnAfterEnabled() {}

        protected virtual void OnPrependInspectorGUI() {}
        protected virtual void OnAppendInspectorGUI() {}
        #endregion
    }
}