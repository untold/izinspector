﻿using UnityEditor.UIElements;

namespace Izinspector.Editors.UIElements.MonoScriptInspector {
    internal class TypeToolbar : Toolbar {
        #region Public Variables
        #endregion

        #region Private Variables
        private readonly ToolbarBreadcrumbs _breadcrumbs;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        public TypeToolbar() {
            _breadcrumbs = new ToolbarBreadcrumbs();
            Add(_breadcrumbs);
        }
        #endregion

        #region Public Methods
        internal void Set(string[] nameSpace) {
            _breadcrumbs.Clear();
            foreach (var ns in nameSpace)
                _breadcrumbs.PushItem(ns);
        }
        #endregion

        #region Private Methods
        #endregion
    }
}