﻿using System.Reflection;
using Izinspector.Editors.Utility;
using UnityEngine;
using UnityEngine.UIElements;

namespace Izinspector.Editors.UIElements.MonoScriptInspector {
    internal class MemberInfoElement : VisualElement {
        #region Public Variables
        #endregion

        #region Private Variables
        private readonly VisualElement _background;
        private readonly Label _memberReturnTypeLabel;
        private readonly Label _memberNameLabel;
        private readonly Label _memberArgsLabel;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        public MemberInfoElement() {
            style.backgroundColor = Color.clear;

            _background = new VisualElement() {
                style = {
                    width = StyleKeyword.Auto,
                    backgroundColor = Colors.UnityDarkBackground,
                    borderLeftWidth = 1f,
                    borderTopWidth = 1f,
                    borderRightWidth = 1f,
                    borderBottomWidth = 1f,
                    borderLeftColor = Colors.UnityToolbarLines,
                    borderTopColor = Colors.UnityToolbarLines,
                    borderRightColor = Colors.UnityToolbarLines,
                    borderBottomColor = Colors.UnityToolbarLines,
                    borderTopLeftRadius = 10.5f,
                    borderTopRightRadius = 10.5f,
                    borderBottomRightRadius = 10.5f,
                    borderBottomLeftRadius = 10.5f,
                    paddingLeft = 6f,
                    paddingTop = 2f,
                    paddingRight = 6f,
                    paddingBottom = 2f,
                    marginTop = 1f,
                    marginBottom = 1f,
                    flexShrink = 1f,
                    flexWrap = Wrap.NoWrap,
                    justifyContent = Justify.FlexStart,
                    alignItems = Align.FlexStart,
                    flexDirection = FlexDirection.Row,
                }
            };

            _memberReturnTypeLabel = new Label("TYPE") {
                name = "member-return-type",
                style = {
                    unityTextAlign = TextAnchor.MiddleLeft,
                    top = 0f,
                    bottom = 0f,
                    width = StyleKeyword.Auto,
                    flexShrink = 1f,
                    color = Colors.UnityBlue,
                }
            };
            _background.Add(_memberReturnTypeLabel);

            _memberNameLabel = new Label("NAME") {
                name = "member-name",
                style = {
                    unityTextAlign = TextAnchor.MiddleLeft,
                    top = 0f,
                    bottom = 0f,
                    width = StyleKeyword.Auto,
                    flexShrink = 1f,
                }
            };
            _background.Add(_memberNameLabel);

            _memberArgsLabel = new Label() {
                name = "member-args",
                style = {
                    unityTextAlign = TextAnchor.MiddleLeft,
                    top = 0f,
                    bottom = 0f,
                    width = StyleKeyword.Auto,
                    flexShrink = 1f,
                }
            };
            _background.Add(_memberArgsLabel);

            Add(_background);

            RegisterCallback<MouseEnterEvent>(OnMouseEnter);
            RegisterCallback<MouseLeaveEvent>(OnMouseLeave);
        }
        #endregion

        #region Public Methods
        public void Set(MemberInfo memberInfo) {
            switch (memberInfo.MemberType) {
                case MemberTypes.All:
                    break;
                case MemberTypes.Constructor:
                    _memberReturnTypeLabel.text = null;
                    _memberNameLabel.style.color = Colors.UnityRed;
                    _memberNameLabel.text = memberInfo.DeclaringType?.Name;
                    _memberArgsLabel.text = "( )";
                    break;
                case MemberTypes.Custom:
                    break;
                case MemberTypes.Event:
                    break;
                case MemberTypes.Field:
                    var fieldInfo = memberInfo as FieldInfo;
                    _memberReturnTypeLabel.style.color = fieldInfo!.FieldType.IsPrimitive ? Colors.UnityBlue : Colors.UnityRed;
                    _memberReturnTypeLabel.text = fieldInfo!.FieldType.ToCodeName();
                    _memberNameLabel.text = memberInfo.Name;
                    break;
                case MemberTypes.Method:
                    var methodInfo = memberInfo as MethodInfo;
                    _memberReturnTypeLabel.style.color = methodInfo!.ReturnType.IsPrimitive || methodInfo!.ReturnType.Name == "Void" ? Colors.UnityBlue : Colors.UnityRed;
                    _memberReturnTypeLabel.text = methodInfo!.ReturnType.ToCodeName();
                    _memberNameLabel.text = memberInfo.Name;
                    _memberArgsLabel.text = "( )";
                    break;
                case MemberTypes.NestedType:
                    break;
                case MemberTypes.Property:
                    var propertyInfo = memberInfo as PropertyInfo;
                    _memberReturnTypeLabel.style.color = propertyInfo!.PropertyType.IsPrimitive ? Colors.UnityBlue : Colors.UnityRed;
                    _memberReturnTypeLabel.text = propertyInfo!.PropertyType.ToCodeName();
                    _memberNameLabel.text = memberInfo.Name;
                    _memberArgsLabel.text = "{" + (propertyInfo.GetMethod != null ? " get;" : null) + (propertyInfo.SetMethod != null ? " set;" : null) + " }";
                    break;
                case MemberTypes.TypeInfo:
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Event Methods
        private void OnMouseEnter(MouseEnterEvent evt) {
            _background.style.borderLeftColor = Colors.UnityDarkBlue;
            _background.style.borderTopColor = Colors.UnityDarkBlue;
            _background.style.borderRightColor = Colors.UnityDarkBlue;
            _background.style.borderBottomColor = Colors.UnityDarkBlue;
            _background.style.backgroundColor = Colors.UnityBackground;
            evt.StopImmediatePropagation();
        }

        private void OnMouseLeave(MouseLeaveEvent evt) {
            _background.style.borderLeftColor = Colors.UnityToolbarLines;
            _background.style.borderTopColor = Colors.UnityToolbarLines;
            _background.style.borderRightColor = Colors.UnityToolbarLines;
            _background.style.borderBottomColor = Colors.UnityToolbarLines;
            _background.style.backgroundColor = Colors.UnityDarkBackground;
            evt.StopImmediatePropagation();
        }
        #endregion
    }
}