﻿using UnityEngine;

namespace Izinspector.Editors.Utility {
    internal static class Strings {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        internal static string PieCreateEmpty => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Create Empty.png";
        internal static string PieCreateSphere => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Create Sphere.png";
        internal static string PieCreateCapsule => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Create Capsule.png";
        internal static string PieCreateCylinder => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Create Cylinder.png";
        internal static string PieCreateCube => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Create Cube.png";
        internal static string PieCreatePlane => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Create Plane.png";
        internal static string PieCreateQuad => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Create Quad.png";

        internal static string PieAlignViewToCamera => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Align View With Cam.png";
        internal static string PieAlignCameraToView => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Align Cam With View.png";

        internal static string PieAlignViewToSelection => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Align View.png";
        internal static string PieAlignSelectionToView => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Align With View.png";
        internal static string PieResetPosition => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Reset Position.png";
        internal static string PieResetRotation => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Reset Rotation.png";
        internal static string PieResetScale => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Reset Scale.png";
        internal static string PieResetAll => "Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Reset All.png";
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        internal static string GetPieCreatePath(PrimitiveType? type) => type.HasValue ? type switch {
            PrimitiveType.Sphere => PieCreateSphere,
            PrimitiveType.Capsule => PieCreateCapsule,
            PrimitiveType.Cylinder => PieCreateCylinder,
            PrimitiveType.Cube => PieCreateCube,
            PrimitiveType.Plane => PieCreatePlane,
            PrimitiveType.Quad => PieCreateQuad,
            _ => null
        } : PieCreateEmpty;
        #endregion

        #region Private Methods
        #endregion
    }
}