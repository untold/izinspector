﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Izinspector.Runtime.PropertyAttributes;
using Izinspector.Runtime.PropertyAttributes.Filters;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Utility {
    internal static class Reflector {
        #region Constants
        private const BindingFlags k_serializableFieldsFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.FlattenHierarchy;
        private const BindingFlags k_allFieldsFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy;
        #endregion

        #region Public Methods
        internal static FieldInfo[] CollectProperties(in SerializedObject serializedObject) {
            var properties = new FieldInfo[0];

            var t = serializedObject.targetObject.GetType();
            var fields = t.GetFields(k_serializableFieldsFlags);

            while (t != typeof(MonoBehaviour) && t != typeof(ScriptableObject)) {
                t = t!.BaseType;
                fields = t!.GetFields(k_serializableFieldsFlags).Concat(fields).ToArray();
            }

            foreach (var fi in fields) {
                if (fi.GetCustomAttribute<NonSerializedAttribute>() != null)
                    continue;

                if (fi.GetCustomAttribute<HideInInspector>() != null)
                    continue;

                if (fi.GetCustomAttribute<HideAttribute>() != null)
                    continue;

                if (!fi.IsPublic && fi.GetCustomAttribute<SerializeField>() == null && fi.GetCustomAttribute<SerializeReference>() == null)
                    continue;

                if (serializedObject.FindProperty(fi.Name) == null)
                    continue;

                var attr = fi.GetCustomAttribute<GroupAttribute>();
                if (attr != null)
                    continue;

                properties = properties.Append(fi).ToArray();
            }

            return properties;
        }

        internal static Dictionary<string, FieldInfo[]> CollectGroupedProperties(in SerializedObject serializedObject) {
            var properties = new Dictionary<string, FieldInfo[]>();

            var t = serializedObject.targetObject.GetType();
            var fields = t.GetFields(k_serializableFieldsFlags);

            while (t != typeof(MonoBehaviour) && t != typeof(ScriptableObject)) {
                t = t!.BaseType;
                fields = t!.GetFields(k_serializableFieldsFlags).Concat(fields).ToArray();
            }

            foreach (var fi in fields) {
                if (fi.GetCustomAttribute<NonSerializedAttribute>() != null)
                    continue;

                if (fi.GetCustomAttribute<HideInInspector>() != null)
                    continue;

                if (fi.GetCustomAttribute<HideAttribute>() != null)
                    continue;

                if (!fi.IsPublic && fi.GetCustomAttribute<SerializeField>() == null && fi.GetCustomAttribute<SerializeReference>() == null)
                    continue;

                if (serializedObject.FindProperty(fi.Name) == null)
                    continue;

                var attr = fi.GetCustomAttribute<GroupAttribute>();
                if (attr == null)
                    continue;

                properties.TryAdd(attr.GroupName, Array.Empty<FieldInfo>());
                properties[attr.GroupName] = properties[attr.GroupName].Append(fi).ToArray();
            }

            return properties;
        }

        internal static FieldInfo[] CollectStaticFields(in SerializedObject serializedObject) {
            var staticFields = Array.Empty<FieldInfo>();

            var t = serializedObject.targetObject.GetType();
            var fields = t.GetFields(k_allFieldsFlags);

            while (t != typeof(MonoBehaviour) && t != typeof(ScriptableObject)) {
                t = t!.BaseType;
                fields = t!.GetFields(k_allFieldsFlags).Concat(fields).ToArray();
            }

            foreach (var fi in fields) {
                if (fi.GetCustomAttribute<InspectableAttribute>() == null)
                    continue;

                staticFields = staticFields.Append(fi).ToArray();
            }

            return staticFields;
        }

        internal static FieldInfo[] CollectActionFields(in SerializedObject serializedObject) {
            var actionFields = Array.Empty<FieldInfo>();

            var t = serializedObject.targetObject.GetType();
            var fields = t.GetFields(k_allFieldsFlags);

            while (t != typeof(MonoBehaviour) && t != typeof(ScriptableObject)) {
                t = t!.BaseType;
                fields = t!.GetFields(k_allFieldsFlags).Concat(fields).ToArray();
            }

            foreach (var fi in fields) {
                if (fi.GetCustomAttribute<DelegateInspectorAttribute>() == null)
                    continue;

                if (!typeof(Delegate).IsAssignableFrom(fi.FieldType))
                    continue;

                actionFields = actionFields.Append(fi).ToArray();
            }

            return actionFields;
        }

        internal static MethodInfo[] CollectAnimationEventsMethods(in SerializedObject serializedObject) {
            var animEventsMethods = Array.Empty<MethodInfo>();

            var t = serializedObject.targetObject.GetType();
            var methods = t.GetMethods(k_allFieldsFlags);
            while (t != typeof(MonoBehaviour) && t != typeof(ScriptableObject)) {
                t = t!.BaseType;
                methods = t!.GetMethods(k_allFieldsFlags).Concat(methods).ToArray();
            }

            foreach (var me in methods) {
                if (me.GetCustomAttribute<AnimationEventAttribute>() == null)
                    continue;
        
                animEventsMethods = animEventsMethods.Append(me).ToArray();
            }

            return animEventsMethods;
        }

        internal static Dictionary<string, MethodInfo> CollectButtonsMethods(in SerializedObject serializedObject) {
            var buttonsMethods = new Dictionary<string, MethodInfo>();

            var t = serializedObject.targetObject.GetType();
            var methods = t.GetMethods(k_allFieldsFlags);
            while (t != typeof(MonoBehaviour) && t != typeof(ScriptableObject)) {
                t = t!.BaseType;
                methods = t!.GetMethods(k_allFieldsFlags).Concat(methods).ToArray();
            }

            foreach (var me in methods) {
                var attr = me.GetCustomAttribute<ButtonAttribute>();
                if (attr == null)
                    continue;

                if (me.GetParameters().Length > 0)
                    continue;

                buttonsMethods.Add(attr.Label, me);
            }

            return buttonsMethods;
        }

        internal static Type[] CollectRequirements(in SerializedObject serializedObject) {
            var typeRequirements = new HashSet<Type>();

            var t = serializedObject.targetObject.GetType();
            var baseCompAttrs = t.GetCustomAttributes<RequireBaseComponentAttribute>();

            foreach (var attr in baseCompAttrs)
                typeRequirements.Add(attr.RequiredType);

            return typeRequirements.ToArray();
        }
        #endregion
    }
}