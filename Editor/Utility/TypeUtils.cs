﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;

namespace Izinspector.Editors.Utility {
    internal static class TypeUtils {
        #region Constants
        private const BindingFlags k_membersFlags = BindingFlags.Default | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
        #endregion

        #region Public Methods
        internal static void ToType(this MonoScript script, out Type type) {
            type = null;

            string nameSpace = null;

            if (script.text.Contains("namespace")) {
                var sr = new StringReader(script.text);
                var line = sr.ReadLine();
                while (!line.Contains("namespace"))
                    line = sr.ReadLine();
                nameSpace = line.Split(' ')[1];
            }

            foreach ( var assembly in AppDomain.CurrentDomain.GetAssemblies()) {
                foreach (var ty in assembly.GetTypes()) {
                    if (ty.Namespace != nameSpace)
                        continue;

                    if (ty.Name != script.name)
                        continue;

                    type = ty;
                    return;
                }
            }
        }

        internal static List<MemberInfo> GetPublicMembers(this Type type) => type.GetMembers(k_membersFlags | BindingFlags.Public).Where(m => m is not MethodInfo { IsSpecialName: true }).ToList();
        internal static List<MemberInfo> GetPrivateMembers(this Type type) => type.GetMembers(k_membersFlags | BindingFlags.NonPublic).ToList();

        internal static string ToCodeName(this Type type) {
            if (type == typeof(void))
                return "void";

            if (type.IsPrimitive)
                return Type.GetTypeCode(type).ToString().ToLowerInvariant();

            if (!type.IsGenericType)
                return type.Name;

            var gen = type.GetGenericTypeDefinition();
            var name = gen.Name.Split("`")[0];
            name += $"<{string.Join(", ", type.GetGenericArguments().Select(t => t.ToCodeName()))}>";

            return name;
        }
        #endregion
    }
}