﻿using UnityEngine;

namespace Izinspector.Editors.Utility {
    internal static class Colors {
        #region Properties
        internal static Color UnityWindowBorder { get; } = new(.1f, .1f, .1f);
        internal static Color UnityWindowHeaderBackground { get; } = new(.16f, .16f, .16f);
        internal static Color UnityDarkBackground { get; } = new(.2f, .2f, .2f);
        internal static Color UnityBackground { get; } = new(.22f, .22f, .22f);
        internal static Color UnityLightBackground { get; } = new(.25f, .25f, .25f);
        internal static Color UnityLighterBackground { get; } = new(.35f, .35f, .35f);

        internal static Color UnityToolbar { get; } = new(.24f, .24f, .24f);
        internal static Color UnityToolbarHighlight { get; } = new(.27f, .27f, .27f);
        internal static Color UnityToolbarLines { get; } = new(.14f, .14f, .14f);

        internal static Color UnityRed { get; } = new(.79f, .27f, .38f);
        internal static Color UnityGreen { get; } = new(.27f, .79f, .38f);
        internal static Color UnityBlue { get; } = new(.27f, .38f, .79f);
        internal static Color UnityYellow { get; } = new(.79f, .79f, .38f);

        internal static Color UnityDarkBlue { get; } = new(.27f, .38f, .49f);

        internal static Color UnitySelection { get; } = new(.17f, .36f, .53f);
        internal static Color UnitySelectionHighlight { get; } = new(.21f, .4f, .57f);

        internal static Color UnityBrightWhite { get; } = new(.78f, .78f, .78f);
        internal static Color UnityWhite { get; } = new(.64f, .64f, .64f);

        internal static Color UnityGrey { get; } = new(.4f, .4f, .4f);
        #endregion

        #region Public Methods
        internal static Color Alpha(this Color color, float alpha) => new(color.r, color.g, color.b, alpha);
        #endregion
    }
}