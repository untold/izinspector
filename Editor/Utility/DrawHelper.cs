﻿using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Utility {
    public static class DrawHelper {
        #region Public Methods
        public static bool DrawSolidFoldout(bool expanded, string label) {
            GUILayout.BeginHorizontal(Styles.ComponentHeaderHeavy);
            {
                EditorGUI.BeginChangeCheck();
                var foldout = EditorGUILayout.Foldout(expanded, label, true);
                if (EditorGUI.EndChangeCheck())
                    expanded = foldout;
            }
            GUILayout.EndHorizontal();

            return expanded;
        }
        #endregion
    }
}