﻿using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Utility {
    internal static class Icons {
        #region Private Variables
        private static GUIContent _lockIcon;
        private static GUIContent _debugOnIcon, _debugOffIcon;

        private static GUIContent _canvasIcon, _cameraIcon, _audioIcon;

        private static GUIContent _refreshIcon, _prevIcon, _nextIcon;

        private static Texture _starOnIcon, _starOnDisabledIcon, _starOffIcon;
        private static Texture _showIcon, _showDisabledIcon, _hideIcon;
        private static Texture _dropdownOnIcon, _dropdownOffIcon;
        private static Texture _inBuildIcon, _notInBuildIcon;

        private static Texture _rmBulletPointDotStyle, _rmBulletPointRomboidStyle, _rmBulletPointAwworStyle;

        private static Texture _pieCreateEmptyStyle;
        private static Texture _loadingStyle;

        private static Texture2D _iconPrefab, _iconTexture, _iconAnimation, _iconNetwork, _iconMaterial, _iconModel, _iconScene, _iconScript;
        #endregion

        #region Properties
        internal static GUIContent LockIcon => _lockIcon ??= EditorGUIUtility.IconContent("d_InspectorLock");

        internal static GUIContent DebugOn => _debugOnIcon ??= EditorGUIUtility.IconContent("d_DebuggerAttached");
        internal static GUIContent DebugOff => _debugOffIcon ??= EditorGUIUtility.IconContent("d_DebuggerDisabled");

        internal static GUIContent Edit => _debugOffIcon ??= EditorGUIUtility.IconContent("d_editicon.sml");

        internal static GUIContent Canvas => _canvasIcon ??= EditorGUIUtility.IconContent("Canvas Icon");
        internal static GUIContent Camera => _cameraIcon ??= EditorGUIUtility.IconContent("Camera Gizmo");
        internal static GUIContent Audio => _audioIcon ??= EditorGUIUtility.IconContent("AudioSource Gizmo");

        internal static GUIContent Refresh => _refreshIcon ??= EditorGUIUtility.IconContent("d_Refresh");
        internal static GUIContent Prev => _prevIcon ??= EditorGUIUtility.IconContent("d_tab_prev");
        internal static GUIContent Next => _nextIcon ??= EditorGUIUtility.IconContent("d_tab_next");

        internal static Texture StarOn => _starOnIcon ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/SW Favorite On.png");
        internal static Texture StarOnDisabled => _starOnDisabledIcon ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/SW Favorite On Disabled.png");
        internal static Texture StarOff => _starOffIcon ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/SW Favorite Off.png");

        internal static Texture Show => _showIcon ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/SW Shown.png");
        internal static Texture ShowDisabled => _showDisabledIcon ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/SW Shown Disabled.png");
        internal static Texture Hide => _hideIcon ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/SW Hidden.png");

        internal static Texture InBuild => _inBuildIcon ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/SW InBuildSettings.png");
        internal static Texture NotInBuild => _notInBuildIcon ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/SW NotInBuildSettings.png");

        internal static Texture DropdownOn => _dropdownOnIcon ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Dropdown On.png");
        internal static Texture DropdownOff => _dropdownOffIcon ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Dropdown Off.png");

        internal static Texture RMBulletPointDot => _rmBulletPointDotStyle ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/RM BulletPoint - Dot.png");
        internal static Texture RMBulletPointRomboid => _rmBulletPointRomboidStyle ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/RM BulletPoint - Romboid.png");
        internal static Texture RMBulletPointArrow => _rmBulletPointAwworStyle ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/RM BulletPoint - Arrow.png");

        internal static Texture PieCreateEmpty => _pieCreateEmptyStyle ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Pie Menu/Pie - Create Empty.png");

        internal static Texture Loading => _loadingStyle ??= (Texture)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Models Browser/Loading - Image.png");

        internal static Texture2D Prefab => _iconPrefab ??= (Texture2D)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Overlays/FolderOverlay - Prefabs.png");
        internal static Texture2D Texture => _iconTexture ??= (Texture2D)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Overlays/FolderOverlay - Textures.png");
        internal static Texture2D Animation => _iconAnimation ??= (Texture2D)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Overlays/FolderOverlay - Animations.png");
        internal static Texture2D Network => _iconNetwork ??= (Texture2D)EditorGUIUtility.IconContent("NetworkView Icon").image;
        internal static Texture2D Material => _iconMaterial ??= (Texture2D)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Overlays/FolderOverlay - Materials.png");
        internal static Texture2D Model => _iconModel ??= (Texture2D)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Overlays/FolderOverlay - Models.png");
        internal static Texture2D Scene => _iconScene ??= (Texture2D)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Overlays/FolderOverlay - Scenes.png");

        internal static Texture2D Script => _iconScript ??= (Texture2D)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Icons/Overlays/FolderOverlay - Scripts.png");
        #endregion
    }
}