﻿using System;
using System.Reflection;
using Izinspector.Runtime.PropertyAttributes.Filters;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Izinspector.Editors.Utility {
    internal static class InspectorDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        internal static void DrawFoldoutAsList(ref bool foldout, string label, Action drawContents) {
            GUILayout.BeginHorizontal(Styles.ListHeader);
            {
                GUILayout.Space(16);
                foldout = EditorGUILayout.Foldout(foldout, label, true);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginVertical(Styles.ListBackground);
            {
                if (foldout)
                    drawContents?.Invoke();
                else {
                    GUILayout.Space(-10);
                    GUILayout.Label("...", EditorStyles.centeredGreyMiniLabel);
                    GUILayout.Space(-4);
                }
            }
            GUILayout.EndVertical();
        }

        internal static void DrawFoldoutAsComponent(ref bool foldout, string label, Action drawContents) {
            GUILayout.BeginHorizontal(Styles.ComponentHeader);
            {
                foldout = EditorGUILayout.Foldout(foldout, label, true);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            {
                GUILayout.Space(16);
                GUILayout.BeginVertical();
                {
                    if (foldout)
                        drawContents?.Invoke();
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();

            var r = GUILayoutUtility.GetLastRect();
            r.y = r.yMax + (foldout ? 2f : 0f);
            r.height = 1f;
            GUI.Box(r, GUIContent.none, Styles.SeparatorLine);
        }

        internal static void DrawStaticField(in FieldInfo fieldInfo, in Object owner) {
            if (!fieldInfo.IsStatic)
                return;

            var isConst = fieldInfo.IsLiteral && !fieldInfo.IsInitOnly;

            var fi = fieldInfo;
            var ow = owner;

            var attr = fieldInfo.GetCustomAttribute<InspectableAttribute>();

            var val = fieldInfo.GetValue(owner);
            EditorGUILayout.BeginHorizontal();
            {
                var displayName = ObjectNames.NicifyVariableName(fieldInfo.Name.StartsWith("k_") ? fieldInfo.Name[2..] : fieldInfo.Name);
                if (isConst) displayName = $"[K] {displayName}";
                EditorGUILayout.LabelField(displayName, GUILayout.Width(EditorGUIUtility.labelWidth));
                GUI.enabled = !attr.IsReadOnly && !isConst;

                void OnValueChanged(object newVal) => fi.SetValue(ow, newVal);

                ShowStyledValue(val, fieldInfo.FieldType, OnValueChanged);
                GUI.enabled = true;
            }
            EditorGUILayout.EndHorizontal();
        }

        internal static void DrawAction(in FieldInfo fieldInfo, in Object owner) {
            var argsTypes = fieldInfo.FieldType.GetGenericArguments();
            var val = fieldInfo.IsStatic ? (Delegate)fieldInfo.GetValue(null) : (Delegate)fieldInfo.GetValue(owner);

            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.Label(ObjectNames.NicifyVariableName(fieldInfo.Name), Styles.AssetLabel);
                foreach (var argType in argsTypes)
                    TypedAssetLabel(argType);
                GUILayout.FlexibleSpace();
                GUILayout.Label(GUIContent.none, Styles.BreadcrumbsSeparator);
                GUILayout.Label(val != null ? val.GetInvocationList().Length.ToString() : "NULL", Styles.AssetLabel, GUILayout.Width(40));
            }
            EditorGUILayout.EndHorizontal();

            if (val == null)
                return;

            foreach (var invoke in val.GetInvocationList()) {
                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label(invoke.Method.Name, GUILayout.Width(EditorGUIUtility.labelWidth));
                    // GUILayout.FlexibleSpace();
                    if (GUILayout.Button(invoke.Target?.GetType().Name, GUILayout.ExpandWidth(true)))
                        EditorGUIUtility.PingObject(((Object)invoke.Target));
                }
                GUILayout.EndHorizontal();
            }
        }

        internal static void DrawAnimationEvent(in MethodInfo methodInfo) {
            var argsTypes = methodInfo.GetParameters();
            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.Label(ObjectNames.NicifyVariableName(methodInfo.Name), Styles.AssetLabel);
                foreach (var argType in argsTypes)
                    TypedAssetLabel(argType);
            }
            EditorGUILayout.EndHorizontal();
        }
        #endregion

        #region Private Methods
        private static void ShowStyledValue(object val, Type fieldType, Action<object> onValueChanged) {
            if (fieldType.IsSubclassOf(typeof(Enum))) {
                EditorGUILayout.LabelField(val.ToString(), EditorStyles.popup);
                return;
            }

            if (fieldType.IsSubclassOf(typeof(Object))) {
                EditorGUILayout.ObjectField((val as Object), fieldType, true);
                return;
            }

            switch (fieldType.Name) {
                case "Single" or "Int32" or "String":
                    EditorGUILayout.LabelField(val.ToString(), EditorStyles.textField);
                    return;
                case "Boolean":
                    EditorGUI.BeginChangeCheck();
                    dynamic newVal = EditorGUILayout.Toggle((bool)val);
                    if (EditorGUI.EndChangeCheck())
                        onValueChanged?.Invoke(newVal);
                    return;
                case "Vector2":
                    newVal = EditorGUILayout.Vector2Field(GUIContent.none, (Vector2)val);
                    if (EditorGUI.EndChangeCheck())
                        onValueChanged?.Invoke(newVal);
                    return;
                case "Vector3":
                    newVal = EditorGUILayout.Vector2Field(GUIContent.none, (Vector3)val);
                    if (EditorGUI.EndChangeCheck())
                        onValueChanged?.Invoke(newVal);
                    return;
                case "Quaternion":
                    newVal = EditorGUILayout.Vector3Field(GUIContent.none, ((Quaternion)val).eulerAngles);
                    if (EditorGUI.EndChangeCheck())
                        onValueChanged?.Invoke(newVal);
                    return;
            }

            EditorGUILayout.HelpBox($"Type {fieldType.Name} is not supported", MessageType.Warning, true);
        }

        private static void TypedAssetLabel(ParameterInfo paramInfo) {
            var displayName = ObjectNames.NicifyVariableName(paramInfo.Name);
            var originalColor = GUI.color;
            GUI.color = paramInfo.ParameterType.Name switch {
                "Boolean" => new Color(0.5f, 0.13f, 0.5f, 0.6f),
                "String" => new Color(0.13f, 0.5f, 0.29f, 0.6f),
                "Int32" => new Color(0.5f, 0.29f, 0.13f, 0.6f),
                "Single" => new Color(0.5f, 0.13f, 0.29f, 0.6f),
                "Vector2" => new Color(0.5f, 0.5f, 0.13f, 0.6f),
                "Vector3" => new Color(0.13f, 0.5f, 0.5f, 0.6f),
                _ => new Color(0.13f, 0.29f, 0.5f, 0.6f)
            };
            GUILayout.Label(displayName, Styles.CustomAssetLabel);
            GUI.color = originalColor;
            GUI.Label(GUILayoutUtility.GetLastRect(), displayName, Styles.CustomAssetLabelText);
        }

        private static void TypedAssetLabel(Type type) {
            var displayName = ObjectNames.NicifyVariableName(type.Name);
            var originalColor = GUI.color;
            GUI.color = type.Name switch {
                "Boolean" => new Color(0.5f, 0.13f, 0.5f, 0.6f),
                "String" => new Color(0.13f, 0.5f, 0.29f, 0.6f),
                "Int32" => new Color(0.5f, 0.29f, 0.13f, 0.6f),
                "Single" => new Color(0.5f, 0.13f, 0.29f, 0.6f),
                "Vector2" => new Color(0.5f, 0.5f, 0.13f, 0.6f),
                "Vector3" => new Color(0.13f, 0.5f, 0.5f, 0.6f),
                _ => new Color(0.13f, 0.29f, 0.5f, 0.6f)
            };
            GUILayout.Label(displayName, Styles.CustomAssetLabel);
            GUI.color = originalColor;
            GUI.Label(GUILayoutUtility.GetLastRect(), displayName, Styles.CustomAssetLabelText);
        }
        #endregion
    }
}