﻿using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Utility {
    internal static class Styles {
        #region Public Variables
        #endregion

        #region Private Variables
        private static GUISkin _skin;

        private static GUIStyle _assetLabelStyle, _customAssetLabelStyle, _customAssetLabelLeftStyle, _customAssetLabelRightStyle, _customAssetLabelTextStyle, _breadcrumbsSeparatorStyle;
        private static GUIStyle _listHeaderStyle, _listBackgroundStyle;
        private static GUIStyle _componentHeaderStyle, _componentHeaderLightStyle, _componentHeaderHeavyStyle, _separatorLineStyle;

        private static GUIStyle _rmBigHeaderStyle, _rmHeaderStyle, _rmSectionStyle, _rmNormalStyle, _rmBulletPointStyle, _rmTableStyle;

        private static GUIStyle _hierarchyGroupStyle, _hierarchyGroupCollapsedStyle, _hierarchyGroupSelectedStyle, _hierarchyGroupCollapsedSelectedStyle, _hierarchyGroupDisabledStyle;
        private static GUIStyle[] _hierarchyHighlightStyles = new GUIStyle[5];
        private static GUIStyle _hierarchyTreeEntryStyle, _hierarchyTreeEntryParallelStyle, _hierarchyTreeEntryEndpointStyle, _hierarchyTreeEntryParallelEndpointStyle;
        private static GUIStyle _filesDetailTextStyle, _filesDetailTextSelectedStyle;

        private static GUIStyle _toggleDisabled;

        private static GUIStyle _foldoutLeftStyle, _foldoutRightStyle;
        #endregion

        #region Properties
        private static GUISkin Skin => _skin ??= (GUISkin)EditorGUIUtility.Load("Packages/com.itstheconnection.izinspector/Editor Default Resources/Izinspector GUISkin.guiskin");

        internal static GUIStyle AssetLabel => _assetLabelStyle ??= new GUIStyle("AssetLabel") {
            margin = new RectOffset(2, 2, 1, 2),
            fixedHeight = 16
        };
        internal static GUIStyle CustomAssetLabel => _customAssetLabelStyle ??= Skin.GetStyle("Asset Label");
        internal static GUIStyle CustomAssetLabelLeft => _customAssetLabelLeftStyle ??= Skin.GetStyle("Asset Label Left");
        internal static GUIStyle CustomAssetLabelRight => _customAssetLabelRightStyle ??= Skin.GetStyle("Asset Label Right");
        internal static GUIStyle CustomAssetLabelText => _customAssetLabelTextStyle ??= new GUIStyle("label") {
            margin = new RectOffset(2, 2, 0, 0),
            fontSize = 10,
            alignment = TextAnchor.UpperCenter,
            padding = AssetLabel.padding,
            fixedHeight = AssetLabel.fixedHeight
        };
        internal static GUIStyle BreadcrumbsSeparator => _breadcrumbsSeparatorStyle ??= new GUIStyle("BreadcrumbsSeparator") {
            margin = new RectOffset(2, 2, 1, 2),
            fixedHeight = 16
        };

        internal static GUIStyle ListHeader => _listHeaderStyle ??= new GUIStyle("RL Header") {
            padding = new RectOffset(4, 4, 1, 1)
        };
        internal static GUIStyle ListBackground => _listBackgroundStyle ??= new GUIStyle("RL Background") {
            padding = new RectOffset(8, 8, 8, 8)
        };

        internal static GUIStyle ComponentHeader => _componentHeaderStyle ??= Skin.GetStyle("Component Foldout Header");
        internal static GUIStyle ComponentHeaderLight => _componentHeaderLightStyle ??= Skin.GetStyle("Component Foldout Header Light");
        internal static GUIStyle ComponentHeaderHeavy => _componentHeaderHeavyStyle ??= Skin.GetStyle("Component Foldout Header Heavy");
        internal static GUIStyle SeparatorLine => _separatorLineStyle ??= Skin.GetStyle("Separator Line");

        internal static GUIStyle RMBigHeader => _rmBigHeaderStyle ??= Skin.GetStyle("Big Header");
        internal static GUIStyle RMHeader => _rmHeaderStyle ??= Skin.GetStyle("Header");
        internal static GUIStyle RMSection => _rmSectionStyle ??= Skin.GetStyle("Section");
        internal static GUIStyle RMNormal => _rmNormalStyle ??= Skin.GetStyle("Normal");
        internal static GUIStyle RMBulletPoint => _rmBulletPointStyle ??= Skin.GetStyle("Bullet Point");
        internal static GUIStyle RMTable => _rmTableStyle ??= Skin.GetStyle("Table");

        internal static GUIStyle HierarchyGroup => _hierarchyGroupStyle ??= Skin.GetStyle("Hierarchy Group");
        internal static GUIStyle HierarchyGroupCollapsed => _hierarchyGroupCollapsedStyle ??= Skin.GetStyle("Hierarchy Group Collapsed");
        internal static GUIStyle HierarchyGroupSelected => _hierarchyGroupSelectedStyle ??= Skin.GetStyle("Hierarchy Group Selected");
        internal static GUIStyle HierarchyGroupCollapsedSelected => _hierarchyGroupCollapsedSelectedStyle ??= Skin.GetStyle("Hierarchy Group Collapsed Selected");
        internal static GUIStyle HierarchyHighlight1 => _hierarchyHighlightStyles[0] ??= Skin.GetStyle("Hierarchy Highlight 1");
        internal static GUIStyle HierarchyHighlight2 => _hierarchyHighlightStyles[1] ??= Skin.GetStyle("Hierarchy Highlight 2");
        internal static GUIStyle HierarchyHighlight3 => _hierarchyHighlightStyles[2] ??= Skin.GetStyle("Hierarchy Highlight 3");
        internal static GUIStyle HierarchyHighlight4 => _hierarchyHighlightStyles[3] ??= Skin.GetStyle("Hierarchy Highlight 4");
        internal static GUIStyle HierarchyHighlight5 => _hierarchyHighlightStyles[4] ??= Skin.GetStyle("Hierarchy Highlight 5");
        internal static GUIStyle HierarchyGroupDisabled => _hierarchyGroupDisabledStyle ??= Skin.GetStyle("Hierarchy Group Disabled");

        internal static GUIStyle HierarchyTreeEntry => _hierarchyTreeEntryStyle ??= Skin.GetStyle("Tree Entry");
        internal static GUIStyle HierarchyTreeEntryParallel => _hierarchyTreeEntryParallelStyle ??= Skin.GetStyle("Tree Entry Parallel");
        internal static GUIStyle HierarchyTreeEntryEndpoint => _hierarchyTreeEntryEndpointStyle ??= Skin.GetStyle("Tree Entry Endpoint");
        internal static GUIStyle HierarchyTreeEntryParallelEndpoint => _hierarchyTreeEntryParallelEndpointStyle ??= Skin.GetStyle("Tree Entry Parallel Endpoint");

        internal static GUIStyle FilesDetailText => _filesDetailTextStyle ??= Skin.GetStyle("Files Details");
        internal static GUIStyle FilesDetailTextSelected => _filesDetailTextSelectedStyle ??= Skin.GetStyle("Files Details Selected");

        internal static GUIStyle Toggle => Skin.toggle;
        internal static GUIStyle ToggleDisabled => _toggleDisabled ??= Skin.GetStyle("Toggle Disabled");
        internal static GUIStyle FoldoutLeft => _foldoutLeftStyle ??= Skin.GetStyle("Foldout Left");
        internal static GUIStyle FoldoutRight => _foldoutRightStyle ??= Skin.GetStyle("Foldout Right");
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}