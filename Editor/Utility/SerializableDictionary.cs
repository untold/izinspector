﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Izinspector.Editors.Utility {
    [System.Serializable]
    internal class SerializableDictionary<T> {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        [field: SerializeField] private List<KeyValuePair<T>> Pairs { get; set; } = new();

        public T this[string key] {
            get {
                var index = Pairs.FindIndex(p => p.Key.Equals(key));
                if (index >= 0)
                    return Pairs[index].Value;

                Debug.LogWarning("<b>Key not found</b> exception\n");
                return default;
            }
            set {
                var index = Pairs.FindIndex(p => p.Key != null && p.Key.Equals(key));
                if (index >= 0) {
                    Pairs[index].Value = value;
                    return;
                }

                Pairs.Add(new KeyValuePair<T>(key, value));
            }
        }

        public int Count => Pairs.Count;

        public IReadOnlyList<KeyValuePair<T>> AllPairs => Pairs;
        public IReadOnlyList<string> Keys => Pairs.Select(p => p.Key).ToList();
        public IReadOnlyList<T> Values => Pairs.Select(p => p.Value).ToList();
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        public bool ContainsKey(string key) => Pairs.FindIndex(p => p.Key != null && p.Key.Equals(key)) >= 0;
        public void AddDefinition(string key, T value) => Pairs.Add(new KeyValuePair<T>(key, value));

        public bool TryGetValue(string key, out T value) {
            var index = Pairs.FindIndex(p => p.Key.Equals(key));

            if (index >= 0) {
                value = Pairs[index].Value;
                return true;
            }

            value = default;
            return false;
        }

        public void ForEach(Action<T> callback) => Pairs.ForEach(p => callback(p.Value));

        public int RemoveWhere(Func<T, bool> match) => Pairs.RemoveAll(p => match(p.Value));
        public void Clear() => Pairs.Clear();
        #endregion

        #region Private Methods
        #endregion
    }

    [System.Serializable]
    internal class KeyValuePair<T> {
        [field: SerializeField] internal string Key { get; private set; }
        [field: SerializeField] internal T Value { get; set; }

        public KeyValuePair(string key, T value) {
            Key = key;
            Value = value;
        }
    }
}