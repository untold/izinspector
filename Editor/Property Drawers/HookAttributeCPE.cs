﻿using System.Reflection;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(HookAttribute))]
    internal sealed class HookAttributeCPE : PropertyDrawer {
        #region Constants
        private const BindingFlags k_bindingFlags = BindingFlags.Default | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy | BindingFlags.InvokeMethod;
        #endregion

        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        private MethodInfo _methodInfo = null;
        private HookAttribute _hookAttribute = null;
        #endregion

        #region Constructors
        #endregion

        #region Overrides
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            _hookAttribute ??= (HookAttribute)attribute;
            var classType = property.serializedObject.targetObject.GetType();
            _methodInfo ??= classType.GetMethod(_hookAttribute.MethodName, k_bindingFlags);

            label.tooltip += $"{(string.IsNullOrWhiteSpace(label.tooltip) ? "" : "\n")}Invokes {_methodInfo?.Name}()";

            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(position, property, label);
            if (EditorGUI.EndChangeCheck() && ((_hookAttribute.RuntimeOnly && EditorApplication.isPlaying) || !_hookAttribute.RuntimeOnly))
                _methodInfo?.Invoke(property.serializedObject.targetObject, null);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}