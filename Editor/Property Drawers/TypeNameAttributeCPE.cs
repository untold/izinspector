﻿using System;
using System.Collections.Generic;
using System.Linq;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(TypeNameAttribute))]
    internal sealed class TypeNameAttributeCPE : PropertyDrawer {
        #region Private Variables
        private List<string> _typeNames;
        #endregion

        #region Overrides
        public TypeNameAttributeCPE() {
            _typeNames = null;
        }
        ~TypeNameAttributeCPE() => _typeNames = null;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (property.propertyType != SerializedPropertyType.String) {
                EditorGUI.HelpBox(position, $"{nameof(TypeNameAttribute)} can only be used on string fields", MessageType.Warning);
                return;
            }

            if (_typeNames == null)
                GetTypes();

            var pathName = property.stringValue.Replace(".", "/");
            var currentIndex = _typeNames.FindIndex(tn => pathName == tn);
            EditorGUI.BeginChangeCheck();
            var newIndex = EditorGUI.Popup(position, label.text, currentIndex, _typeNames.ToArray());
            if (EditorGUI.EndChangeCheck())
                property.stringValue = _typeNames[newIndex].Replace("/", ".");
        }
        #endregion

        #region Private Methods
        private void GetTypes() {
            _typeNames = new();

            var typeNameAttribute = (attribute as TypeNameAttribute)!;
            var qualified = typeNameAttribute.UseQualifiedName;
            var baseType = typeNameAttribute.BaseType;
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies) {
                var types = assembly.DefinedTypes.Where(t => t.IsSubclassOf(baseType));
                if (types.Any())
                    _typeNames.AddRange(types.Select(t => (qualified ? t.AssemblyQualifiedName : t.FullName)?.Replace(".", "/")));
            }
        }
        #endregion
    }
}