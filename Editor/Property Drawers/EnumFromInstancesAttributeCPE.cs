﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(EnumFromInstancesAttribute))]
    internal sealed class EnumFromInstancesAttributeCPE : PropertyDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        private Type _type;
        private List<string> _paths;
        private GUIContent[] _labels;
        #endregion

        #region Properties
        #endregion

        #region Overrides
        public EnumFromInstancesAttributeCPE() : base() {
            _paths = null;
            _labels = null;
        }
        ~EnumFromInstancesAttributeCPE() => _paths = null;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (property.propertyType != SerializedPropertyType.ObjectReference) {
                EditorGUI.HelpBox(position, $"Type {property.propertyType} cannot be displayed as enum", MessageType.Warning);
                return;
            }

            if (_paths == null)
                GetOptions();

            var currentIndex = 0;

            var hasValue = property.objectReferenceValue;

            if (hasValue) {
                for (var i = 0; i < _labels.Length; i++) {
                    if (!hasValue.name.Equals(_labels[i].text))
                        continue;

                    currentIndex = i;
                    break;
                }
            }

            var fieldRect = new Rect(position) {
                width = position.width - (hasValue ? 24 : 0)
            };

            label.tooltip = _type.Name;
            EditorGUI.BeginChangeCheck();
            var newIndex = EditorGUI.Popup(fieldRect, label, currentIndex, _labels);
            if (EditorGUI.EndChangeCheck() && currentIndex != newIndex) {
                var path = _paths[newIndex];
                if (path == null)
                    property.objectReferenceValue = null;
                else
                    property.objectReferenceValue = AssetDatabase.LoadMainAssetAtPath(path);
            }

            if (!hasValue)
                return;

            var btnLeftRect = new Rect(position) {
                x = fieldRect.xMax + 2,
                width = 22
            };

            if (GUI.Button(btnLeftRect, EditorGUIUtility.IconContent("d_Settings"), EditorStyles.miniButtonLeft)) {
                var menu = new GenericMenu();

                menu.AddItem(new GUIContent("Show", EditorGUIUtility.IconContent("d_scenevis_visible_hover").image), false, () => EditorGUIUtility.PingObject(hasValue));
                menu.AddItem(new GUIContent("Select", EditorGUIUtility.IconContent("d_Grid.Default").image), false, () => Selection.SetActiveObjectWithContext(hasValue, null));
                menu.AddItem(new GUIContent("Open", EditorGUIUtility.IconContent("d_editicon.sml").image), false, () => AssetDatabase.OpenAsset(hasValue));

                menu.DropDown(btnLeftRect);
            }
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private void GetOptions() {
            // _type = fieldInfo.FieldType.IsArray ? fieldInfo.FieldType.GetElementType() : fieldInfo.FieldType;
            _type = fieldInfo.FieldType.IsArray
                ? fieldInfo.FieldType.GetElementType() :
                typeof(IEnumerable).IsAssignableFrom(fieldInfo.FieldType)
                    ? fieldInfo.FieldType.GetGenericArguments()[0]
                    : fieldInfo.FieldType;
            _paths = new List<string>();
            _labels = new GUIContent[0];

            _paths.Add(null);
            _labels = new[] { new GUIContent("~None~") };

            var guids = AssetDatabase.FindAssets($"t:{_type.Name}");
            foreach (var guid in guids) {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var fileName = Path.GetFileNameWithoutExtension(path);
                _paths.Add(path);
                _labels = _labels.Append(new GUIContent(fileName)).ToArray();
            }
        }
        #endregion
    }
}