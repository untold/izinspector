﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(EnumOptionsAttribute))]
    internal sealed class EnumOptionsAttributeCPE : PropertyDrawer {
        #region Private Variables
        private List<string> _options;
        #endregion

        #region Overrides
        public EnumOptionsAttributeCPE() => _options = null;
        ~EnumOptionsAttributeCPE() => _options = null;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var attr = fieldInfo.GetCustomAttribute<EnumOptionsAttribute>();
            _options ??= attr.Options.ToList();
            var currentIndex = -1;

            var displayName = attr.DisplayName ?? label.text;

            switch (property.propertyType) {
                case SerializedPropertyType.String:
                    currentIndex = _options.FindIndex(o => o.Equals(property.stringValue));
                    EditorGUI.BeginChangeCheck();
                    var newIndex = EditorGUI.Popup(position, displayName, currentIndex, _options.ToArray());
                    if (EditorGUI.EndChangeCheck())
                        property.stringValue = _options[newIndex];
                    break;
                case SerializedPropertyType.Integer:
                    currentIndex = property.intValue;
                    EditorGUI.BeginChangeCheck();
                    newIndex = EditorGUI.Popup(position, displayName, currentIndex, _options.ToArray());
                    if (EditorGUI.EndChangeCheck())
                        property.intValue = newIndex;
                    break;
                case SerializedPropertyType.Boolean:
                    currentIndex = property.boolValue ? 1 : 0;
                    EditorGUI.BeginChangeCheck();
                    newIndex = EditorGUI.Popup(position, displayName, currentIndex, _options.ToArray());
                    if (EditorGUI.EndChangeCheck())
                        property.boolValue = newIndex != 0;
                    break;
                default:
                    EditorGUI.HelpBox(position, $"Type {property.propertyType} cannot be displayed as enum", MessageType.Warning);
                    break;
            }
        }
        #endregion
    }
}