﻿using System.Reflection;
using Izinspector.Editors.Utility;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(SolidHeaderAttribute))]
    internal sealed class SolidHeaderAttributeCPE : PropertyDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Overrides
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var header = fieldInfo.GetCustomAttribute<SolidHeaderAttribute>().Label;

            var headerRect = new Rect(position) {
                height = EditorGUIUtility.singleLineHeight + 1,
                x = position.x - 18,
                xMax = position.xMax + 5
            };
            GUI.Label(headerRect, string.Empty, Styles.HierarchyGroup);
            var headerLabelRect = new Rect(position) {
                height = EditorGUIUtility.singleLineHeight - 1
            };
            GUI.Label(headerLabelRect, header, EditorStyles.boldLabel);

            position.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            position.height -= EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.PropertyField(position, property, label);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) { return base.GetPropertyHeight(property, label) + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing; }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}