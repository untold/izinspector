using Izinspector.Runtime.Types;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(SceneReference))]
    internal sealed class SceneReferenceCPE : PropertyDrawer {
        #region Overrides
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var sceneAssetProp = property.FindPropertyRelative("d_sceneAsset");
            var sceneNameProp = property.FindPropertyRelative("m_sceneName");
            var scenePathProp = property.FindPropertyRelative("m_scenePath");

            label.tooltip = sceneNameProp.stringValue;
            position.y += EditorGUIUtility.standardVerticalSpacing * .5f;
            position.height = EditorGUIUtility.singleLineHeight;

            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(position, sceneAssetProp, label);
            if (!EditorGUI.EndChangeCheck())
                return;

            scenePathProp.stringValue = AssetDatabase.GetAssetPath(sceneAssetProp.objectReferenceValue);
            sceneNameProp.stringValue = System.IO.Path.GetFileNameWithoutExtension(scenePathProp.stringValue);
        }
        #endregion
    }
}