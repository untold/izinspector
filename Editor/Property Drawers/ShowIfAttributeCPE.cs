﻿using System.Reflection;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(ShowIfAttribute))]
    internal sealed class ShowIfAttributeCPE : PropertyDrawer {
        #region Constants
        private const BindingFlags k_bindingFlags = BindingFlags.Default | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy | BindingFlags.GetField | BindingFlags.GetProperty | BindingFlags.InvokeMethod;
        #endregion

        #region Properties
        private FieldInfo _fieldInfo = null;
        private ShowIfAttribute _showIfAttribute = null;

        private bool? _show;
        #endregion

        #region Overrides
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            _show = CheckShow(property);

            if (!_show.HasValue)
                return EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

            return _show.Value ? EditorGUI.GetPropertyHeight(property, label, true) : -EditorGUIUtility.standardVerticalSpacing;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (!_show.HasValue)
                EditorGUI.HelpBox(position, "ShowIf attribute can only reference bool fields", MessageType.Error);
            else if (_show.Value)
                EditorGUI.PropertyField(position, property, label);
        }
        #endregion

        #region Private Methods
        private bool? CheckShow(SerializedProperty property) {
            _showIfAttribute ??= (ShowIfAttribute)attribute;
            var classType = property.serializedObject.targetObject.GetType();
            _fieldInfo ??= classType.GetField(_showIfAttribute.MethodName, k_bindingFlags);

            if (_fieldInfo?.FieldType != typeof(bool))
                return null;

            return (bool)_fieldInfo!.GetValue(property.serializedObject.targetObject) ^ _showIfAttribute.Invert;
        }
        #endregion
    }
}