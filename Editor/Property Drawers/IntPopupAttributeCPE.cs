﻿using System.Linq;
using System.Reflection;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(IntPopupAttribute))]
    internal sealed class IntPopupAttributeCPE : PropertyDrawer {
        #region Private Variables
        private int[] _options;
        private GUIContent[] _labels;
        #endregion

        #region Overrides
        public IntPopupAttributeCPE() {
            _options = null;
            _labels = null;
        }

        ~IntPopupAttributeCPE() {
            _options = null;
            _labels = null;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var attr = fieldInfo.GetCustomAttribute<IntPopupAttribute>();
            _options ??= attr.Values.ToArray();
            _labels ??= attr.Values.Select(v => new GUIContent(v.ToString())).ToArray();

            switch (property.propertyType) {
                case SerializedPropertyType.Integer:
                    var currentValue = property.intValue;
                    EditorGUI.BeginChangeCheck();
                    var newValue = EditorGUI.IntPopup(position, label, currentValue, _labels, _options);
                    if (EditorGUI.EndChangeCheck())
                        property.intValue = newValue;
                    break;
                default:
                    EditorGUI.HelpBox(position, $"Type {property.propertyType} cannot be displayed as enum", MessageType.Warning);
                    break;
            }
        }
        #endregion
    }
}