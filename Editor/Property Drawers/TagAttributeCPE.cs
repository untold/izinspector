using UnityEngine;
using UnityEditor;
using Izinspector.Runtime.PropertyAttributes;
using System.Linq;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(TagAttribute))]
    public class TagAttributeCPE : PropertyDrawer {
        #region Public Functions
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            return EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if(fieldInfo.FieldType != typeof(string) && fieldInfo.FieldType != typeof(string[])) {
                EditorGUI.HelpBox(position, "Only string type can use the Tag attribute", MessageType.Error);
                return;
            }

            var tags = UnityEditorInternal.InternalEditorUtility.tags;
            var currentTagIndex = tags.ToList().IndexOf(property.stringValue);

            var newIndex = EditorGUI.Popup(position, label.text, currentTagIndex, tags.Select(t => t).ToArray());

            if (newIndex == currentTagIndex)
                return;

            property.stringValue = tags[newIndex];
            property.serializedObject.ApplyModifiedProperties();
        }
        #endregion
    }
}