﻿using System.Collections.Generic;
using System.Linq;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(LayerAttribute))]
    internal sealed class LayerAttributeCPE : PropertyDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        private static string[] _layers;
        private static int[] _layersIds;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        public LayerAttributeCPE() {
            if (_layers != null)
                return;

            var layers = GetAllLayers();
            _layers = layers.Keys.ToArray();
            _layersIds = layers.Values.ToArray();
        }
        #endregion

        #region Overrides
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            EditorGUI.BeginProperty(position, label, property);
            property.intValue = EditorGUI.IntPopup(position, label.text, property.intValue, _layers, _layersIds);
            EditorGUI.EndProperty();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private static Dictionary<string, int> GetAllLayers() {
            var layers = new Dictionary<string, int>();

            for (var i = 0; i < 32; i++) {
                var layerName = LayerMask.LayerToName(i);

                if (string.IsNullOrEmpty(layerName))
                    continue;

                layers.Add(layerName, i);
            }

            return layers;
        }
        #endregion
    }
}