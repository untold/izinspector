﻿using Izinspector.Editors.Utility;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(LockableAttribute))]
    internal sealed class LockableAttributeCPE : PropertyDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Overrides
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            GUI.enabled = property.isExpanded;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;

            var iconRect = new Rect(position) {
                x = position.x - 18,
                width = 18,
                height = EditorGUIUtility.singleLineHeight
            };
            var originalColor = GUI.color;
            if (property.isExpanded)
                GUI.color *= .5f;
            if (GUI.Button(iconRect, Icons.LockIcon, EditorStyles.label))
                property.isExpanded = !property.isExpanded;
            GUI.color = originalColor;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}