using System.Reflection;
using Izinspector.Runtime.PropertyAttributes;
using UnityEngine;
using UnityEditor;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(SliderTicked))]
    public class SliderTickedCPE : PropertyDrawer {
        #region Constants
        private const float INDENT = 5f;
        #endregion

        #region Public Variables
        #endregion

        #region Private Variables
        private SliderTicked _target;
        #endregion

        #region Private Methods
        #endregion

        #region Public Functions
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            return EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            _target ??= fieldInfo.GetCustomAttribute(typeof(SliderTicked), false) as SliderTicked;

            // Handles.DrawSolidRectangleWithOutline(position, Color.clear, Color.magenta);
            var rect = new Rect(position) {
                x = (EditorGUIUtility.labelWidth) + 20 + INDENT,
                width = position.width - EditorGUIUtility.labelWidth - 7 - EditorGUIUtility.fieldWidth - INDENT * 2
            };

            // Handles.DrawSolidRectangleWithOutline(rect, Color.clear, Color.yellow);

            EditorGUI.BeginProperty(position, label, property);
            {
                EditorGUI.BeginChangeCheck();
                property.floatValue = EditorGUI.Slider(position, label, property.floatValue, _target.Min, _target.Max);
                if (EditorGUI.EndChangeCheck()) {
                    foreach (var tick in _target.Ticks) {
                        if(Mathf.Abs(property.floatValue - tick) > _target.SnapThreshold)
                            continue;

                        property.floatValue = tick;
                        break;
                    }

                    EditorUtility.SetDirty(property.serializedObject.targetObject);
                }
            }
            EditorGUI.EndProperty();

            for (var i = 0; i < _target.Ticks.Length; i++) {
                var r = new Rect(rect) {
                    width = 0.5f,
                    // height = rect.height * 0.4f,
                    height = rect.height * 0.12f,
                    x = rect.x + rect.width * (_target.Ticks[i] / (_target.Max - _target.Min)),
                    // y = rect.y + rect.height * 0.3f
                    y = rect.y + rect.height * (0.5f - 0.06f)
                };
                Handles.DrawSolidRectangleWithOutline(r, Color.white, Color.white * 0.8f);
            }
        }
        #endregion
    }
}