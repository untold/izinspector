﻿using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(PlaceholderAttribute))]
    internal sealed class PlaceholderAttributeCPE : PropertyDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        private GUIStyle _placeholderStyle;
        #endregion

        #region Properties
        #endregion

        #region Overrides
        public PlaceholderAttributeCPE() {
            _placeholderStyle = new GUIStyle(EditorStyles.label) {
                alignment = TextAnchor.MiddleLeft,
                normal = new GUIStyleState() { textColor = new Color(1f, 1f, 1f, 0.25f) }
            };
        }

        ~PlaceholderAttributeCPE() => _placeholderStyle = null;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (property.propertyType != SerializedPropertyType.String) {
                EditorGUI.HelpBox(position, $"Type {property.propertyType} does not support placeholders", MessageType.Warning);
                return;
            }

            EditorGUI.PropertyField(position, property, label, true);

            if (!string.IsNullOrEmpty(property.stringValue))
                return;

            var placeholderRect = new Rect(position) { x = position.x + EditorGUIUtility.labelWidth + 4, height = EditorGUIUtility.singleLineHeight };
            EditorGUI.LabelField(placeholderRect, (attribute as PlaceholderAttribute)!.Label, _placeholderStyle);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}