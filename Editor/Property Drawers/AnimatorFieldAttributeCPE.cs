﻿using System.Linq;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(AnimatorFieldAttribute))]
    public class AnimatorFieldAttributeCPE : PropertyDrawer {
        #region Private Variables
        private static bool _usesHash;
        #endregion

        #region Public Functions
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            return EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if(!IsValidType(fieldInfo.FieldType)) {
                EditorGUI.HelpBox(position, "Only string or int types can use the AnimatorField attribute", MessageType.Error);
                return;
            }

            var anim = ((MonoBehaviour)property.serializedObject.targetObject).GetComponentInParent<Animator>();
            if (!anim) {
                EditorGUI.HelpBox(position, "There is no animator attached to this GameObject not its parents", MessageType.Warning);
                return;
            }

            if (!anim.runtimeAnimatorController) {
                EditorGUI.HelpBox(position, "The animator has no RuntimeAnimatorController set", MessageType.Warning);
                return;
            }

            var filter = ((AnimatorFieldAttribute)attribute).Type;

            if (_usesHash)
                DrawForHashes(position, property, label, anim, filter);
            else
                DrawForStrings(position, property, label, anim, filter);

            property.serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Private Methods
        private static bool IsValidType(System.Type type) {
            _usesHash = false;

            if (type == typeof(string) || type == typeof(string[]))
                return true;

            if (type != typeof(int) && type != typeof(int[]))
                return false;

            _usesHash = true;
            return true;
        }

        private void DrawForStrings(Rect position, SerializedProperty property, GUIContent label, in Animator anim, AnimatorControllerParameterType? fieldType) {
            var parameters = fieldType != null ? anim.parameters.Where(p => p.type == fieldType) : anim.parameters;
            var paramList = parameters.Select(p => p.name).ToList();
            var currentFieldIndex = paramList.IndexOf(property.stringValue);

            var newIndex = EditorGUI.Popup(position, label.text, currentFieldIndex, paramList.ToArray());

            if (newIndex == currentFieldIndex)
                return;

            property.stringValue = paramList[newIndex];
        }

        private void DrawForHashes(Rect position, SerializedProperty property, GUIContent label, in Animator anim, AnimatorControllerParameterType? fieldType) {
            var parameters = fieldType != null ? anim.parameters.Where(p => p.type == fieldType).ToArray() : anim.parameters;
            var paramHashList = parameters.Select(p => p.nameHash).ToList();
            var currentFieldIndex = paramHashList.IndexOf(property.intValue);

            var newIndex = EditorGUI.Popup(position, label.text, currentFieldIndex, parameters.Select(t => t.name).ToArray());

            if (newIndex == currentFieldIndex)
                return;

            property.intValue = paramHashList[newIndex];
        }
        #endregion
    }
}