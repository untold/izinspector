﻿using Izinspector.Editors.Utility;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(DebugFieldAttribute))]
    internal sealed class DebugFieldAttributeCPE : DecoratorDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Overrides
        public override float GetHeight() => 0f;

        public override void OnGUI(Rect position) {
            var rect = new Rect(position) {
                x = position.x - 18,
                y = position.yMax,
                width = 18,
                height = EditorGUIUtility.singleLineHeight
            };

            GUI.Label(rect, Icons.DebugOn);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}