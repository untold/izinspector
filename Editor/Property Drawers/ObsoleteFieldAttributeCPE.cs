using Izinspector.Editors.Utility;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(ObsoleteFieldAttribute))]
    internal sealed class ObsoleteFieldAttributeCPE : DecoratorDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Overrides
        public override float GetHeight() => 0f;

        public override void OnGUI(Rect position) {
            var rect = new Rect(position) {
                x = position.x - 18,
                y = position.yMax,
                width = 3,
                height = EditorGUIUtility.singleLineHeight
            };

            Handles.DrawSolidRectangleWithOutline(rect, Colors.UnityGrey, Color.clear);

            rect.width = 18;
            GUI.Label(rect, new GUIContent(string.Empty, "Obsolete"));
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}