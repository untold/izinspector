using System;
using Izinspector.Editors.Tools.TypeSearcher;
using Izinspector.Runtime.Types;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(TypeReference<>))]
    public sealed class TypeReferenceCPE : PropertyDrawer {
        #region Public Functions
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => EditorGUIUtility.singleLineHeight;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var asmQualifiedNameProp = property.FindPropertyRelative("m_assemblyQualifiedName");
            var typeNameProp = property.FindPropertyRelative("m_typeName");

            label.text = typeNameProp.stringValue;
            label.tooltip = asmQualifiedNameProp.stringValue;
            label.image = EditorGUIUtility.IconContent("Assembly Icon").image;

            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();
            var fieldRect = EditorGUI.PrefixLabel(position, new("Type"));
            if (EditorGUI.DropdownButton(fieldRect, label, FocusType.Passive))
                BringUpSearchWindow(fieldRect, property);

            EditorGUI.EndProperty();
        }

        private void BringUpSearchWindow(Rect position, SerializedProperty property) {
            var origin = GUIUtility.GUIToScreenPoint(new(position.center.x, position.yMax + position.height - 4));

            var type = fieldInfo.FieldType.GenericTypeArguments[0];
            var ctx = new SearchWindowContext(origin, Mathf.Max(position.width, 140f));

            var entriesProvider = ScriptableObject.CreateInstance<TypeSearchProviderSO>();
            entriesProvider.InitializeType(type);
            entriesProvider.LinkRequest(t => SetType(property, t));

            SearchWindow.Open(ctx, entriesProvider);
            Object.DestroyImmediate(entriesProvider);
        }
        #endregion

        #region Event Methods
        private void SetType(SerializedProperty property, Type type) {
            property.serializedObject.Update();

            var asmQualifiedNameProp = property.FindPropertyRelative("m_assemblyQualifiedName");
            var typeNameSpaceProp = property.FindPropertyRelative("m_typeNameSpace");
            var typeNameProp = property.FindPropertyRelative("m_typeName");

            asmQualifiedNameProp.stringValue = type.AssemblyQualifiedName;
            typeNameSpaceProp.stringValue = type.Namespace;
            typeNameProp.stringValue = type.Name;

            property.serializedObject.ApplyModifiedProperties();
        }
        #endregion
    }
}