﻿using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(RuntimeFieldAttribute))]
    internal sealed class RuntimeFieldAttributeCPE : PropertyDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Overrides
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => EditorApplication.isPlaying ? base.GetPropertyHeight(property, label) : 0f;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (!EditorApplication.isPlaying)
                return;

            EditorGUI.PropertyField(position, property, label);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}