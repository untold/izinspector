using System.Collections.Generic;
using System.Linq;
using Izinspector.Runtime.PropertyAttributes;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

namespace Izinspector.Editors.PropertyDrawers {
    [CustomPropertyDrawer(typeof(NavMeshMaskAttribute))]
    internal sealed class NavMeshMaskAttributeCPE : PropertyDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        private static string[] _areaNames;
        private static int[] _areas;

        private static bool _didSetUp;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        public NavMeshMaskAttributeCPE() {
            if (_areaNames != null)
                return;

            _areaNames = NavMesh.GetAreaNames();
            _areas = _areaNames.Select(NavMesh.GetAreaFromName).ToArray();

            _didSetUp = false;
        }
        #endregion

        #region Overrides
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var attr = (NavMeshMaskAttribute)attribute;

            if (!_didSetUp && !attr.Multiple) {
                _didSetUp = true;
                _areaNames = _areaNames.Prepend("All").ToArray();
                _areas = _areas.Prepend(-1).ToArray();
            }

            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();
            var num = property.intValue;

            if (attr.Multiple)
                num = EditorGUI.MaskField(position, label.text, num, _areaNames);
            else {
                var navMeshArea = property.intValue;
                var selectedIndex = -1;
                for (var i = 0; i < _areaNames.Length; i++) {
                    if (_areas[i] != navMeshArea)
                        continue;

                    selectedIndex = i;
                    break;
                }

                num = EditorGUI.Popup(position, label.text, selectedIndex, _areaNames);
            }

            if (EditorGUI.EndChangeCheck())
                property.intValue = attr.Multiple ? num : _areas[num];

            EditorGUI.EndProperty();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private static Dictionary<string, int> GetAllLayers() {
            var layers = new Dictionary<string, int>();

            for (var i = 0; i < 32; i++) {
                var layerName = LayerMask.LayerToName(i);

                if (string.IsNullOrEmpty(layerName))
                    continue;

                layers.Add(layerName, i);
            }

            return layers;
        }
        #endregion
    }
}