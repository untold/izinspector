﻿using System;
using System.Linq;
using System.Reflection;
using Izinspector.Editors.Preferences;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Izinspector.Editors.Overlays {
    [InitializeOnLoad]
    internal sealed class HierarchyOverlays {
        #region Constants
        private const BindingFlags k_flagsField = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
        private const BindingFlags k_flagsInvoke = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.InvokeMethod;
        private const BindingFlags k_flagsGetter = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.GetProperty;
        #endregion

        #region Public Variables
        #endregion

        #region Private Variables
        private static EditorWindow _hierarchyWindow;

        private static Assembly _assembly;
        private static Type _hierarchyWindowType;
        private static Type _sceneHierarchyType;
        private static Type _treeViewType;
        private static Type _treeViewDataType;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        static HierarchyOverlays() {
            EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyElementDraw;
        }

        ~HierarchyOverlays() {
            EditorApplication.hierarchyWindowItemOnGUI -= OnHierarchyElementDraw;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private static void OnHierarchyElementDraw(int instanceId, Rect selectionRect) {
            var obj = EditorUtility.InstanceIDToObject(instanceId);

            if (!obj)
                return;

            var isSelected = Selection.Contains(obj);
            var isActive = (obj as GameObject)!.activeInHierarchy;

            if (obj.name.StartsWith("---") && IzinspectorPrefs.ShowHeaderOverlay) {
                var groupRect = new Rect(selectionRect) {
                    x = selectionRect.x - 28,
                    width = selectionRect.width + 22 + 24
                };
                var isExpanded = IsExpanded(obj);
                var style = isSelected
                    ? (isExpanded ? Styles.HierarchyGroupSelected : Styles.HierarchyGroupCollapsedSelected)
                    : (isActive ? (isExpanded ? Styles.HierarchyGroup : Styles.HierarchyGroupCollapsed) : Styles.HierarchyGroupDisabled);
                var label = new GUIContent(obj.name[3..].ToUpperInvariant(), obj.name);
                EditorGUI.LabelField(groupRect, label, style);

                var siz = style.CalcSize(label);
                var leftArrowRect = new Rect(groupRect) {
                    x = groupRect.center.x - siz.x * .5f - 18,
                    y = groupRect.y + 1,
                    width = 14,
                    height = 14
                };

                EditorGUI.BeginChangeCheck();
                EditorGUI.Toggle(leftArrowRect, isExpanded, Styles.FoldoutLeft);
                if (EditorGUI.EndChangeCheck())
                    ToggleObjectExpanded(obj);

                var rightArrowRect = new Rect(groupRect) {
                    x = groupRect.center.x + siz.x * .5f + 4,
                    y = groupRect.y + 1,
                    width = 14,
                    height = 14
                };

                EditorGUI.BeginChangeCheck();
                EditorGUI.Toggle(rightArrowRect, isExpanded, Styles.FoldoutRight);
                if (EditorGUI.EndChangeCheck())
                    ToggleObjectExpanded(obj);

                var e = Event.current;
                if (e.type == EventType.MouseDown && e.button == 2 && groupRect.Contains(e.mousePosition))
                    ToggleObjectExpanded(obj);

                DrawObjectStateToggle(obj, selectionRect);

                return;
            }

            if (IzinspectorPrefs.ShowLabelOverlay) {
                var start = obj.name[0];
                var labelRect = new Rect(selectionRect) {
                    // width = Mathf.Max(selectionRect.width, 120)
                    width = 220
                };

                var style = start switch {
                    '#' => Styles.HierarchyHighlight1,
                    '@' => Styles.HierarchyHighlight2,
                    '!' => Styles.HierarchyHighlight3,
                    '$' => Styles.HierarchyHighlight4,
                    '>' => Styles.HierarchyHighlight5,
                    _ => null
                };
                if (style != null)
                    GUI.Box(labelRect, GUIContent.none, style);
            }

            var go = obj as GameObject;
            if (go == null)
                return;

            DrawObjectStateToggle(obj, selectionRect);
            // if (selectionRect.width >= 80 && IzinspectorPrefs.ShowObjectStateOverlay) {
            //     var activeToggleRect = new Rect(selectionRect) {
            //         x = selectionRect.xMax - 2 - 12,
            //         width = 32
            //     };
            //     EditorGUI.BeginChangeCheck();
            //     EditorGUI.Toggle(activeToggleRect, go.activeSelf, Styles.Toggle);
            //     if (EditorGUI.EndChangeCheck()) {
            //         Undo.RecordObject(go, $"Toggle {go.name}");
            //         go.SetActive(!go.activeSelf);
            //         // EditorUtility.SetDirty(go);
            //     }
            // }

            if (IzinspectorPrefs.ShowTreeEntryDepthOverlay) {
                try {
                    var depth = Depth(obj);
                    if (depth > 1) {
                        for (var i = depth - 1; i > 0; i--) {
                            if (go.transform.childCount > 0 && i == 1)
                                break;

                            var rect = new Rect(selectionRect) {
                                x = selectionRect.x - 14 * i,
                                width = 14 * i
                            };
                            var isProximateDepth = i == 1;
                            var isLastSibling = go.transform.GetSiblingIndex() == go.transform.parent.childCount - 1;
                            var siblingStyle = isLastSibling ? Styles.HierarchyTreeEntryEndpoint : Styles.HierarchyTreeEntry;

                            var isParentLastSiblingRecursive = false;
                            var parent = go.transform.parent;
                            int safeCounter = 0;
                            while (parent != null) {
                                var grandParent = parent.parent;
                                isParentLastSiblingRecursive = grandParent && parent.GetSiblingIndex() == grandParent.childCount - 1;
                                parent = grandParent;
                                safeCounter++;
                                if (safeCounter > 5)
                                    break;
                            }

                            var parallelSiblingStyle = isLastSibling && isParentLastSiblingRecursive ? Styles.HierarchyTreeEntryParallelEndpoint : Styles.HierarchyTreeEntryParallel;
                            GUI.Label(rect, GUIContent.none, isProximateDepth ? siblingStyle : parallelSiblingStyle);
                        }
                    }
                }
                #pragma warning disable
                catch (Exception _) {
                    return;
                }
                #pragma warning restore
            }

            if (!IzinspectorPrefs.ShowIconOverlay)
                return;

            var leftRect = new Rect(selectionRect) {
                x = selectionRect.x - 28,
                width = 16
            };

            GUI.enabled = go.activeSelf;
            if (go.CompareTag("EditorOnly"))
                EditorGUI.LabelField(leftRect, Icons.DebugOn);
            else if (LayerMask.LayerToName(go.layer) == "UI")
                EditorGUI.LabelField(leftRect, Icons.Canvas);
            else if (go.TryGetComponent(out Camera cam))
                EditorGUI.LabelField(leftRect, Icons.Camera);
            else if (go.TryGetComponent(out AudioSource source))
                EditorGUI.LabelField(leftRect, Icons.Audio);
            GUI.enabled = true;
        }

        private static void DrawObjectStateToggle(Object obj, Rect selectionRect) {
            var go = obj as GameObject;
            if (go == null)
                return;

            if (selectionRect.width < 80 || !IzinspectorPrefs.ShowObjectStateOverlay)
                return;

            var activeToggleRect = new Rect(selectionRect) {
                x = selectionRect.xMax - 4 - 12,
                width = 32
            };
            EditorGUI.BeginChangeCheck();
            EditorGUI.Toggle(activeToggleRect, go.activeSelf, go.activeInHierarchy ? Styles.Toggle : Styles.ToggleDisabled);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(go, $"Toggle {go.name}");
                go.SetActive(!go.activeSelf);
                // EditorUtility.SetDirty(go);
            }
        }

        private static void ToggleObjectExpanded(Object obj) {
            // UnityEditor.SceneHierarchyWindow win;
            // var isExpanded = win.GetExpandedIDs().Contains(obj.GetInstanceID());
            // win.m_SceneHierarchy.ExpandTreeViewItem(obj.GetInstanceID(), !isExpanded);
            // var row = win.m_SceneHierarchy.m_TreeView.data.GetRow(obj.GetInstanceID());
            // var depth = win.m_SceneHierarchy.m_TreeView.data.GetItem(row).depth;

            var window = EditorWindow.focusedWindow;
            _assembly ??= typeof(EditorWindow).Assembly;
            _hierarchyWindowType ??= _assembly.GetType("UnityEditor.SceneHierarchyWindow");
            _sceneHierarchyType ??= _assembly.GetType("UnityEditor.SceneHierarchy");

            var expandedIDs = (int[])_hierarchyWindowType.InvokeMember("GetExpandedIDs", k_flagsInvoke, null, window, null);
            var isExpanded = expandedIDs.Contains(obj.GetInstanceID());
            var sceneHierarchy = _hierarchyWindowType.GetField("m_SceneHierarchy", k_flagsField)!.GetValue(window);

            _sceneHierarchyType.InvokeMember("ExpandTreeViewItem", k_flagsInvoke, null, sceneHierarchy, new object[] { obj.GetInstanceID(), !isExpanded });
        }

        private static bool IsExpanded(Object obj) {
            _assembly ??= typeof(EditorWindow).Assembly;
            _hierarchyWindowType ??= _assembly.GetType("UnityEditor.SceneHierarchyWindow");
            _hierarchyWindow ??= EditorWindow.GetWindow(_hierarchyWindowType);

            var expandedIDs = (int[])_hierarchyWindowType.InvokeMember("GetExpandedIDs", k_flagsInvoke, null, _hierarchyWindow, null);
            return expandedIDs.Contains(obj.GetInstanceID());
        }

        private static int Depth(Object obj) {
            _assembly ??= typeof(EditorWindow).Assembly;
            _hierarchyWindowType ??= _assembly.GetType("UnityEditor.SceneHierarchyWindow");
            _sceneHierarchyType ??= _assembly.GetType("UnityEditor.SceneHierarchy");
            _treeViewType = _assembly.GetType("UnityEditor.IMGUI.Controls.TreeViewController");
            _treeViewDataType = _assembly.GetType("UnityEditor.IMGUI.Controls.ITreeViewDataSource");

            _hierarchyWindow ??= EditorWindow.GetWindow(_hierarchyWindowType);

            var sceneHierarchy = _hierarchyWindowType.GetField("m_SceneHierarchy", k_flagsField)!.GetValue(_hierarchyWindow);
            var treeView = _sceneHierarchyType.GetField("m_TreeView", k_flagsField)!.GetValue(sceneHierarchy);
            var treeViewData = _treeViewType.GetProperty("data", k_flagsGetter)?.GetValue(treeView);

            var _getRowMethod = _treeViewDataType.GetMethod("GetRow", k_flagsInvoke, null, new []{ typeof(int) }, null);
            var _getItemMethod = _treeViewDataType.GetMethod("GetItem", k_flagsInvoke, null, new []{ typeof(int) }, null);

            var row = _getRowMethod!.Invoke(treeViewData, new object[] { obj.GetInstanceID() });
            var item = (UnityEditor.IMGUI.Controls.TreeViewItem)_getItemMethod!.Invoke(treeViewData, new object[] { row });

            return item.depth;
        }
        #endregion
    }
}