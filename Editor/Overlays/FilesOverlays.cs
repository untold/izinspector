﻿using System.Collections.Generic;
using System.Linq;
using Izinspector.Editors.Preferences;
using Izinspector.Editors.Utility;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace Izinspector.Editors.Overlays {
    [InitializeOnLoad]
    internal sealed class FilesOverlays {
        #region Public Variables
        #endregion

        #region Private Variables
        private static System.Type _textureUtilType;
        private static System.Reflection.MethodInfo _getStorageMemorySizeMethod;

        private static readonly Dictionary<string, Texture2D> _specialFolders = new() {
            { "Prefab", Icons.Prefab },
            { "Prefabs", Icons.Prefab },
            { "Texture", Icons.Texture },
            { "Textures", Icons.Texture },
            { "Animation", Icons.Animation },
            { "Animations", Icons.Animation },
            { "Network", Icons.Network },
            { "Networking", Icons.Network },
            { "Material", Icons.Material },
            { "Materials", Icons.Material },
            { "Model", Icons.Model },
            { "Models", Icons.Model },
            { "Scenes", Icons.Scene },
            { "Scene", Icons.Scene },
            { "Script", Icons.Script },
            { "Scripts", Icons.Script },
        };
        #endregion

        #region Properties
        #endregion

        #region Constructors
        static FilesOverlays() {
            EditorApplication.projectWindowItemOnGUI += OnHierarchyElementDraw;
        }

        ~FilesOverlays() {
            EditorApplication.projectWindowItemOnGUI -= OnHierarchyElementDraw;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private static void OnHierarchyElementDraw(string guid, Rect selectionRect) {
            var assetPath = AssetDatabase.GUIDToAssetPath(guid);
            if (AssetDatabase.IsValidFolder(assetPath)) {
                DrawDirectoryOverlay(assetPath, selectionRect);
                return;
            }

            var obj = AssetDatabase.LoadAssetAtPath<Object>(assetPath);
            if (!obj)
                return;

            var isSelected = Selection.Contains(obj);
            var path = AssetDatabase.GetAssetPath(obj);

            switch (obj) {
                case SceneAsset scene when IzinspectorPrefs.ShowFileSceneBuildOverlay:
                    DrawSceneOverlay(path, scene, selectionRect);
                    return;
                case Texture tex when selectionRect.height <= EditorGUIUtility.singleLineHeight && IzinspectorPrefs.ShowFileTextureInfoOverlay:
                    DrawTextureOverlay(tex, selectionRect, isSelected);
                    break;
            }
        }

        private static void DrawDirectoryOverlay(string assetPath, Rect selectionRect) {
            var folderName = assetPath.Split('/')[^1];
            if (!_specialFolders.TryGetValue(folderName, out var icon))
                return;

            var overlayRect = selectionRect.height < selectionRect.width
                ? new Rect(selectionRect) {
                    x = selectionRect.xMin + 6,
                    y = selectionRect.y + 6,
                    width = 14,
                    height = 14
                }
                : new Rect(selectionRect) {
                    x = selectionRect.xMin + selectionRect.width * .6f,
                    y = selectionRect.yMin + selectionRect.height * .45f,
                    width = Mathf.Min(selectionRect.width, selectionRect.height) * .4f,
                    height = Mathf.Min(selectionRect.width, selectionRect.height) * .4f
                };
            GUI.Label(overlayRect, icon);
        }

        private static void DrawSceneOverlay(string path, SceneAsset scene, Rect selectionRect) {
            var sceneIsInBuild = EditorBuildSettings.scenes.Any(s => s.path.Equals(path));

            var activeToggleRect = new Rect(selectionRect) {
                x = selectionRect.xMax - 19,
                width = 16,
                height = EditorGUIUtility.singleLineHeight
            };
            EditorGUI.BeginChangeCheck();
            EditorGUI.Toggle(activeToggleRect, sceneIsInBuild, Styles.Toggle);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(scene, $"Toggle {scene.name}");
                if (!sceneIsInBuild)
                    EditorBuildSettings.scenes = EditorBuildSettings.scenes.Append(new EditorBuildSettingsScene(path, true)).ToArray();
                else
                    EditorBuildSettings.scenes = EditorBuildSettings.scenes.Where(s => !s.path.Equals(path)).ToArray();
            }
        }

        private static void DrawTextureOverlay(Texture tex, Rect selectionRect, bool isSelected) {
            var detailsRect = new Rect(selectionRect) {
                x = selectionRect.x + 260,
                width = selectionRect.width - 260
            };

            var w = detailsRect.width;

            var sizeRect = new Rect(detailsRect) {
                width = Mathf.Max(w * .2f, 80)
            };
            var memoryRect = new Rect(detailsRect) {
                x = sizeRect.xMax - 1,
                width = Mathf.Max(w * .2f, 60)
            };
            var cprRect = new Rect(detailsRect) {
                x = memoryRect.xMax - 1,
                xMax = Mathf.Max(memoryRect.xMax + 120, selectionRect.xMax + 1)
            };

            GUI.Label(sizeRect, $"{tex.width}x{tex.height} px", Styles.FilesDetailText);
            GUI.Label(memoryRect, GetMemory(tex), Styles.FilesDetailText);
            GUI.Label(cprRect, GetCompression(tex), Styles.FilesDetailText);

        }

        private static string GetMemory(Object obj) {
            if (obj == null) {
                Debug.Log("obj is empty, unable to get the hard disk size");
                return "???";
            }

            var sprite = obj as Sprite;
            Texture texture = null;

            if (sprite != null)
                texture = sprite.texture;

            if (texture == null)
                texture = obj as Texture;

            if (texture == null)
                return "???";

            _textureUtilType ??= System.Reflection.Assembly.Load("UnityEditor.dll").GetType("UnityEditor.TextureUtil");
#if UNITY_2021_1_OR_NEWER
            _getStorageMemorySizeMethod ??= _textureUtilType.GetMethod("GetStorageMemorySizeLong", new[] { typeof(Texture) });

            return EditorUtility.FormatBytes((long)_getStorageMemorySizeMethod!.Invoke(null, new object[] { texture }));
#else
            _getStorageMemorySizeMethod ??= _textureUtilType.GetMethod("GetStorageMemorySize", new[] { typeof(Texture) });

            return EditorUtility.FormatBytes(_getStorageMemorySizeMethod!.Invoke(null, new object[] { texture }));
#endif
        }

        private static string GetCompression(Texture tex) => GraphicsFormatUtility.GetFormatString(tex.graphicsFormat);
        #endregion
    }
}