﻿using System.IO;
using UnityEditor;
using UnityEditor.AssetImporters;
using UnityEngine;

namespace Izinspector.Editors.Importers {
    [ScriptedImporter(0, new string[0], new []{ "md" })]
    internal sealed class MarkdownImporter : ScriptedImporter {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        [field: SerializeField] internal string[] Lines { get; set; } = new string[0];
        #endregion

        #region Importer Callbacks
        public override void OnImportAsset(AssetImportContext ctx) {
            var path = ctx.assetPath;
            EditorGUIUtility.PingObject(AssetDatabase.LoadMainAssetAtPath(path));
            Lines = File.ReadAllLines(path);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}