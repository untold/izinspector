﻿using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Izinspector.Editors.Importers {
    internal static class MarkdownParser {
        #region Public Variables
        #endregion

        #region Private Variables
        private static GUIStyle _bigTitleStyle = null;
        private static GUIStyle _subTitleStyle = null;
        private static GUIStyle _minorTitleStyle = null;
        private static GUIStyle _normalTextStyle = null;
        private static GUIStyle _bulletPointTextStyle = null;
        private static GUIStyle _codeTextStyle = null;
        #endregion

        #region Properties
        private static GUIStyle BigTitleStyle => _bigTitleStyle ??= new GUIStyle(EditorStyles.label) {
            fontSize = 24,
            fontStyle = FontStyle.Bold,
            stretchWidth = true,
            wordWrap = true,
            richText = true,
            padding = new RectOffset(0, 0, 16, 0)
        };

        private static GUIStyle SubTitleStyle => _subTitleStyle ??= new GUIStyle(EditorStyles.label) {
            fontSize = 18,
            fontStyle = FontStyle.Bold,
            stretchWidth = true,
            wordWrap = true,
            richText = true,
            padding = new RectOffset(0, 0, 16, 0)
        };

        private static GUIStyle MinorTitleStyle => _minorTitleStyle ??= new GUIStyle(EditorStyles.label) {
            fontSize = 16,
            fontStyle = FontStyle.Italic,
            stretchWidth = true,
            wordWrap = true,
            richText = true,
            padding = new RectOffset(0, 0, 10, -4)
        };

        private static GUIStyle NormalTextStyle => _normalTextStyle ??= new GUIStyle(EditorStyles.label) {
            fontSize = 13,
            fontStyle = FontStyle.Normal,
            normal = new GUIStyleState() { textColor = new Color(1f, 1f, 1f, .5f) },
            stretchWidth = true,
            wordWrap = true,
            richText = true,
            padding = new RectOffset(0, 0, 10, 0)
        };

        private static GUIStyle BulletPointTextStyle => _bulletPointTextStyle ??= new GUIStyle(EditorStyles.label) {
            fontSize = 13,
            fontStyle = FontStyle.Normal,
            normal = new GUIStyleState() { textColor = new Color(1f, 1f, 1f, .5f) },
            stretchWidth = true,
            wordWrap = true,
            richText = true,
            padding = new RectOffset(12, 0, 6, 0)
        };

        private static GUIStyle CodeTextStyle => _codeTextStyle ??= new GUIStyle(EditorStyles.label) {
            fontSize = 12,
            fontStyle = FontStyle.Normal,
            normal = new GUIStyleState() { textColor = new Color(1f, 1f, 1f, .25f) },
            stretchWidth = true,
            wordWrap = true,
            richText = true,
            padding = new RectOffset(24, 0, 0, 0)
        };
        #endregion

        #region Public Methods
        internal static void ParseLine(this string line, out string text, out GUIStyle style) {
            text = "";
            style = GUIStyle.none;

            if (TryParseComment(line, ref text, ref style))
                return;

            if (TryParseCode(line, ref text, ref style))
                return;

            if (TryParseTitle(line, ref text, ref style))
                return;

            if (TryParseBulletPoint(line, ref text, ref style))
                return;

            if (line.Length <= 1) {
                text = null;
                style = GUIStyle.none;
                return;
            }

            text = line;
            style = NormalTextStyle;
        }
        #endregion

        #region Private Methods
        private static bool TryParseComment(string line, ref string text, ref GUIStyle style) {
            var commentRegex = new Regex(@"(?<=<!--).*(?=-->)");

            var match = commentRegex.Match(line);

            if (match.Length <= 0)
                return false;

            text = null;
            style = GUIStyle.none;

            return true;
        }

        private static bool TryParseCode(string line, ref string text, ref GUIStyle style) {
            if (!line.StartsWith("        "))
                return false;

            text = line[8..];
            style = CodeTextStyle;

            return true;
        }

        private static bool TryParseTitle(string line, ref string text, ref GUIStyle style) {
            var titleRegex = new Regex(@"^[#]* ");

            var match = titleRegex.Match(line);

            if (match.Length <= 0)
                return false;

            var matchLength = match.Value.Length;
            text = line[matchLength..];
            style = matchLength switch {
                <= 2 => BigTitleStyle,
                <= 4 => SubTitleStyle,
                _ => MinorTitleStyle
            };

            return true;
        }

        private static bool TryParseBulletPoint(string line, ref string text, ref GUIStyle style) {
            var bulletPointRegex = new Regex(@"^\*");

            var match = bulletPointRegex.Match(line);

            if (match.Length <= 0)
                return false;

            var matchLength = match.Value.Length;
            text = "•" + line[matchLength..];
            style = BulletPointTextStyle;

            return true;
        }
        #endregion
    }
}