﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.AssetImporters;
using UnityEngine;

namespace Izinspector.Editors.Importers {
    [CustomEditor(typeof(MarkdownImporter))]
    internal sealed class MarkdownImporterEditor : ScriptedImporterEditor {
        #region Public Variables
        #endregion

        #region Private Variables
        private SerializedProperty _linesProp;

        private readonly List<(string text, GUIStyle style)> _lines = new List<(string, GUIStyle)>();
        #endregion

        #region Properties
        protected override bool needsApplyRevert => false;
        public override bool RequiresConstantRepaint() => true;
        #endregion

        #region Constructors
        #endregion

        #region Editor Callbacks
        public override void OnEnable() {
            base.OnEnable();

            _linesProp = serializedObject.FindProperty("<Lines>k__BackingField");

            for (var i = 0; i < _linesProp.arraySize; i++) {
                var line = _linesProp.GetArrayElementAtIndex(i).stringValue;
                line.ParseLine(out var text, out var style);
                if (!string.IsNullOrWhiteSpace(text))
                    _lines.Add(new ValueTuple<string, GUIStyle>(text, style));
            }
        }

        public override void OnInspectorGUI() {
            foreach (var line in _lines) {
                GUILayout.Label(line.text, line.style);
            }
        }

        protected override bool ShouldHideOpenButton() => true;
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}