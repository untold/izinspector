# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.2/).

## [0.1.0] - 2022-11-13
This is the first setup of the <b>Izinspector</b> Package.

## [1.0.0] - 2022-11-25
Added many <b>PropertyAttributes</b> with corresponding <b>Drawers/Decorators</b>.
Improved existing features.

## [1.1.0] - 2022-11-25
Added support for creating in-engine <i>ReadMe</i> assets as <b>ScriptableObjects</b>.

## [1.2.0] - 2022-11-26
Added utilities for the <b>Hierarchy</b>.

## [1.2.1] - 2022-11-27
Added utilities for the <b>Project Window</b>, plus various fixes.

## [1.2.2] - 2022-11-27
Internalized and sealed most of the types that were not supposed to be public nor inherited.

## [1.2.3] - 2022-11-27
Fixed package name and improved <b>Hierarchy</b> utilities and GUI.

## [1.2.4] - 2022-11-27
Improved <b>Hierarchy</b> utilities GUI.
Extended advanced inspector to <b>ScriptableObjects</b>.
Added user scoped <b>Settings</b>.

## [2.0.0] - 2023-03-01
Upgraded Package to Unity version 2022.2.4f1.
No backwards compatibility granted.

## [2.1.0] - 2023-03-06
Improved hierarchy overlays and active state more readable.

## [2.1.1] - 2023-03-06
Re-added backwards compatibility.

## [2.2.0] - 2023-03-27
Added SceneSwitcher Tool, imported and re-adapted from NiceUtils.

## [2.2.1] - 2023-03-28
Added ResourceViewer Tool, it's just a rough implementation and will be improved in future releases.

## [2.3.1] - 2023-03-29
Added PieMenus Tool, it allows to add nested pie menus to the scene view through the use of Custom Attributes.

## [2.3.3] - 2023-03-31
Improved PieMenus Tool with PieItems icons, validator functions and built-in commands.
Builtin menus can be disabled in preferences.
Minor bug fixes.

## [2.3.4] - 2023-04-01
Improved PieMenus Tool with multiple Roots Contexts.
Added more built-in commands.
Minor bug fixes.

## [2.4.0] - 2023-05-01
Reworked SceneSwitcher Tool using UIElements.
Minor bug fixes.

## [2.4.1] - 2023-05-02
Fixes for retro compatibility with Unity 2021.

## [2.4.2] - 2023-05-10
Fixes for retro compatibility with Unity 2021: removed Pie Menus.

## [2.4.3] - 2023-06-21
Fixes to EnumOptions attribute.
Added new attributes.

## [2.5.0] - 2023-07-14
Added new Asset Dependency Finder functionality to find prefabs and assets that are dependant on others.

## [2.5.1] - 2023-07-14
Improved Asset Dependency Finder with iterative search.

## [2.5.2] - 2023-07-14
Conditional compilation for Asset Dependency Finder for *Unity 2022 or newer*.

## [2.5.3] - 2023-07-14
Added backwards compatibility for Asset Dependency Finder.

## [2.5.4] - 2023-08-17
Added backwards compatibility for Asset Dependency Finder.

## [2.6.0] - 2023-08-24
Added Lockable Objects Tool to prevent editing the transforms of some objects.

## [2.6.1] - 2023-11-21
Added LockAsset attribute to display lock icon on assets referred through hardcoding.
Added AnimatorField attribute to display dropdown menu to select parameter from attached animator.
Added functioning (but buggy) Models Browser.
